<?php get_header(); ?>

<section id="main">
    <?php
    $banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-calendar.png';
    $banner_title = 'Events Calendar';
    include('_partials/short-banner.php'); ?>
    <div class="content">
        <div class="main-col full-width">
            <h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
            <div class="content-wrap">
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
