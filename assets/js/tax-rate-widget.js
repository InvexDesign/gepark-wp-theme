function elementScrolledIntoView($element)
{
    var $window = $(window);
    var viewportTop = $window.scrollTop();
    var viewportBottom = viewportTop + $window.height();
    var elementTop = $element.offset().top;

    return ((elementTop <= viewportBottom) && (elementTop >= viewportTop));
}

function animateTaxRate($element)
{
    $element.addClass('animated');

    var $fill = $element.find('.fill');
    var $text = $element.find('.text');
    var rate = parseFloat($text.html());
    $fill.css('width', rate + '%');
    $text.css('width', rate + '%');
}

function taxRateScrollHandler()
{
    if($rates.length > 0)
    {
        $rates.each(function(index, element) {
            var $element = $(element);
            if(elementScrolledIntoView($element))
            {
                animateTaxRate($element);
            }
        });

        $rates = $('.tax-rate.widget .rate:not(.animated)');
    }
    else
    {
        $(window).off('scroll', taxRateScrollHandler);
        console.log('de-registered taxRateScrollHandler')
    }
}

var $rates = null;
$(document).ready(function()
{
    $rates = $('.tax-rate.widget .rate:not(.animated)');

    if($rates.length > 0)
    {
        $(window).scroll(taxRateScrollHandler);
        console.log('registered taxRateScrollHandler')
    }
});