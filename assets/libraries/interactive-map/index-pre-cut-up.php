<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Glen Ellyn Park District | Established in 1919 | Glen Ellyn, Illinois</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800,400italic,300italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
<link href="../style.css" rel="stylesheet" type="text/css" />
<link href="../font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<!-- Share This Scripts -->
<!--<script type="text/javascript">var switchTo5x=true;</script>-->
<!--<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>-->
<!--<script type="text/javascript">stLight.options({publisher: "6b265ae6-b6d6-4c5a-b707-194d475d71c3", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>-->
<!-- End Share This -->

<!-- Front End Map -->
	<?php require("map_functions.php"); ?>
	<script type="text/javascript" src="jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="jquery-ui.min.js"></script>
	<script type="text/javascript" src="jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="jquery.dataTables.columnFilter.js"></script>
	<!-- TODO: Update this API Key! -->
<!--	<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAApG3C02wvj58_ntggQeayvhQIXH0QpgSOV6MnyL1oQokAnOqf0BTAo9PxYt38jP3TyVvXJmmBFQkNww" type="text/javascript"></script>-->
	<script src="https://maps.google.com/maps?file=api&v=2&key=AIzaSyAl9b47uT7VPFgg2qU6ML7yCaWEbHFuiMg" type="text/javascript"></script>
	<script type="text/javascript" src="map_list_search.js"></script>

	<link type="text/css" href="jquery.dataTables.css" rel="stylesheet" />
	<link type="text/css" href="jquery-ui.css" rel="stylesheet" />
	<link type="text/css" href="style_asit.css" rel="stylesheet" />
	<link type="text/css" href="gepd_map.css" rel="stylesheet" />
<!-- END Front End Map -->

</head>
<body>
	<div class="head-wrap">
		<header id="header">
	    	<h1><a href="../index_placeholder.html" title="Glen Ellyn Park District">Glen Ellyn Park District</a></h1>
	        <div class="meta">
	        	<ul class="social">
	            	<li><a href="https://www.facebook.com/geparkdistrict" title="Like GEPD on Facebook" target="_blank"><img src="../images/social_facebook.png" alt="Facebook Icon" /></a></li>
	                <li><a href="http://www.twitter.com/geparks" title="Follow GEPD on Twitter" target="_blank"><img src="../images/social_twitter.png" alt="Twitter Icon" /></a></li>
	                <li><a href="https://www.instagram.com/glenellynparkdist/" title="Find GEPD on Instagram" target="_blank"><img src="../images/social_instagram.png" alt="Instagram Icon" /></a></li>
	            </ul>
	            <ul class="meta-nav">
	            	<li><a href="#">Contact</a></li>
	                <li><a href="#">Bids / RFPs</a></li>
	                <li><a href="#">Jobs</a></li>
	                <li><a href="#">Weather</a></li>
	            </ul>
	            <form method="get" id="searchform" action="#">
	                <input type="text" value="Search" id="s" name="s" onfocus="if(this.value=='Search')this.value=''" onblur="if(this.value=='')this.value='Search'">
	                <input type="image" src="../images/search_btn.png" id="search-btn" />
	            </form>
	        </div>
	        <div class="callouts">
		        <a href="#" class="activity-guide"><span>Activity Guide</span></a>
		        <a href="#" class="register-now"><span>Register Now</span></a>
	        </div>
	        <nav id="nav">
		        <a href="http://gepark.org/about/" class="about current">About</a>
		        <a href="#" class="parks-facilities">Parks &amp; Facilities</a>
		        <a href="#" class="recreation">Recreation</a>
		        <a href="#" class="registration">Registration</a>
		        <a href="#" class="events">Events</a>
		        <a href="#" class="news">News</a>
		        <a href="http://gepark.org/get-involved/adopt.html" class="get-involved">Get Involved</a>
	        </nav>
	    </header>
	</div>
    <section id="main">
	    <div class="banner" style="background-image: url(../images/flower.png)">
	    	<div class="banner-inner">
		    	<h2><span>Interactive Map</span></h2>
	    	</div>
	    </div>
	    <div class="content">
		    <div class="main-col">
            	<h3>Search for a park with our interactive map
					<span class="share-page">
						<span class='st_facebook' displayText=''></span>
						<span class='st_twitter' displayText=''></span>
						<span class='st_email' displayText=''></span>
						<span class='st_sharethis' displayText=''></span>
					</span>
				</h3>
                <div class="content-wrap">
					<h2>Parks and Facilities Map</h2>

					<!-- Hidden DataTable for front end search/sort -->
					<table id="all_markers" style="display:none;">
						<thead>
						<tr>
							<th>id</th>
							<th>sort name</th>
							<th>name</th>
							<th>url</th>
							<th>address</th>
							<th>city</th>
							<th>state</th>
							<th>zip</th>
							<th>lat</th>
							<th>lng</th>
							<th>features</th>
							<th>description</th>
							<th>imgPath</th>
							<th>phone</th>
							<th>facility_type</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$markers = get_all_markers();
						//        var_dump('BARLEY');
						//        var_dump($markers);
						//        return;
						if(!$markers)
						{
							echo "There are no markers in the database!";
							return;
						}
						?>
						<?php foreach($markers as $row) : ?>
							<tr>
								<td><?php echo $row['id']; ?></td>
								<td><?php echo $row['sort_name']; ?></td>
								<td><?php echo $row['name']; ?></td>
								<td><?php echo $row['url']; ?></td>
								<td><?php echo $row['address']; ?></td>
								<td><?php echo $row['city']; ?></td>
								<td><?php echo $row['state']; ?></td>
								<td><?php echo $row['zip']; ?></td>
								<td><?php echo $row['lat']; ?></td>
								<td><?php echo $row['lng']; ?></td>
								<td><?php echo $row['features']; ?></td>
								<td><?php echo $row['description']; ?></td>
								<td><?php echo $row['imgPath']; ?></td>
								<td><?php echo $row['phone']; ?></td>
								<td><?php echo $row['facility_type']; ?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
						<tfoot>
						<td>ID</td>
						<td>Sort Name</td>
						<td>Name</td>
						<td>Url</td>
						<td>Address</td>
						<td>City</td>
						<td>State</td>
						<td>Zip</td>
						<td>Latitude</td>
						<td>Longitude</td>
						<td>Features</td>
						<td>Description</td>
						<td>imgPath</td>
						<td>Phone</td>
						<td>Facility Type</td>
						</tfoot>
					</table>
					<script>
						var selected;
						var googleMarkers = new Array();
						var filteredMarkerRows = arrayifyTable();
						var infoWindows = new Array();
						var markerHtml = new Array();

						// ID of the div to be turned into a google map.
						var MAP_ID = "map";
						var GOOGLE_MAP;
						// Associative Array/Object mapping db field names to their column index.
						var HEADER_ROW = new Object;

						// Associative Array/Object mapping feature ids to names.
						var FEATURES = new Object;
						var FEATURES_SORTED_ARRAY = new Array();
						<?php
						$features = get_all_features();
						foreach($features as $feature)
						{
							echo "FEATURES['" . $feature['id'] . "'] = '" . $feature['name'] . "';";
							echo "FEATURES_SORTED_ARRAY.push(" . $feature['id'] . ");";
						}
						?>
						var FACILITY_TYPES = null;
						var PHOTO_PREFIX = '<?php echo PHOTO_PREFIX; ?>';

						$(document).ready(function()
						{
							/*
							 $('#facility_type_select').selectmenu().change(
							 function()
							 { filter(); }
							 );
							 $('input:text').button().css(
							 {
							 'font' : 'inherit',
							 'color' : 'inherit',
							 'text-align' : 'left',
							 'outline' : 'none',
							 'cursor' : 'text',
							 'width' : '176px'
							 }
							 );
							 */
							$("#view_tabs").tabs();
							initializeMap();
							var table = $('#all_markers').DataTable({
								bPaginate: false,
								order: [[ 1, 'asc' ]]
							});
							HEADER_ROW = objectifyHeader();

							renderMarkersFromFilteredRows();
							renderSearchInterface();
							//renderSearchFacilities();
							$('#all_markers_wrapper').hide();
							$('#reset_button').button();
						});
					</script>

					<div class="front-end-map-content">
						<table class="interactiveMap">
							<tr>
								<td class="search-results">
									<div id="view_tabs" class="tab-container">
										<ul>
											<li><a href="#map_tab">Map View</a></li>
											<li><a href="#list_tab">List View</a></li>
										</ul>
										<div id="map_tab" class="tab">
											<div id="map" class="search-results"></div>
										</div>
										<div id="list_tab" class="tab">
											<table id="list_view">
											</table>
										</div>
									</div>
								</td>
								<td class="search-container">
									<h3 style="margin-bottom: 10px; color: #990002;" class="first">Search By Name</h3>
									<input type="text" name="text_search" id="text_search" maxlength="128" style="width:176px; margin-bottom:15px;" onkeyup="filter();"/>
									<input type="checkbox" name="text_search_by_name" id="text_search_by_name" onchange="filter();"
										   value="true" title="Check this box to search strictly by facility/park name." checked style="display:none;">
									<!--
									<h2 class="not-first">Facility Type:</h2>
									<select id="facility_type_select" name="facility" style="width:200px; margin-bottom:5px;" onchange="filter();">
									</select>
									<br/>
									-->
									<h3 style="margin-bottom: 10px; color: #990002;" class="not-first">Search By Feature</h3>
									<form id="features_form" style="width: 215px;" onchange="filter();">
									</form>

									<input id="reset_button" type="button" onclick="resetSearch();" value="Reset Search Parameters"/>
								</td>
							</tr>
						</table>
						<div id="result_count" style="margin-bottom:25px; font-size:13px;">
						</div>
						<div class="clearer">&nbsp;</div>
					</div>
				</div>
			</div>
	    </div>
    </section>
    <div class="foot-wrap">
	    <footer id="footer">
		    <div class="col">
			    <p><strong>Glen Ellyn Park District</strong><br />185 Spring Ave<br />Glen Ellyn, IL 60137<br /><a href="https://www.google.com/maps/place/Glen+Ellyn+Park+District/@41.8661338,-88.047295,19z/data=!4m5!3m4!1s0x880e52fefae1c9cd:0x9bb09d0ae33f3f56!8m2!3d41.8661698!4d-88.0460049" target="_blank">[+] Google Maps</a></p>
				<a href="tel:+16308582462" class="icon-link"><!--<i class="fa fa-mobile" aria-hidden="true"></i> -->(630) 858-2462</a>
				<a href="mailto:support@gepark.org" class="icon-link"><!--<i class="fa fa-envelope-o" aria-hidden="true"></i> -->support@gepark.org</a>		
		    </div>
		    <div class="col mission">
			    <h4>Mission Statement</h4>
			    <p>Our mission is driven to foster diverse, community based leisure opportunities, through a harmonious blend of quality recreation programs, facilities and open space which will enhance the quality of life into the future.</p>
		    </div>
		    <div class="col footer-nav">
			    <ul>
				    <li><a href="#">Home</a></li>
				    <li><a href="#">About</a></li>
				    <li><a href="#">Parks &amp; Facilities</a></li>
				    <li><a href="#">Recreation</a></li>
				    <li><a href="#">Registration</a></li>
			    </ul>
		    </div>
		    <div class="col footer-nav">
			    <ul>
				    <li><a href="#">Events</a></li>
				    <li><a href="#">News</a></li>
				    <li><a href="#">Get Involved</a></li>
				    <li><a href="#">Employment</a></li>
				    <li><a href="#">Contact</a></li>
			    </ul>
		    </div>
		    <div class="col copyright">
			    <img src="../images/logo_footer.png" />
			    <span>Glen Ellyn Park District &copy; 2016<br />Powered by <a href="https://www.invexdesign.com" target="_blank" title="Chicago Web Design, Web Hosting, SEO | Invex Design"><strong>Invex Design</strong></a></span>
		    </div>
		    <span class="overline"> </span>
	    </footer>
    </div>
</body>
</html>
