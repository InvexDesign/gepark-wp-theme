<?php
require_once("map_constants.php");

/* ----- Generic Functions ----- */
function mysql_prep($value) {
	$magic_quotes_active = get_magic_quotes_gpc();
	$new_enough_php = function_exists( "mysql_real_escape_string" ); // i.e. PHP >= v4.3.0
	if( $new_enough_php ) { // PHP v4.3.0 or higher
		// undo any magic quote effects so mysql_real_escape_string can do the work
		if( $magic_quotes_active ) { $value = stripslashes( $value ); }
		$value = mysql_real_escape_string( $value );
	} else { // before PHP v4.3.0
		// if magic quotes aren't already on then add slashes manually
		if( !$magic_quotes_active ) { $value = addslashes( $value ); }
		// if magic quotes are active, then the slashes already exist
	}
	return $value;
}

function redirect_to($location = NULL) {
	if ($location != NULL) {
		header("Location: {$location}");
		exit;
	}
}

function confirm_query($result_set) {
	if (!$result_set) {
		die("Database query failed: " . mysql_error());
	}
}

function is_id_in_string($id, $string) {
	$arr = explode(',',$string);
	$num = count($arr);
	for ($j = 0; $j < $num; $j++) {
		if ($arr[$j] == $id)
			return true;
	}
	return false;
}

function parseToXML($htmlStr) {
	$xmlStr=str_replace('<','&lt;',$htmlStr);
	$xmlStr=str_replace('>','&gt;',$xmlStr);
	$xmlStr=preg_replace("/href=\"([^\"]*)\"/","/target=_top href=$1",$xmlStr);
	$xmlStr=preg_replace("/style=\"([^\"]*)\"/","/style=$1",$xmlStr);
	$xmlStr=preg_replace("/color=\"([^\"]*)\"/","/color=$1",$xmlStr);
	$xmlStr=str_replace('"','&quot;',$xmlStr);
	$xmlStr=str_replace("'",'&#39;',$xmlStr);
	$xmlStr=str_replace("&",'&amp;',$xmlStr);
	return $xmlStr;
}

/* ----- Marker Specific Functions ----- */

function get_all_markers() {
	$mysqli = new mysqli(MAP_DB_SERVER,MAP_DB_USER,MAP_DB_PASS,MAP_DB_NAME);
	$query = "SELECT * FROM ".MARKERS_TABLE." ORDER BY sort_name ASC";

	$result = $mysqli->query($query);
	$mysqli->close();

	return $result;
}

function get_marker_by_id($marker_id) {
	global $connection;
	$query = "SELECT * FROM ".MARKERS_TABLE." WHERE id = {$marker_id} LIMIT 1";
	$result_set = mysql_query($query, $connection);
	confirm_query($result_set);
	// REMEMBER:
	// if no rows are returned, fetch_array will return false
	if ($marker = mysql_fetch_array($result_set)) {
		return $marker;
	} else {
		return NULL;
	}
}

function get_markers_by_features_and_name($features_arr, $name, $facility) {
	global $connection;
	$query = "SELECT * FROM ".MARKERS_TABLE." WHERE ";
	if (is_null($features_arr)) {
		$query .= "name LIKE '%$name'";
		if ($facility != "")
			$query .= " AND facility = '{$facility}'";
		$query .= " ORDER BY sort_name ASC";
	} else {
		// go through each features selected and append it to the query
		$features_num = 1;
		foreach ($features_arr as $key => $val) {
			if ($features_num != 1)
				$query .= "AND ";
			$query .= "(features = '{$val}' OR features LIKE '{$val},%' OR features LIKE '%,{$val}' OR features LIKE '%,{$val},%') ";
			$features_num++;
		}
		$query .= "AND name LIKE '%$name'";
		if ($facility != "")
			$query .= " AND facility = '{$facility}'";
		$query .= " ORDER BY sort_name ASC";
	}
	//mail('brian@invexdesign.com','DP Map SQL Query',$query);
	$result_set = mysql_query($query);
	confirm_query($result_set);
	// if no rows are returned, fetch_array will return false
	if (mysql_num_rows($result_set) != 0) {
		return $result_set;
	} else {
		return NULL;
	}
}

function get_all_markers_for_single_feature($feature_id)
{
	global $connection;
	$query = "SELECT * FROM ".MARKERS_TABLE." WHERE ";
	$query .= "(features = '{$feature_id}' OR features LIKE '{$feature_id},%' OR features LIKE '%,{$feature_id}' OR features LIKE '%,{$feature_id},%') ";
	$query .= " ORDER BY sort_name ASC";

	$result_set = mysql_query($query);
	confirm_query($result_set);
	// if no rows are returned, fetch_array will return false
	if (mysql_num_rows($result_set) != 0)
	{ return $result_set; }
	else
	{ return NULL; }
}
function search_markers_for_single_feature($feature_id, $name, $facility)
{
	global $wpdb;
	$query = "SELECT * FROM ".MARKERS_TABLE." WHERE ";
	$query .= "(features = '{$feature_id}' OR features LIKE '{$feature_id},%' OR features LIKE '%,{$feature_id}' OR features LIKE '%,{$feature_id},%') ";
	if($name != "")
	{ $query .= " AND name LIKE '%$name'"; }
	if($facility != "")
	{ $query .= " AND facility = '{$facility}'"; }
	$query .= " ORDER BY sort_name ASC";

	$results = $wpdb->get_results($query, ARRAY_A);

	// if no rows are returned, fetch_array will return false
	if( count($results) != 0)
	{ return $results; }
	else
	{ return NULL; }
}

/* ----- Feature Functions ----- */

function get_all_features()
{
	$mysqli = new mysqli(MAP_DB_SERVER,MAP_DB_USER,MAP_DB_PASS,MAP_DB_NAME);
	$query = "SELECT * FROM ".FEATURES_TABLE." ORDER BY name ASC";

	$result = $mysqli->query($query);
	$mysqli->close();

	return $result;

//	global $wpdb;
	//echo "<pre>".var_dump($query)."</pre>";
//	$results = $wpdb->get_results($query, ARRAY_A);
	//echo "<pre>".var_dump($results)."</pre>";
//	return $results;
}

function get_all_features_sorted_by_id() {
	global $connection;
	$query = "SELECT * FROM ".FEATURES_TABLE." ORDER BY id ASC";
	$result_set = mysql_query($query, $connection);
	confirm_query($result_set);
	return $result_set;
}

function get_feature_by_id($feature_id)
{
	global $wpdb;
	$query = "SELECT * FROM ".FEATURES_TABLE." WHERE id = {$feature_id} LIMIT 1";
	//echo "<pre>".var_dump($query)."</pre>";
	$results = $wpdb->get_results($query, ARRAY_A);
	//echo "<pre>".var_dump($results)."</pre>";

	// If no rows are returned, fetch_array will return false
	if( count($results) == 1)
	{ return $results[0]; }
	else
	{ return NULL; }
}

function get_features_by_category($category_id)
{
	global $connection;
	$query = "SELECT * FROM ".FEATURES_TABLE." WHERE ";
	$query .= "(categories = '{$category_id}' OR categories LIKE '{$category_id},%' OR categories LIKE '%,{$category_id}' OR categories LIKE '%,{$category_id},%') ";
	$query .= " ORDER BY id ASC";

	$result_set = mysql_query($query);
	confirm_query($result_set);
	// if no rows are returned, fetch_array will return false
	if (mysql_num_rows($result_set) != 0)
	{ return $result_set; }
	else
	{ return NULL; }
}

//TODO: Data doesnt exist yet.
function get_all_feature_categories() {
	/*global $connection;
	$query = "SELECT DISTINCT category FROM ".FEATURES_TABLE." ORDER BY sort_name ASC";
	$result_set = mysql_query($query, $connection);
	confirm_query($result_set);
	return $result_set;
	*/
	return "TODO: Data doesnt exist yet.";
}

function get_all_categories($sortByName = false)
{
	if($sortByName)
	{ $sort = "name"; }
	else
	{ $sort = "id"; }

	global $wpdb;
	$query = "SELECT * FROM ".CATEGORIES_TABLE." ORDER BY ". $sort ." ASC";
	//echo "<pre>".var_dump($query)."</pre>";
	$results = $wpdb->get_results($query, ARRAY_A);
	//echo "<pre>".var_dump($results)."</pre>";
	return $results;
}

function get_category_by_id($category_id)
{
	global $wpdb;
	$query = "SELECT * FROM ".CATEGORIES_TABLE." WHERE id = {$category_id} LIMIT 1";
	//echo "<pre>".var_dump($query)."</pre>";
	$results = $wpdb->get_results($query, ARRAY_A);
	//echo "<pre>".var_dump($results)."</pre>";

	// If no rows are returned, fetch_array will return false
	if( count($results) == 1 )
	{ return $results[0]; }
	else
	{ return NULL; }
}

/* FACILITY TYPE FUNCTIONS */

function get_all_facility_types() {
	$mysqli = new mysqli(MAP_DB_SERVER,MAP_DB_USER,MAP_DB_PASS,MAP_DB_NAME);
	$query = "SELECT * FROM ".FACILITY_TYPE_TABLE;

	$result = $mysqli->query($query);
	$mysqli->close();

	return $result;
}

//
//	function mysql_prep($value) {
//		$magic_quotes_active = get_magic_quotes_gpc();
//		$new_enough_php = function_exists( "mysql_real_escape_string" ); // i.e. PHP >= v4.3.0
//		if( $new_enough_php ) { // PHP v4.3.0 or higher
//			// undo any magic quote effects so mysql_real_escape_string can do the work
//			if( $magic_quotes_active ) { $value = stripslashes( $value ); }
//			$value = mysql_real_escape_string( $value );
//		} else { // before PHP v4.3.0
//			// if magic quotes aren't already on then add slashes manually
//			if( !$magic_quotes_active ) { $value = addslashes( $value ); }
//			// if magic quotes are active, then the slashes already exist
//		}
//		return $value;
//	}
//
//	function redirect_to($location = NULL) {
//		if ($location != NULL) {
//			header("Location: {$location}");
//			exit;
//		}
//	}
//
//	function confirm_query($result_set) {
//		if (!$result_set) {
//			die("Database query failed: " . mysql_error());
//		}
//	}
//
//	function is_id_in_string($id, $string) {
//		$arr = explode(',',$string);
//		$num = count($arr);
//		for ($j = 0; $j < $num; $j++) {
//			if ($arr[$j] == $id)
//				return true;
//		}
//		return false;
//	}
//
//	function parseToXML($htmlStr) {
//		$xmlStr=str_replace('<','&lt;',$htmlStr);
//		$xmlStr=str_replace('>','&gt;',$xmlStr);
//		$xmlStr=preg_replace("/href=\"([^\"]*)\"/","/target=_top href=$1",$xmlStr);
//		$xmlStr=preg_replace("/style=\"([^\"]*)\"/","/style=$1",$xmlStr);
//		$xmlStr=preg_replace("/color=\"([^\"]*)\"/","/color=$1",$xmlStr);
//		$xmlStr=str_replace('"','&quot;',$xmlStr);
//		$xmlStr=str_replace("'",'&#39;',$xmlStr);
//		$xmlStr=str_replace("&",'&amp;',$xmlStr);
//		return $xmlStr;
//	}
//
//	/* ----- Marker Specific Functions ----- */
//
//	function get_all_markers() {
//		global $connection;
//		$query = "SELECT * FROM ".MARKERS_TABLE." ORDER BY sort_name ASC";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		return $result_set;
//	}
//
//	function get_marker_by_id($marker_id) {
//		global $connection;
//		$query = "SELECT * FROM ".MARKERS_TABLE." WHERE id = {$marker_id} LIMIT 1";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		// REMEMBER:
//		// if no rows are returned, fetch_array will return false
//		if ($marker = mysql_fetch_array($result_set)) {
//			return $marker;
//		} else {
//			return NULL;
//		}
//	}
//
//	function get_markers_by_features_and_name($features_arr, $name, $facility) {
//		global $connection;
//		$query = "SELECT * FROM ".MARKERS_TABLE." WHERE ";
//		if (is_null($features_arr)) {
//			$query .= "name LIKE '%$name'";
//			if ($facility != "")
//				$query .= " AND facility = '{$facility}'";
//			$query .= " ORDER BY sort_name ASC";
//		} else {
//			// go through each features selected and append it to the query
//			$features_num = 1;
//			foreach ($features_arr as $key => $val) {
//				if ($features_num != 1)
//					$query .= "AND ";
//				$query .= "(features = '{$val}' OR features LIKE '{$val},%' OR features LIKE '%,{$val}' OR features LIKE '%,{$val},%') ";
//				$features_num++;
//			}
//			$query .= "AND name LIKE '%$name'";
//			if ($facility != "")
//				$query .= " AND facility = '{$facility}'";
//			$query .= " ORDER BY sort_name ASC";
//		}
//		//mail('brian@invexdesign.com','DP Map SQL Query',$query);
//		$result_set = mysql_query($query);
//		confirm_query($result_set);
//		// if no rows are returned, fetch_array will return false
//		if (mysql_num_rows($result_set) != 0) {
//			return $result_set;
//		} else {
//			return NULL;
//		}
//	}
//
//	function get_all_markers_for_single_feature($feature_id)
//	{
//		global $connection;
//		$query = "SELECT * FROM ".MARKERS_TABLE." WHERE ";
//		$query .= "(features = '{$feature_id}' OR features LIKE '{$feature_id},%' OR features LIKE '%,{$feature_id}' OR features LIKE '%,{$feature_id},%') ";
//		$query .= " ORDER BY sort_name ASC";
//
//		$result_set = mysql_query($query);
//		confirm_query($result_set);
//		// if no rows are returned, fetch_array will return false
//		if (mysql_num_rows($result_set) != 0)
//		{ return $result_set; }
//		else
//		{ return NULL; }
//	}
//	function search_markers_for_single_feature($feature_id, $name, $facility)
//	{
//		global $connection;
//		$query = "SELECT * FROM ".MARKERS_TABLE." WHERE ";
//		$query .= "(features = '{$feature_id}' OR features LIKE '{$feature_id},%' OR features LIKE '%,{$feature_id}' OR features LIKE '%,{$feature_id},%') ";
//		if($name != "")
//		{ $query .= " AND name LIKE '%$name'"; }
//		if($facility != "")
//		{ $query .= " AND facility = '{$facility}'"; }
//		$query .= " ORDER BY sort_name ASC";
//
//		$result_set = mysql_query($query);
//		confirm_query($result_set);
//		// if no rows are returned, fetch_array will return false
//		if (mysql_num_rows($result_set) != 0)
//		{ return $result_set; }
//		else
//		{ return NULL; }
//	}
//
//	/* ----- Feature Functions ----- */
//
//	function get_all_features() {
//		global $connection;
//		$query = "SELECT * FROM ".FEATURES_TABLE." ORDER BY name ASC";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		return $result_set;
//	}
//
//	function get_all_features_sorted_by_id() {
//		global $connection;
//		$query = "SELECT * FROM ".FEATURES_TABLE." ORDER BY id ASC";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		return $result_set;
//	}
//
//	function get_feature_by_id($feature_id) {
//		global $connection;
//		$query = "SELECT * FROM ".FEATURES_TABLE." WHERE id = {$feature_id} LIMIT 1";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		// REMEMBER:
//		// if no rows are returned, fetch_array will return false
//		if ($feature = mysql_fetch_array($result_set)) {
//			return $feature;
//		} else {
//			return NULL;
//		}
//	}
//
//	function get_features_by_category($category_id)
//	{
//		global $connection;
//		$query = "SELECT * FROM ".FEATURES_TABLE." WHERE ";
//		$query .= "(categories = '{$category_id}' OR categories LIKE '{$category_id},%' OR categories LIKE '%,{$category_id}' OR categories LIKE '%,{$category_id},%') ";
//		$query .= " ORDER BY id ASC";
//
//		$result_set = mysql_query($query);
//		confirm_query($result_set);
//		// if no rows are returned, fetch_array will return false
//		if (mysql_num_rows($result_set) != 0)
//		{ return $result_set; }
//		else
//		{ return NULL; }
//	}
//
//	//TODO: Data doesnt exist yet.
//	function get_all_feature_categories() {
//		/*global $connection;
//		$query = "SELECT DISTINCT category FROM ".FEATURES_TABLE." ORDER BY sort_name ASC";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		return $result_set;
//		*/
//		return "TODO: Data doesnt exist yet.";
//	}
//
//	function get_all_categories($sortByName = false)
//	{
//		if($sortByName)
//		{ $sort = "name"; }
//		else
//		{ $sort = "id"; }
//
//		global $connection;
//		$query = "SELECT * FROM ".CATEGORIES_TABLE." ORDER BY ". $sort ." ASC";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		return $result_set;
//	}
//
//	function get_category_by_id($category_id)
//	{
//		global $connection;
//		$query = "SELECT * FROM ".CATEGORIES_TABLE." WHERE id = {$category_id} LIMIT 1";
//		$result_set = mysql_query($query, $connection);
//		confirm_query($result_set);
//		// REMEMBER:
//		// if no rows are returned, fetch_array will return false
//		if ($feature = mysql_fetch_array($result_set))
//		{ return $feature; }
//		else
//		{ return NULL; }
//	}

?>