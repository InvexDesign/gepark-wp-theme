/* Helpers */
function unique(arr) {
    var hash = {}, result = [];
    for ( var i = 0, l = arr.length; i < l; ++i ) {
        if ( !hash.hasOwnProperty(arr[i]) ) { //it works with objects! in FF, at least
            hash[ arr[i] ] = true;
            result.push(arr[i]);
        }
    }
    return result;
}
function explode_then_unique(arr)
{
    var hash = {}, result = [];
    for(var j = 0, m = arr.length; j < m; ++j)
	{
		if(typeof arr[j] === 'undefined' || arr[j] == '')
		{ continue; }
		
		var splitArray = arr[j].split(',');
		for(var i = 0, l = splitArray.length; i < l; ++i)
		{
			if( !hash.hasOwnProperty(splitArray[i]) )
			{
				hash[ splitArray[i] ] = true;
				result.push(splitArray[i]);
			}
		}
    }
	
    return result;
}

/* Datatable Management */
function objectifyHeader()
{
	var headerObject = new Object;
	var header = $('#all_markers thead tr')[0]
	var cells = header.cells;
	
	for(var i = 0; i < cells.length; ++i)
	{ headerObject[ cells[i].innerHTML ] = i; }
	
	return headerObject;
}
function arrayifyHeader()
{
	var headerArray = new Array();
	var header = $('#all_markers thead tr')[0]
	var cells = header.cells;
	
	for(var i = 0; i < cells.length; ++i)
	{ headerArray.push( cells[i].innerHTML ); }
	
	return headerArray;
}
function objectifyRow(row)
{
	var rowObject = new Object;
	var cells = row.cells;
	
	for(var i = 0; i < cells.length; ++i)
	{ rowObject[i] = cells[i].innerHTML; }
	
	return rowObject;
}
function arrayifyRow(row)
{
	var rowArray = new Array();
	var cells = row.cells;
	
	for(var i = 0; i < cells.length; ++i)
	{ rowArray.push( cells[i].innerHTML ); }
	
	return rowArray;
}
function arrayifyTable()
{
	var tableArray = new Array();
	var rows = $('#all_markers tbody tr[role="row"]');
	
	for(var i = 0; i < rows.length; ++i)
	{ tableArray.push( arrayifyRow(rows[i]) ); } 
	
	return tableArray;
}

function generateMapIcon(image_src_url)
{
	var icon = new GIcon();
	icon.image = image_src_url;
	icon.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
	icon.iconSize = new GSize(30, 30);
	icon.shadowSize = new GSize(22, 20);
	icon.iconAnchor = new GPoint(6, 20);
	icon.infoWindowAnchor = new GPoint(5, 1);

	return icon;
}

//TODO: Hardcoded
var MAP_MARKER_SRC = 'http://gepark.org/map-admin/images/map-marker-icon.png';
var mapIcon = new GIcon();
mapIcon.image = MAP_MARKER_SRC;
mapIcon.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
mapIcon.iconSize = new GSize(12, 20);
mapIcon.shadowSize = new GSize(22, 20);
mapIcon.iconAnchor = new GPoint(6, 20);
mapIcon.infoWindowAnchor = new GPoint(5, 1);

var iconThirteen = new GIcon();
iconThirteen.image = 'http://www.zeroninedesigns.com/google/mm_20_lime.png';
iconThirteen.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
iconThirteen.iconSize = new GSize(12, 20);
iconThirteen.shadowSize = new GSize(22, 20);
iconThirteen.iconAnchor = new GPoint(6, 20);
iconThirteen.infoWindowAnchor = new GPoint(5, 1);

var iconFourteen = new GIcon();
iconFourteen.image = 'http://www.zeroninedesigns.com/google/mm_20_coolBlue.png';
iconFourteen.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
iconFourteen.iconSize = new GSize(12, 20);
iconFourteen.shadowSize = new GSize(22, 20);
iconFourteen.iconAnchor = new GPoint(6, 20);
iconFourteen.infoWindowAnchor = new GPoint(5, 1);

// var customIcons = [];
// customIcons["Aquatic Center"] = iconOne;
// customIcons["Chapel"] = iconTwo;
// customIcons["Community Center"] = iconThree;
// customIcons["Dog Park"] = iconFour;
// customIcons["Farm"] = iconFive;
// customIcons["Golf Course"] = iconSix;
// customIcons["Indoor Ice Rink"] = iconSeven;
// customIcons["Indoor Tennis Club"] = iconEight;
// customIcons["Nature Center"] = iconNine;
// customIcons["Park"] = iconTen;
// customIcons["Maintenance Facility"] = iconEleven;
// customIcons["School Grounds"] = iconTwelve;
// customIcons["Senior Center"] = iconThirteen;
// customIcons["Skate Park"] = iconFourteen;


/* Map Management */
function initializeMap()
{
	if(GBrowserIsCompatible())
	{
		GOOGLE_MAP = new GMap2( document.getElementById(MAP_ID) );
		GOOGLE_MAP.setCenter(new GLatLng(41.8644481,-88.063708), 13);
		GOOGLE_MAP.setUIToDefault();
		GOOGLE_MAP.removeMapType(G_PHYSICAL_MAP);
	}
	else
	{
		alert('GBrowserIsCompatible() failed!');
	}
}
function removeMarkersFromMap()
{
	GOOGLE_MAP.clearOverlays();
	googleMarkers = [];
}
function generatePopupHtml(name, url, address, city, state, zip, phone, description, imgPath)
{
	var html = '<h5>' + name + '</h5>' +
		'<div class="caption">' +
			'<div class="imgWrap">' +
				'<img style="width: 100px;" class="map-popup-img" src="' + PHOTO_PREFIX + imgPath + '" />' +
			'</div>' +
			'<p class="info">' +
				address + '<br/>' +
				city + ', ' + state + ' ' + zip;
				if(phone && (phone != '') )
				{ html += '<br />' + phone; }
    html +=  '<br />' +	'<a href="' + prepareGoogleMapsDirectionsLink(address,city,state,zip) +'" target="_blank">Get Directions</a>';

    if(url && url != '')
    {
        html += '<br />' +

        '<a href="' + url + '" target="_blank">View Full Page</a>';
    }

    html += '</p>' +
	//html+= 	'</p>' +
	//		'<a href="' + prepareGoogleMapsDirectionsLink(address,city,state,zip) +'" target="_blank">Get Directions</a>' +
	//		'<br />' +
			'<p class="description">' +
				description +
			'</p>' +
		'</div>';
		
	return html;
}
function prepareGoogleMapsDirectionsLink(address, city, state, zip)
{
	var fullAddress = address + '+' + city + '+' + state + '+' + zip;
	return 'https://maps.google.com?daddr=' + fullAddress.replace(' ', '+');
}
function createMarker(point, name, url, address, city, state, zip, type, description, imgPath, phone, facility_type)
{
	var icon = FACILITY_TYPE_ICONS[facility_type];
	// if(facility_type in FACILITY_TYPE_ICONS)
	// {
	// 	icon = FACILITY_TYPE_ICONS[facility_type];
	// }
	// else
	// {
	// 	icon = FACILITY_TYPE_ICONS[facility_type];
	// }
	var marker = new GMarker(point, icon);
	//var marker = new GMarker(point, myIcon);
	marker.index = googleMarkers.length;
	
	var html = generatePopupHtml(name, url, address, city, state, zip, phone, description, imgPath);
/* 
	markerHtml.push(html);
	var new_info_window = new google.maps.InfoWindow({
		content: html,
	});
	infoWindows.push(new_info_window);
 */	
	GEvent.addListener(marker, 'click',
		function()
		{
			//new_info_window.open(GOOGLE_MAP, marker);
			var DIRECTION_CONTENT =
				'<div style="margin-top:20px;width:270px"><form action="http://maps.google.com/maps" method="get" target="_blank" class="directionsForm">' +
				'<p style="margin:0 0 5px 0;font-size:12px;"><strong>Get Directions to ' + name +'</strong></p>' +
				'<p class="descriptor" style="margin:5px 0 5px 0;">Start address:</p>' +
				'<input type="text" SIZE=33 MAXLENGTH=75 name="saddr" id="saddr" value="Street, City, State, Zip" class=descriptor' +
				' onFocus="this.value=\'\';" style="margin-bottom:5px;" /><br>' +
				'<INPUT value="Get Directions" TYPE="SUBMIT" class=descriptor>' +
				'<input type="hidden" name="daddr" value="' + address + ', ' + city + ', ' + state + ' ' + zip + '"/></form></div>';

			var tabs = [];  
			//tabs.push(new GInfoWindowTab('Info', html));  
			//tabs.push(new GInfoWindowTab("Directions", DIRECTION_CONTENT));
			
			var max_height = $(MAP_ID).height() - 75;
			//marker.openInfoWindowHtml(markerHtml[marker.index], {maxWidth:300, maxHeight:max_height, autoScroll:false });
			marker.openInfoWindowHtml(html, {maxWidth:300, maxHeight:max_height, autoScroll:false });
		}
	);

	return marker;
}

function map_list_item_clicked(index)
{
	$('a[href="#map_tab"]').click();
	
	/* 
	$('#view_tabs').on('tabsactivate', function() {
		GEvent.trigger(googleMarkers[index], 'click');
	});
	*/
	
	setTimeout(function() {
		GEvent.trigger(googleMarkers[index], 'click');
	}, 250);
}

/* Map-List Rendering */
function renderMapMarker(row, index)
{
	//var id 			= row[ HEADER_ROW["id"] ];
	var name 			= row[ HEADER_ROW["name"] ];
	var url      		= row[ HEADER_ROW["url"] ];
	var address 		= row[ HEADER_ROW["address"] ];
	var city 			= row[ HEADER_ROW["city"] ];
	var state 			= row[ HEADER_ROW["state"] ];
	var zip 			= row[ HEADER_ROW["zip"] ];
	var lat 			= row[ HEADER_ROW["lat"] ];
	var lng 			= row[ HEADER_ROW["lng"] ];
	var features 		= row[ HEADER_ROW["features"] ];
	var description 	= row[ HEADER_ROW["description"] ];
	var imgPath 		= row[ HEADER_ROW["imgPath"] ];
	var phone 			= row[ HEADER_ROW["phone"] ];
	var facility_type 	= row[ HEADER_ROW["facility_type"] ];

	//Populate the multiselect list of markers.
	var oSelect = $('#list').append('<option value=' + index + '>' + name + '</option>');

	var point = new GLatLng(
		parseFloat(lat),
		parseFloat(lng)
	);

	if(typeof url == 'undefined')
	{
		url = '';
	}
	//console.log('url: ' + url);

	if(imgPath == '')
	{
		imgPath = 'default.png';
	}
	// console.log('imgPath: ' + imgPath);

	// Generate google map marker and display it.
	googleMarkers[index] = createMarker(point, name, url, address, city, state, zip, features, description, imgPath, phone, facility_type); //TODO: Change to facility_type
	GOOGLE_MAP.addOverlay(googleMarkers[index]);
	
	// Generate html innards for list view.
	var html =

	'<div class="list-view-marker">' +
		'<div class="fix-margin thumbnail">' +
            '<img class="map-list-img" src="' + PHOTO_PREFIX + imgPath + '" />' +
		'</div>' +
		'<div class="description">' +
			'<h4 style="margin:0;">' +
				'<a href="javascript:" onclick="map_list_item_clicked('+ index + ');">' +
					name +
				'</a>' +
			'</h4>' +
			'<br />' +
			'<p>' +
				address + '<br/>' +
				city + ', ' + state + ' ' + zip +
			'</p>';
			// '<p>' + address + '</p>' +
			// '<p>Hours: 6:00 AM-11:00 PM</p>';

    if(url != '')
    {
        html +=
            '<p><a href="' + url + '" target="_blank">View Park Page</a></p>';
    }
    else
    {
        html +=
            '<p>&nbsp;</p>';
    }

    html +=
		'</div>' +
		'<div class="clearer"></div>' +
	'</div>';
	
	return html;
}
function renderMarkersFromFilteredRows()
{
	removeMarkersFromMap();
	
	// Get filtered rows from hidden table.
	filteredMarkerRows = arrayifyTable();
	
	// Wipe out all current markers and repopulate them below.
	var oSelect = $('#list');
	oSelect.html('');
	
	var listViewMarkerInnardsCollection = new Array();
	for(var i = 0; i < filteredMarkerRows.length; ++i)
	{
		//TODO: Is there a better way to know that there were no records/no data found?
		if( (filteredMarkerRows[i] == 'No matching records found') || (filteredMarkerRows[i] == 'No data available in table') )
		{ continue; }
		
		// Generate map markers
		listViewMarkerInnards = renderMapMarker(filteredMarkerRows[i], i);
		listViewMarkerInnardsCollection.push(listViewMarkerInnards);
	}
	
	// Render list view table.
	var listViewTable = $('#list_view');
	$('#list_view').html('');
	for(var i = 0; i < listViewMarkerInnardsCollection.length; i += 2)
	{
		if(i + 1 < listViewMarkerInnardsCollection.length)
		{ listViewTable.append('<tr><td>' + listViewMarkerInnardsCollection[i] + '</td><td>' + listViewMarkerInnardsCollection[i+1] + '</td></tr>'); }
		else
		{ listViewTable.append('<tr><td>' + listViewMarkerInnardsCollection[i] + '</td><td></td></tr>'); }
	}
	
	var table = $('#all_markers').dataTable();
	var totalRows = table.fnSettings().fnRecordsTotal();
	var filteredRows = table.fnSettings().fnRecordsDisplay();
	var resultCount = $('#result_count');
	resultCount.html('Showing ' + filteredRows + ' facilities out of ' + totalRows + ' total.');
}

/* Search Management */
function renderSearchInterface(isFixedFeature)
{
	if(!isFixedFeature)
	{ renderSearchFeatures(); }
}

//TODO: Render options with no results as disabled EACH TIME?
// function renderSearchFacilities()
// {
	//Only render the facility_type_select once.
	// if(!FACILITY_TYPES)
	// {
		//Collect all facility_types from rows that pass the current search filters.
		// var facility_types = new Array();
		// var rows = filteredMarkerRows;
		// for(var i = 0; i < rows.length; i++)
		// { facility_types.push( rows[i][ HEADER_ROW["facility_type"] ] ); }
		
		//Only keep the unique facility_types.
		// var uniqueFacilities = unique(facility_types);
		// FACILITY_TYPES = uniqueFacilities.sort();
		
		//Store the previously selected facility so we can restore it.
		// var selected = $('#facility_type_select').val();
		
		//Wipe out all options so we can rebuild only the ones that have search results.
		// var select = $('#facility_type_select');
		// select.html('');
		// select.append('<option value="" style="font-style:italic;color:#999;" selected>-- View All Types --</option>');

		// for(var i = 0; i < uniqueFacilities.length; i++)
		// { select.append('<option value="' + uniqueFacilities[i] + '">' + uniqueFacilities[i] + '</option>'); }
	// }
// }

function renderSearchFeatures()
{
	//Collect all feature strings from rows that pass the current search filters.
	var uniqueFeatures = new Array();
	var unexplodedFeatures = new Array();
	var rows = filteredMarkerRows;
	if(rows[0] != "No matching records found")
	{
		for(var i = 0; i < rows.length; i++)
		{ unexplodedFeatures.push( rows[i][ HEADER_ROW["features"] ] ); } //TODO: Change to features
		
		// Explode the feature strings and keep only unique values.
		uniqueFeatures = explode_then_unique(unexplodedFeatures);
	}
	
	// Store the currently selected checkboxes so we can restore them.
	var checkedIds = new Array();
	var checkedboxes = $('#features_form input[type=checkbox]:checked');
	for(var i = 0; i < checkedboxes.length; i++)
	{ checkedIds.push(checkedboxes[i].value) }
	
	var form = $('#features_form');
	form.html('');
	for(var i = 0; i < FEATURES_SORTED_ARRAY.length; i++)
	{
		var feature_id = '' + FEATURES_SORTED_ARRAY[i];
		var feature_name = FEATURES[feature_id];
		
		var checkbox_id = 'feature_checkbox_' + feature_id;
		var checkbox_label = feature_name;
		var input = $('<input type="checkbox" name="' + checkbox_id + '" id="' + checkbox_id + '" value="' + feature_id + '">');
		
		var wasChecked = ($.inArray(feature_id, checkedIds) > -1);
		var hasNoResults = ($.inArray(feature_id, uniqueFeatures) < 0);
		
		// If this feature was not found in any markers in the new search then disable it.
		if(hasNoResults)
		{ input.attr('disabled', true); }
		

		// Check this box if it was checked prior to the new search.
		if(wasChecked)
		{ input.attr('checked', true); }
		
		var label = $('<label for="' + checkbox_id + '">' + checkbox_label + '</label>');
		form.append(input);
		form.append(label);
		form.append('<br/>');
	}
}
function generateFeatureIdRegex(id, position, len)
{
	if(typeof(id) == 'undefined')
	{
		alert("Invalid ID!");
		console.error("generateFeatureIdRegex: Invalid ID!");
		return;
	}
	if(typeof(position) == 'undefined')
	{
		alert("Invalid position!");
		console.error("generateFeatureIdRegex: Invalid position!");
		return;
	}
	if(typeof(len) == 'undefined')
	{
		alert("Invalid len!");
		console.error("generateFeatureIdRegex: Invalid len!");
		return;
	}
	
	var regex = '(';
	// Matches if the id is the FIRST in the features_string_array.
	// Only the FIRST feature_id (ascending sort) could match in this case.
	if(position == 0)
	{		
		regex +=  '^'+ id + '(?=[^0-9])';
		regex += '|';
	}
	
	// Matches if the id is NOT FIRST and NOT LAST in the features_string_array.
	// ANY feature_id could match in this case.
	regex += '[^0-9]' + id + '(?=[^0-9])';
	
	// Matches if the id is the LAST in the features_string_array.
	// Only the LAST feature_id (ascending sort) could match in this case.
	if( position == (len - 1) )
	{
		regex += '|';
		regex += '[^0-9]' + id + '$';
	}
	
	regex += ')';
	
	return regex;
}
function prepareFeaturesSearchRegex()
{
	var selected_features = '';
	var regex = '';
	var checkedBoxes = $('#features_form input[type=checkbox]:checked');

	//Sort the checkboxes by value ASC
	checkedBoxes.sort(function (a, b)
	{
		a = parseInt($(a).attr("value"), 10);
		b = parseInt($(b).attr("value"), 10);

		if(a > b)
		{
			return 1;
		}
		else if(a < b)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	});

	for(var i = 0; i < checkedBoxes.length; ++i)
	{
		if(i != 0)
		{ regex += '.*'; }
		
		regex += generateFeatureIdRegex(checkedBoxes[i].value, i, checkedBoxes.length);
		selected_features += checkedBoxes[i].value + ',';
	}
	
	console.log("selected_features: " + selected_features);
	console.log("regex: " + regex);
	return regex;
}
function filterByFeatures()
{
	var regex = prepareFeaturesSearchRegex();
	if( !regex )
	{ regex = ''; }
	
	$('#all_markers').DataTable().column( HEADER_ROW["features"] ).search(
		regex,
		true,
		false
	).draw();
}

// function filterByFacilityType()
// {
	// var facilityType = $('#facility_type_select').val();
	// if( !facilityType )
	// { facilityType = ''; }
	
	// $('#all_markers').DataTable().column( HEADER_ROW["facility_type"] ).search(
		// facilityType,
		// false,
		// true
	// ).draw();
// }

function filterByTextSearch()
{
	var search = $('#text_search').val();
	if( !search )
	{ search = ''; }
	
	// Search strictly by name column.
	if( $('#text_search_by_name').is(':checked') )
	{
		$('#all_markers').DataTable().column( HEADER_ROW["name"] ).search(
			search,
			false,
			true
		).draw();
	}
	// Search through ALL columns.
	else
	{
		$('#all_markers').DataTable().search(
			search,
			false,
			true
		).draw();
	}
}
function resetSearch(isFixedFeature)
{
	$('#text_search').val('');
	$('#text_search_by_name').attr('checked', true);
	//$('#facility_type_select').val(0);
	$('#features_form input[type=checkbox]:checked').attr('checked', false);
	
	clearAllSearches();
	renderMarkersFromFilteredRows();
	renderSearchInterface(isFixedFeature);
}
function clearAllSearches()
{
	$('#all_markers').DataTable()
		.search( '' )
		.columns().search( '' )
		.draw();
}
function filter(isFixedFeature)
{
	clearAllSearches();
	//filterByFacilityType();
	
	// Only filter by feature if we are on the main page, otherwise we are on a single feature page.
	if(!isFixedFeature)
	{ filterByFeatures(); }
	
	filterByTextSearch();
	
	renderMarkersFromFilteredRows();
	renderSearchInterface(isFixedFeature);
	//$.each(filteredMarkerRows, function(index, row){console.log(row[8]);}); // For debugging features selection
}
