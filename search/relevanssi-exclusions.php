<?php

function rlv_exclude_posts($restriction)
{
	global $wpdb;

	$tag_query="
		SELECT $wpdb->posts.ID
		FROM $wpdb->posts, $wpdb->term_relationships, $wpdb->terms
		WHERE $wpdb->posts.ID = $wpdb->term_relationships.object_id
		AND $wpdb->terms.term_id = $wpdb->term_relationships.term_taxonomy_id
		AND $wpdb->terms.slug = 'exclude-from-search'";

	$restriction .= " AND post.ID NOT IN ($tag_query) ";

//	$category_exclusions = defined('RLV_CATEGORY_EXCLUSIONS') ? RLV_CATEGORY_EXCLUSIONS : null;
//	if($category_exclusions)
//	{
//		$category_query="
//            SELECT $wpdb->posts.ID
//            FROM $wpdb->posts, $wpdb->term_relationships, $wpdb->terms
//            WHERE $wpdb->posts.ID = $wpdb->term_relationships.object_id
//            AND $wpdb->terms.term_id = $wpdb->term_relationships.term_taxonomy_id
//            AND $wpdb->terms.slug = 'exclude-from-search'";
//
//		$restriction .= " AND post.ID NOT IN ($category_query) ";
//	}

	return $restriction;
}
//add_filter('relevanssi_indexing_restriction', 'rlv_exclude_posts');
