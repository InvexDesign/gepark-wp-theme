<div class="result">
	<div class="content">
		<?php include('result-content.php'); ?>
	</div>
	<a class="link" href="<?php echo get_permalink(); ?>">
		<?php include('result-button.php'); ?>
	</a>
</div>
