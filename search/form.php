<form id="search_form" class="search-form" action="<?php echo home_url(); ?>" method="get">
	<input class="search-term" name="s" type="text" value="<?php the_search_query(); ?>" placeholder="Enter a search term" />
	<div class="submit-button" onclick="document.getElementById('search_form').submit();">
		<?php include('form-button.php'); ?>
	</div>
</form>
