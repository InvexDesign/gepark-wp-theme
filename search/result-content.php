<?php
global $post;

if(function_exists('relevanssi_get_the_title')) {
	$title = relevanssi_get_the_title($post->ID);
} else {
	$title = get_the_title();
}

$post_type = get_post_type();
switch($post_type)
{
	case 'tribe_events':
		$post_type = 'Event';
		break;
	case 'attachment':
	  $title = basename(get_attached_file($post->ID));
	  if($post->post_mime_type) {
			$post_type = pathinfo(get_attached_file($post->ID), PATHINFO_EXTENSION);
		}
}
?>
<?php $categories = get_the_category(); ?>
<div class="classification">
	<div class="type" title="Post Type"><?php
		echo preg_replace('/[_\-]/', ' ', $post_type);
	?></div>
	<?php if($categories) : ?>
		<div class="categories" title="Categories">
			<?php foreach($categories as $category) : ?>
				<div class="category"><?php
					echo preg_replace('/[_\-]/', ' ', $category->name);
				?></div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>
<a class="title" href="<?php echo get_permalink(); ?>"><?php
	echo $title;
?></a>
<div class="excerpt">
	<?php if(function_exists('relevanssi_the_excerpt')) : ?>
		<?php relevanssi_the_excerpt(); ?>
	<?php else : ?>
		<?php the_excerpt(); ?>
	<?php endif; ?>
</div>
