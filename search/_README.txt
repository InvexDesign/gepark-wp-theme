Requirements
- all searches must be GET requests to "BASE_WORDPRESS_URL?s=YOUR_SEARCH_TERM"

Installation
- DO NOT CLONE this repo!
- COPY THE FILES FROM THE REPO into a directory in your theme
    - ie "YOUR_THEME_DIRECTORY/search/"
- move "search-example.php" to "YOUR_THEME_DIRECTORY/search.php"
    - the name of the file MUST be EXACTLY "search.php" so that WP will recognize it as the search template.
- customize search.php
    - update $SEARCH_DIRECTORY to reflect the relative path you chose above (ie "search")
    - update layout markup/etc if needed, but maintain the original markup as much as possible
- customize partial files individually if changes are needed
    - ie "head.php", "form-button.php", "result-button.php", etc
- customize your css via "search-custom.css"
- register/include "search.min.css" and "search-custom.css" in head
- make sure all search forms submit a GET request to the base WP url with a single parameter "s"
    - ie if using "searchform.php"
