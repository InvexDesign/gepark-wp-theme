<?php
/*
Template Name: Safety Village
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Safety Village';
		$metaslider_id = 403;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.2459479841114!2d-88.04841668455929!3d41.86605457922311!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52fed6d8784b%3A0xda1564d845825539!2s185+Spring+Ave%2C+Glen+Ellyn%2C+IL+60137!5e0!3m2!1sen!2sus!4v1475700444323&output=embed';
		$sidebar_menu_id = 29;
		$sidebar_widget_area_id = 'safety_village_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
