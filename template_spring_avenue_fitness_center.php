<?php
/*
Template Name: Spring Avenue Fitness Center
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Spring Avenue Fitness Center';
		$metaslider_id = 392;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.237006347486!2d-88.04567868455925!3d41.866246979223135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52f9a907f16f%3A0x75eaf263fc9a3f74!2sSpring+Avenue+Fitness+Center!5e0!3m2!1sen!2sus!4v1475700866550&output=embed';
		$sidebar_menu_id = 32;
		$sidebar_widget_area_id = 'spring_avenue_fitness_center_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
