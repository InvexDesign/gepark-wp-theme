<?php
/*
Template Name: Youth Indoor Soccer
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Youth Indoor Soccer';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/youth-indoor-soccer.png';
		$sidebar_menu_id = 64;
		$sidebar_widget_area_id = 'youth_indoor_soccer_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
