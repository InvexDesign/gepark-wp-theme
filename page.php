<?php get_header(); ?>

<section id="main">
	<div class="banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/spi-calendar.jpg)">
		<div class="banner-inner">
			<h2><span>Events Calendar</span></h2>
		</div>
	</div>
	<div class="content">
		<?php if (is_page('calendar')) echo '<div class="main-col full-width">'; ?>
		<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
				<?php endwhile; ?>
		<?php endif; ?>
		<?php if (is_page('calendar')) echo '</div>'; ?>
	</div>
</section>

<?php get_footer(); ?>
