<?php
/*
Template Name: District News
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'News';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/news.png';
		$sidebar_menu_id = 71;
		$sidebar_widget_area_id = 'news_sidebar_area';

		$show_post_date = true;
		$show_post_date = true;
		$posts_per_page = 20;
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$wp_query_parameters = [
			'posts_per_page' => $posts_per_page,
			'paged'          => $paged,
			'order'          => 'desc',
			'cat'            => 67,
		];

        require_once('_templates/short-banner-with-sidebar-with-blogroll.php'); ?>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
