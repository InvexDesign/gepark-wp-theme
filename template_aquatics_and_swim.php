<?php
/*
Template Name: Aquatics & Swim
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Aquatics & Swim';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/aquatics-and-swim.png';
		$sidebar_menu_id = 44;
		$sidebar_widget_area_id = 'aquatics_and_swim_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
