<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.2.4
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single">

	<p class="tribe-events-back">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html__( 'All %s', 'the-events-calendar' ), $events_label_plural ); ?></a>
	</p>

	<!-- Notices -->
	<?php tribe_the_notices() ?>
	
    <div class="events-col-wrap">
        <!-- Invex: START Main Col -->
        <div class="main-col events-main">
        	<div class="main-col-head">
				<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>
                <h3>
                    <?php the_title(); ?>
                    <span class="date">
                        <?php echo tribe_events_event_schedule_details( $event_id); ?>
                    </span>
                </h3>
            </div>
            <div class="content-wrap">
                <?php if ( tribe_get_cost() ) : ?>
                    <!--<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>-->
                <?php endif; ?>
                <?php while ( have_posts() ) :  the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <!-- Event featured image, but exclude link -->
                        
            
                        <!-- Event content -->
                        <?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
                        <div class="tribe-events-single-event-description tribe-events-content">
                            <?php the_content(); ?>
                        </div>
                        <!-- .tribe-events-single-event-description -->
                        <?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
                    </div>
             </div>
             <!-- Event footer -->
            <div id="tribe-events-footer">
                <!-- Navigation -->
                <ul class="tribe-events-sub-nav">
                    <li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
                    <li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
                </ul>
                <!-- .tribe-events-sub-nav -->
            </div>
            <!-- #tribe-events-footer -->
        </div>
        <!-- Invex: END Main Col -->
    
        <div class="events-sidebar">
                <!-- Event meta -->
            <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
            <?php tribe_get_template_part( 'modules/meta' ); ?>
            <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
                    <?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
                <?php endwhile; ?>
        </div>
    </div>
</div><!-- #tribe-events-content -->
