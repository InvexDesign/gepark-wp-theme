<?php
/*
Template Name: Lake Ellyn Boathouse
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Lake Ellyn Boathouse';
		$metaslider_id = 415;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2970.4444546516193!2d-88.0624251!3d41.8832977!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e531806f5a001%3A0xc43beddeca563825!2sLake+Ellyn+Park!5e0!3m2!1sen!2sus!4v1475699879339&output=embed';
		$sidebar_menu_id = 39;
		$sidebar_widget_area_id = 'lake_ellyn_boathouse_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
