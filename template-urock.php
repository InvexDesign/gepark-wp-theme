<?php
/*
Template Name: U-Rock Competition
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = 'Battle of the Bands';
		$banner_title = 'U-Rock Competition';
		$banner_image_url = 'http://gepark.org/wp-content/uploads/2017/02/banner_urock.jpg';
		require_once('_templates/short-banner-full-width.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
