<?php
/*
Template Name: Spring Avenue Dog Park
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Spring Avenue Dog Park';
		$metaslider_id = 426;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.229570449519!2d-88.04928368455926!3d41.86640697922307!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52fede515999%3A0x683c1f28010d17a7!2sSpring+Avenue+Dog+Park!5e0!3m2!1sen!2sus!4v1475700788318&output=embed';
		$sidebar_menu_id = 31;
		$sidebar_widget_area_id = 'spring_avenue_dog_park_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
