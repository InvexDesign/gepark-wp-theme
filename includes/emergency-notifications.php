<?php

$emergency_notification_status = get_field('emergency-notifications--status', 'option');
switch($emergency_notification_status) {
	case 'published':
		$display_emergency_notification = true;
		break;
	case 'preview':
		$display_emergency_notification = is_user_logged_in();
		break;
	case 'disabled':
	default:
		$display_emergency_notification = false;
}
?>
<?php if($display_emergency_notification) : ?>
	<?php $emergency_notification_content = get_field('emergency-notifications--content', 'option'); ?>
	<div id="emergency">
		<div class="inner-wrap"><?php echo $emergency_notification_content; ?></div>
	</div>
<?php endif;
