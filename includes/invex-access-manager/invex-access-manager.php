<?php

/* TODO: refactor/centralize access logic to single ? */

require_once('non-iwpc-helpers.php');
require_once('custom-post-type-definitions.php');
require_once('admin-sidebar-menu-definitions.php');

/* IMPORTANT: these must come AFTER the definitions above */
require_once('config/user-profiles.php');/* This will be customized per install */

/* IMPORTANT: these must come AFTER the user-profile definitions above */
require_once('config/iam-config.php');/* This will be customized per install */

require_once('access-functions.php');
require_once('restrict-single-standard-post-access.php');
require_once('restrict-standard-post-index.php');
require_once('remove-admin-sidebar-menus.php');
require_once('remove-edit-post-button-from-admin-bar.php');

if(defined('IAM_SHOW_MATRIX') && IAM_SHOW_MATRIX) {
	jsConsoleLog('---- IAM Access Matrix ---');
	jsConsoleLog(json_encode(IAM_ACCESS_MATRIX), true);
	jsConsoleLog('-------');
}

/*
 * Use this for figuring out which slugs to hide
 * */
if(!function_exists('debug_admin_menus')):
	function debug_admin_menus()
	{
		if(!is_admin()) {
			return;
		}
		global $submenu, $menu, $pagenow;
		if(current_user_can('manage_options'))
		{ // ONLY DO THIS FOR ADMIN
			// PRINTS ON DASHBOARD
			if($pagenow == 'index.php')
			{
				echo '<pre>';
				echo print_r($menu);
				echo '</pre>';
				echo '<pre>';
				echo print_r($submenu);
				echo '</pre>';
			}
		}
	}

	if(defined('IAM_DEBUG_SIDEBAR') && IAM_DEBUG_SIDEBAR) {
		add_action('admin_notices', 'debug_admin_menus');
	}
endif;