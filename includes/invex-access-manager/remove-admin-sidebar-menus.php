<?php

function IAM_removeAdminSidebarMenus()
{
	jsConsoleLog("IAM_removeAdminSidebarMenus(), hook=\"admin_init\"");

	$user_id = get_current_user_id();
	jsConsoleLog("user #$user_id");

	if(!defined('IAM_ACCESS_MATRIX'))
	{
		/* TODO: error log */
		jsConsoleLog("---- IAM_ACCESS_MATRIX is undefined, allowing access ----");
		return;
	}

	$IAM = IAM_ACCESS_MATRIX;

	if(isset($IAM[$user_id]))
	{
		if(isset($IAM[$user_id]['hidden_sidebar_menus']))
		{
			foreach($IAM[$user_id]['hidden_sidebar_menus'] as $key => $menu_slug)
			{
				if(is_array($menu_slug))
				{
					foreach($menu_slug as $submenu_slug)
					{
						remove_submenu_page($key, $submenu_slug);
						jsConsoleLog("hid \"$key\" > \"$submenu_slug\" submenu");
					}
				}
				else
				{
					remove_menu_page($menu_slug);
					jsConsoleLog("hid \"$menu_slug\" menu");
				}
			}
		}
		else
		{
			jsConsoleLog("no rules set for hidden_sidebar_menus, no change affected");
		}
	}
	else
	{
		jsConsoleLog("no rules set for user, no change affected");
	}

	jsConsoleLog("----------------");
}
add_action('admin_init', 'IAM_removeAdminSidebarMenus');
