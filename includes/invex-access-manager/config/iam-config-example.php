<?php

//define('IAM_DEBUG_SIDEBAR', true);
//define('IAM_ALLOW_BY_DEFAULT', true);/* TODO: per post type defaults? *//* TODO: this is dangerous to turn to false! */
//define('IAM_JS_CONSOLE_LOG_ENABLED', true);/* IMPORTANT: turn off once done debugging, it will break ajax requests ie MEDIA */
define('IAM_SHOW_MATRIX', false);

define('IAM_ACCESS_MATRIX',
	[
		1 => [
			POST_TYPE_PAGE         => [
				'allow'  => true,
				'except' => [5],
//			'except' => [20],
			],
			POST_TYPE_POST         => [
//			'allow'  => true,
//			'except' => [1],
//			'except' => [770],
//			'except' => [1, 770],
			],
			'tribe_events'         => [
				'allow' => true,
			],
			'hidden_sidebar_menus' => [
				TABLEPRESS_MENU_SLUG => [
					TABLEPRESS_ADD_SUBMENU_SLUG,
					TABLEPRESS_IMPORT_SUBMENU_SLUG,
				]
//			TABLEPRESS_MENU_SLUG,
//			MEDIA_MENU_SLUG,
//			ACCORDIONS_MENU_SLUG,
//			TABS_MENU_SLUG,
//			ACF_MENU_SLUG,
			],
		],
		2 => [
			POST_TYPE_PAGE         => [
				'allow' => true,
			],
			POST_TYPE_POST         => [
				'allow' => false,
			],
			'tribe_events'         => [
				'allow'  => false,
				'except' => [2, 118],
			],
			'hidden_sidebar_menus' => [
				TABLEPRESS_MENU_SLUG
			],
		],
	]
);
