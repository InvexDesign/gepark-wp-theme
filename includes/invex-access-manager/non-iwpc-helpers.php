<?php

function jsConsoleLog($message, $json = false)
{
	if(strpos($_SERVER['REQUEST_URI'], 'ajax') !== false) {
		return;
	}
	if(strpos($_SERVER['REQUEST_URI'], 'async-upload') !== false) {
		return;
	}

	$enabled = defined('JS_CONSOLE_LOG_ENABLED') ? JS_CONSOLE_LOG_ENABLED : false;
	$enabled = defined('IAM_JS_CONSOLE_LOG_ENABLED') ? IAM_JS_CONSOLE_LOG_ENABLED : $enabled;

	if(!$enabled) {
		return;
	}
	?><script>console.log(<?php echo $json ? 'JSON.parse(' : ''; ?>'<?php echo $message; ?>'<?php echo $json ? ')' : ''; ?>);</script><?php
}

function jsAlert($message)
{
	if(strpos($_SERVER['REQUEST_URI'], 'ajax') !== false) {
		return;
	}
	if(strpos($_SERVER['REQUEST_URI'], 'async-upload') !== false) {
		return;
	}

	$enabled = defined('JS_CONSOLE_LOG_ENABLED') ? JS_CONSOLE_LOG_ENABLED : false;
	$enabled = defined('IAM_JS_CONSOLE_LOG_ENABLED') ? IAM_JS_CONSOLE_LOG_ENABLED : $enabled;

	if(!$enabled) {
		return;
	}
	?><script>alert('<?php echo $message; ?>');</script><?php
}