<?php

/*
 *
 * This is a list of supported/removable admin sidebar menus/submenus
 *
 * */

define('IAM_HIDDEN_SIDEBAR_MENUS_KEY', 'hidden_sidebar_menus');

define('GLOBAL_SETTINGS_MENU_SLUG', 'global-settings');
define('POSTS_MENU_SLUG', 'edit.php');
define('PAGES_MENU_SLUG', 'edit.php?post_type=page');
define('COMMENTS_MENU_SLUG', 'edit-comments.php');
define('APPEARANCE_MENU_SLUG', 'themes.php');
define('PLUGINS_MENU_SLUG', 'plugins.php');
define('USERS_MENU_SLUG', 'users.php');
define('TOOLS_MENU_SLUG', 'tools.php');
define('SETTINGS_MENU_SLUG', 'options-general.php');
define('PROFILE_MENU_SLUG', 'profile.php');

define('TABLEPRESS_MENU_SLUG', 'tablepress');
define('TABLEPRESS_ADD_SUBMENU_SLUG', 'tablepress_add');
define('TABLEPRESS_INDEX_SUBMENU_SLUG', 'tablepress');
define('TABLEPRESS_IMPORT_SUBMENU_SLUG', 'tablepress_import');
define('TABLEPRESS_EXPORT_SUBMENU_SLUG', 'tablepress_export');
define('TABLEPRESS_OPTIONS_SUBMENU_SLUG', 'tablepress_options');
define('TABLEPRESS_ABOUT_SUBMENU_SLUG', 'tablepress_about');

define('MEDIA_MENU_SLUG', 'upload.php');
define('MEDIA_LIBRARY_SUBMENU_SLUG', 'upload.php');
define('MEDIA_ADD_NEW_SUBMENU_SLUG', 'media-new.php');

define('ACCORDIONS_MENU_SLUG', 'edit.php?post_type=accordion');
define('ACCORDIONS_INDEX_SUBMENU_SLUG', 'edit.php?post_type=accordion');
define('ACCORDIONS_ADD_NEW_SUBMENU_SLUG', 'post-new.php?post_type=accordion');

define('TABS_MENU_SLUG', 'edit.php?post_type=tab');
define('TABS_INDEX_SUBMENU_SLUG', 'edit.php?post_type=tab');
define('TABS_ADD_NEW_SUBMENU_SLUG', 'post-new.php?post_type=tab');

define('ACF_MENU_SLUG', 'edit.php?post_type=acf-field-group');
define('ACF_FIELD_GROUPS_SUBMENU_SLUG', 'edit.php?post_type=acf-field-group');
define('ACF_ADD_NEW_SUBMENU_SLUG', 'post-new.php?post_type=acf-field-group');
define('ACF_TOOLS_SUBMENU_SLUG', 'acf-tools');
define('ACF_UPDATES_SUBMENU_SLUG', 'acf-settings-updates');

define('TRIBE_EVENTS_MENU_SLUG', 'edit.php?post_type=tribe_events');

define('FORMIDABLE_MENU_SLUG', 'formidable');

define('CSPARKS_BIDS_PROJECTS_MENU_SLUG', 'edit.php?post_type=bid-project');
define('RESPONSIVE_LIGHTBOX_GALLERY_GALLERY_MENU_SLUG', 'edit.php?post_type=rl_gallery');
define('RESPONSIVE_LIGHTBOX_GALLERY_LIGHTBOX_MENU_SLUG', 'responsive-lightbox-settings');
define('TIMETABLE_EVENT_SCHEDULE_MENU_SLUG', 'edit.php?post_type=mp-event');
define('TIMELINE_EVENTS_MENU_SLUG', 'edit.php?post_type=timeline_event');
define('HUSTLE_MENU_SLUG', 'hustle');
define('LOGOS_SHOWCASE_MENU_SLUG', 'edit.php?post_type=lshowcase');
define('ST_EVENT_CALENDAR_MENU_SLUG', 'stec_menu__general');
define('WP_MAIL_SMTP_MENU_SLUG', 'wp-mail-smtp');
define('YOAST_SEO_MENU_SLUG', 'wpseo_dashboard');
define('AAM_MENU_SLUG', 'aam');
define('ADD_THIS_MENU_SLUG', 'addthis_registration');
define('ITHEMES_SECURITY_MENU_SLUG', 'itsec');
define('WORDFENCE_MENU_SLUG', 'Wordfence');
define('METASLIDER_MENU_SLUG', 'metaslider');
