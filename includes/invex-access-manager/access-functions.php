<?php

function canUserAccessPostId($user_id, $post_id)
{
	jsConsoleLog("canUserAccessPostId()");
	jsConsoleLog("user #$user_id");
	jsConsoleLog("post_id #$post_id");

	if(!defined('IAM_ACCESS_MATRIX'))
	{
		/* TODO: error log */
		jsConsoleLog("---- IAM_ACCESS_MATRIX is undefined, allowing access ----");
		return true;
	}

	$IAM = IAM_ACCESS_MATRIX;
	$allow_by_default = defined('IAM_ALLOW_BY_DEFAULT') ? IAM_ALLOW_BY_DEFAULT : true;
	$allow = $allow_by_default;

	$post_type = get_post_type($post_id);

	if(!$post_id)
	{
		jsConsoleLog("no post id set - no access limits should be enforced");
		return true;
	}

	if(isset($IAM[$user_id]))
	{
		jsConsoleLog("post_type \"$post_type\"");

		if(isset($IAM[$user_id][$post_type]))
		{
			/* TODO: error checking - allow is NON BOOLEAN */
			if(isset($IAM[$user_id][$post_type]['allow']))
			{
				if($IAM[$user_id][$post_type]['allow'])
				{/* ALLOW */
					jsConsoleLog("allow all $post_type");
					$allow = true;

					/* TODO: error checking - except is NON ARRAY */
					/* TODO: error checking - except has NON INTEGER array values */
					if(isset($IAM[$user_id][$post_type]['except']) && is_array($IAM[$user_id][$post_type]['except']))
					{/* ALLOW - Exceptions */
						jsConsoleLog("allow except - \[" . implode(',', $IAM[$user_id][$post_type]['except']) . "\]");

						if(in_array($post_id, $IAM[$user_id][$post_type]['except']))
						{/* ALLOW - Exceptions - Match */
							jsConsoleLog("allow except MATCH #$post_id");
							$allow = false;
						}
						else
						{/* ALLOW - Exceptions - No Match */
							jsConsoleLog("allow except NO-MATCH #$post_id");
							$allow = true;
						}
					}
					else
					{/* ALLOW - Exceptions - None Set */
						jsConsoleLog("allow has no exceptions");
					}
				}
				else
				{/* DENY */
					jsConsoleLog("deny all $post_type");
					$allow = false;

					/* TODO: error checking - except is NON ARRAY */
					/* TODO: error checking - except has NON INTEGER array values */
					if(isset($IAM[$user_id][$post_type]['except']) && is_array($IAM[$user_id][$post_type]['except']))
					{/* DENY - Exceptions */
						jsConsoleLog("deny except - \[" . implode(',', $IAM[$user_id][$post_type]['except']) . "\]");

						if(in_array($post_id, $IAM[$user_id][$post_type]['except']))
						{/* DENY - Exceptions - Match */
							jsConsoleLog("deny except MATCH #$post_id");
							$allow = true;
						}
						else
						{/* DENY - Exceptions - No Match */
							jsConsoleLog("deny except NO-MATCH #$post_id");
							$allow = false;
						}
					}
					else
					{/* DENY - Exceptions - None Set */
						jsConsoleLog("deny has no exceptions");
					}
				}
			}
			else
			{
				jsConsoleLog("MALFORMED - \"allow\" not set");
				/* TODO: fire error/log */
			}
		}
		else
		{
			jsConsoleLog("no rules set for post type ($post_type), default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
		}
	}
	else
	{
		jsConsoleLog("no rules set for user, default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
	}

	jsConsoleLog("---- " . ($allow ? 'ALLOWED' : 'DENIED') . " ----");

	return $allow;
}

/* TODO: consider removing this? is it ever used? would it ever be used? */
function canUserAccessPostType($user_id, $post_type)
{
	jsConsoleLog("canUserAccessPostType()");
	jsConsoleLog("user #$user_id");
	jsConsoleLog("post_type \"$post_type\"");

	if(!defined('IAM_ACCESS_MATRIX'))
	{
		/* TODO: error log */
		jsConsoleLog("---- IAM_ACCESS_MATRIX is undefined, allowing access ----");
		return true;
	}

	$IAM = IAM_ACCESS_MATRIX;
	$allow_by_default = defined('IAM_ALLOW_BY_DEFAULT') ? IAM_ALLOW_BY_DEFAULT : true;
	$allow = $allow_by_default;

	if(isset($IAM[$user_id]))
	{
		if(isset($IAM[$user_id][$post_type]))
		{
			/* TODO: error checking - allow is NON BOOLEAN */
			if(isset($IAM[$user_id][$post_type]['allow']))
			{
				if($IAM[$user_id][$post_type]['allow'])
				{
					jsConsoleLog("allow all $post_type");
					$allow = true;
				}
				else
				{
					jsConsoleLog("deny all $post_type");
					$allow = false;
				}
			}
			else
			{
				jsConsoleLog("MALFORMED - \"allow\" not set");
				/* TODO: fire error/log */
			}
		}
		else
		{
			jsConsoleLog("no rules set for post type ($post_type), default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
		}
	}
	else
	{
		jsConsoleLog("no rules set for user, default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
	}

	jsConsoleLog("---- " . ($allow ? 'ALLOWED' : 'DENIED') . " ----");

	return $allow;
}

/* TODO: consider removing this? is it ever used? would it ever be used? */
function canUserViewSidebarMenuGroup($user_id, $menu_slug)
{
	jsConsoleLog("canUserViewSidebarMenuGroup()");
	jsConsoleLog("user #$user_id");
	jsConsoleLog("menu_slug \"$menu_slug\"");

	if(!defined('IAM_ACCESS_MATRIX'))
	{
		/* TODO: error log */
		jsConsoleLog("---- IAM_ACCESS_MATRIX is undefined, allowing access ----");
		return true;
	}

	$IAM = IAM_ACCESS_MATRIX;
	$allow_by_default = defined('IAM_ALLOW_BY_DEFAULT') ? IAM_ALLOW_BY_DEFAULT : true;
	$allow = $allow_by_default;

	if(isset($IAM[$user_id]))
	{
		if(isset($IAM[$user_id][$menu_slug]))
		{
			/* TODO: error checking - allow is NON BOOLEAN */
			if(isset($IAM[$user_id][$menu_slug]['allow']))
			{
				if($IAM[$user_id][$menu_slug]['allow'])
				{/* ALLOW */
					jsConsoleLog("allow all $menu_slug");
					$allow = true;
				}
				else
				{/* DENY */
					jsConsoleLog("deny all $menu_slug");
					$allow = false;
				}
			}
			else
			{
				jsConsoleLog("MALFORMED - \"allow\" not set");
				jsConsoleLog("no rules set for post type ($menu_slug), default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
				/* TODO: fire error/log */
			}
		}
		else
		{
			jsConsoleLog("no rules set for post type ($menu_slug), default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
		}
	}
	else
	{
		jsConsoleLog("no rules set for user, default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
	}

	jsConsoleLog("---- " . ($allow ? 'ALLOWED' : 'DENIED') . " ----");

	return $allow;
}
