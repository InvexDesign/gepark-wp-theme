<?php

/* POST INDEX - BACKEND */
function IAM_excludePostsFromPostIndex($query)
{
	global $pagenow;

	/* TODO: test update for post type scopes */

	if(is_admin() && $pagenow == 'edit.php')
	{
		jsConsoleLog("IAM_excludePostsFromPostIndex(), hook=\"parse_query\"");
		jsConsoleLog("edit.php matched");

		if(!defined('IAM_ACCESS_MATRIX'))
		{
			/* TODO: error log */
			jsConsoleLog("---- IAM_ACCESS_MATRIX is undefined, allowing access ----");
			return;
		}

		$IAM = IAM_ACCESS_MATRIX;
		$allow_by_default = defined('IAM_ALLOW_BY_DEFAULT') ? IAM_ALLOW_BY_DEFAULT : true;
		$allow = $allow_by_default;

		$user_id = get_current_user_id();
		jsConsoleLog("user #$user_id");

		if(isset($IAM[$user_id]))
		{
			$post_type = $query->query['post_type'];
			jsConsoleLog("post_type \"$post_type\"");

			if(isset($IAM[$user_id][$post_type]))
			{
				/* TODO: error checking - allow is NON BOOLEAN */
				if(isset($IAM[$user_id][$post_type]['allow']))
				{
					if($IAM[$user_id][$post_type]['allow'])
					{/* ALLOW */
						jsConsoleLog("allow all $post_type");
						$allow = true;

						/* TODO: error checking - except is NON ARRAY */
						/* TODO: error checking - except has NON INTEGER array values */
						if(isset($IAM[$user_id][$post_type]['except']) && is_array($IAM[$user_id][$post_type]['except']))
						{/* ALLOW - Exceptions */
							jsConsoleLog("allow except - \[" . implode(',', $IAM[$user_id][$post_type]['except']) . "\]");
							jsConsoleLog("---- query exclude \[" . implode(',', $IAM[$user_id][$post_type]['except']) . "\] ----");
							$query->query_vars['post__not_in'] = $IAM[$user_id][$post_type]['except'];

							return;
						}
						else
						{/* ALLOW - Exceptions - None Set */
							jsConsoleLog("allow has no exceptions");
						}
					}
					else
					{/* DENY */
						jsConsoleLog("deny all $post_type");
						$allow = false;

						/* TODO: error checking - except is NON ARRAY */
						/* TODO: error checking - except has NON INTEGER array values */
						if(isset($IAM[$user_id][$post_type]['except']) && is_array($IAM[$user_id][$post_type]['except']))
						{/* DENY - Exceptions */
							jsConsoleLog("deny except - \[" . implode(',', $IAM[$user_id][$post_type]['except']) . "\]");
							jsConsoleLog("---- query include only \[" . implode(',', $IAM[$user_id][$post_type]['except']) . "\] ----");
							$query->query_vars['post__in'] = $IAM[$user_id][$post_type]['except'];

							return;
						}
						else
						{/* DENY - Exceptions - None Set */
							jsConsoleLog("deny has no exceptions");
						}
					}
				}
				else
				{
					jsConsoleLog("MALFORMED - \"allow\" not set");
					jsConsoleLog("no rules set for post type ($post_type), default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
					/* TODO: fire error/log */
				}
			}
			else
			{
				jsConsoleLog("no rules set for post type ($post_type), default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
			}
		}
		else
		{
			jsConsoleLog("no rules set for user, default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ")");
		}


		jsConsoleLog("---- " . ($allow ? 'ALLOWED' : 'DENIED') . " ----");

		if(!$allow)
		{
			$query->query_vars['post__in'] = ['-999'];
		}
	}
}
add_filter('parse_query', 'IAM_excludePostsFromPostIndex');
