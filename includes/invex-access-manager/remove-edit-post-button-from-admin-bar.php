<?php

/* WP ADMIN BAR - EDIT - FRONTEND: If user is editing post on the frontend, remove the "Edit Post" button from the WP Admin Bar */
function IAM_removeAdminBarEditPostButton($wp_admin_bar)
{
	jsConsoleLog("IAM_removeAdminBarEditPostButton(), hook=\"admin_bar_menu\"");

	$user_id = get_current_user_id();
	jsConsoleLog("user #$user_id");

	global $post;
	$post_id = $post->ID;
	jsConsoleLog("post #$post_id");

	if(!canUserAccessPostId($user_id, $post_id)) {
		$wp_admin_bar->remove_node('edit');
	}

	jsConsoleLog("----------------");
}
add_action('admin_bar_menu', 'IAM_removeAdminBarEditPostButton', 999);
