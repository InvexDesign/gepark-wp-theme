<?php

/* POST EDIT - BACKEND */
function IAM_restrictSinglePostAccess()
{
	jsConsoleLog("IAM_restrictSinglePostAccess(), hook=\"load-post.php\"");

	$user_id = get_current_user_id();
	jsConsoleLog("user #$user_id");

	$allow_by_default = defined('IAM_ALLOW_BY_DEFAULT') ? IAM_ALLOW_BY_DEFAULT : true;
	$allow = $allow_by_default;

	if($_GET['post'])
	{
		$post_id = intval($_GET['post']);
		jsConsoleLog("post #$post_id");

		$allow = canUserAccessPostId($user_id, $post_id);
	}
	else
	{
		jsConsoleLog("---- no post detected, default access given (" . ($allow_by_default ? 'allowed' : 'denied') . ") ----");
	}

	jsConsoleLog("---- " . ($allow ? 'ALLOWED' : 'DENIED') . " ----");

	if(!$allow)
	{
		/* TODO: enable redirect */
//		wp_redirect(home_url() . '/wp-admin?asd=123');
		jsAlert("---- DENIED ACCESS ----");
	}
}
add_action('load-post.php', 'IAM_restrictSinglePostAccess');
