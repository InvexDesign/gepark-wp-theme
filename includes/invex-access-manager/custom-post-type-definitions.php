<?php

/*
 *
 * This is a list of supported/restrictable post types
 *
 * */

define('POST_TYPE_PAGE', 'page');
define('POST_TYPE_POST', 'post');


/*
 * TODO: POST TYPES
tribe_events (The Events Calendar),
bid-project (Bids & Projects),
mp-event (Timeline),
rl_gallery (Gallery),
*/
