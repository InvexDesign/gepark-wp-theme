<?php

function enqueue_admin_scripts($hook)
{
	wp_enqueue_script('custom-admin', get_bloginfo('template_directory') . '/assets/js/wp-admin.js');
}
add_action('admin_enqueue_scripts', 'enqueue_admin_scripts');