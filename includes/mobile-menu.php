<div class="mobile-menu">
	<div class="wrapper">
		<div class="header">
			<span class="title">Menu</span>
			<span class="separator"></span>
			<i class="fa fa-times-circle icon close" aria-hidden="true"></i>
		</div>
		<div class="content">
			<div class="icon-menu">
				<a class="link" href="<?php echo home_url('/'); ?>">
					<i class="fa fa-home icon"></i>
					<span>Home</span>
				</a>
				<a class="link" href="<?php echo REGISTRATION_URL; ?>" target="_blank">
					<i class="fa fa-mouse-pointer icon" aria-hidden="true"></i>
					<span>Register Online</span>
				</a>
				<a class="link" href="<?php echo home_url('/'); ?>activity-guide">
					<i class="fa fa-file-text icon" aria-hidden="true"></i>
					<span>Activity Guide</span>
				</a>
				<a class="link" href="<?php echo home_url('/'); ?>rainout-line/">
					<i class="fa fa-bolt icon" aria-hidden="true"></i>
					<span>Rainout Line</span>
				</a>
				<a class="link" href="<?php echo home_url('/'); ?>contact/">
					<i class="fa fa-envelope icon" aria-hidden="true"></i>
					<span>Contact</span>
				</a>
			</div>
			<div class="accordion-menu">
		  	<?php renderMobileMenuAccordion('About', 'MOBILE_ABOUT_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('Parks & Facilities', 'MOBILE_PARKS_FACILITIES_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('Recreation', 'MOBILE_RECREATION_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('Athletics', 'MOBILE_ATHLETICS_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('Registration', 'MOBILE_REGISTRATION_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('Events', 'MOBILE_EVENTS_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('News', 'MOBILE_NEWS_MENU_ID'); ?>
		  	<?php renderMobileMenuAccordion('Get Involved', 'MOBILE_GET_INVOLVED_MENU_ID'); ?>
			</div>
			<div class="social">
				<a href="<?php echo FACEBOOK_URL; ?>" class="facebook" target="_blank" title="Like GEPD on Facebook">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="<?php echo TWITTER_URL; ?>" class="twitter" target="_blank" title="Follow GEPD on Twitter">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
				<a href="<?php echo INSTAGRAM_URL; ?>" class="instagram" target="_blank" title="Find GEPD on Instagram">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function()
	{
		$('.accordion-menu > .item > .head').click(function()
		{
			$(this).closest('.item').toggleClass('open');
		});

		$('.mobile-menu .close').click(function()
		{
			$(this).closest('.mobile-menu').removeClass('open');
		});

		$('.mobile-menu-button').click(function()
		{
			$('.mobile-menu').addClass('open');
		});
	});
</script>