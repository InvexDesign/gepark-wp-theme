<?php
	// Set Global Vars
	define('HOME_URL', 'https://gepark.org/');
	define('EMPLOYMENT_URL', 'https://www.applitrack.com/gepark/onlineapp/');
	define('REGISTRATION_URL', 'https://apm.activecommunities.com/gepark/Home');
	define('GUIDE_URL', 'https://gepark.org/gamma/activity-guide/');
	define('FACEBOOK_URL', 'https://www.facebook.com/glenellynparks');
	define('TWITTER_URL', 'http://www.twitter.com/glenellynparks');
	define('INSTAGRAM_URL', 'https://www.instagram.com/glenellynparks/');
