<?php

add_filter('acf/settings/remove_wp_meta_box', '__return_false');

if(function_exists('acf_add_options_page'))
{
	acf_add_options_page([
		'page_title' => 'Global Settings',
		'menu_title' => 'Global Settings',
		'menu_slug'  => 'global-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	]);
}

function getAdvancedCustomFieldValue($field, $default = null, $post_id = false)
{
	$field = get_field_object($field, $post_id);
	if($field && $field['value'])
	{
		return $field['value'];
	}

	return $default;
}
