<?php

function _heroBanner($post_id = null)
{
	if(!function_exists('get_field')) {
		error_log('_heroBanner() FAILED - ACF is required');
		return;
	}

	$post_id = $post_id ?: get_the_ID();

	$hero_banner = get_field('hero_banner', $post_id);
	if(!$hero_banner) {
		error_log('_heroBanner() FAILED - missing field "hero_banner"');
		return;
	}

	$max_inheritance_count = 10;//NOTE: limit
	$current_post_id = $post_id;
	if($hero_banner['inherit_from_parent'])
	{
		while($hero_banner && $hero_banner['inherit_from_parent'])
		{
			if($max_inheritance_count <= 0) {
				error_log("_heroBanner(): while loop/max_inheritance_count limit reached \"$max_inheritance_count\", something isnt right...");
				break;
			}

			$parent_post_id = wp_get_post_parent_id($current_post_id);
			if(!$parent_post_id) {
				break;
			}

			$parent_hero_banner = get_field('hero_banner', $parent_post_id);
			if(!$parent_hero_banner) {
				break;
			}

			$current_post_id = $parent_post_id;
			$hero_banner = $parent_hero_banner;
			$max_inheritance_count--;
		}
	}

	include(get_stylesheet_directory() . '/_templates/_partials/hero-banner.php');
}

function _sidebar($post_id = null)
{
	if(!function_exists('get_field')) {
		error_log('_sidebar() FAILED - ACF is required');
		return;
	}

	$post_id = $post_id ?: get_the_ID();

	$sidebar = get_field('sidebar', $post_id);
	if(!$sidebar) {
		error_log('_sidebar() FAILED - missing field "sidebar"');
		return;
	}

	include(get_stylesheet_directory() . '/_templates/_partials/smart-sidebar.php');
}

function _shareThis()
{
	include(get_template_directory() . '/_templates/_partials/sharethis.php');
}

function _notificationBanner()
{
	include(get_template_directory() . '/_templates/_partials/notification-banner.php');
}
