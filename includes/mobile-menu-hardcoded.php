<div class="mobile-menu">
	<div class="wrapper">
		<div class="header">
			<span class="title">Menu</span>
			<span class="separator"></span>
			<i class="fa fa-times-circle icon close" aria-hidden="true"></i>
		</div>
		<div class="content">
			<div class="icon-menu">
				<a class="link" href="<?php echo home_url('/'); ?>">
					<i class="fa fa-home icon"></i>
					<span>Home</span>
				</a>
				<a class="link" href="<?php echo REGISTRATION_URL; ?>" target="_blank">
					<i class="fa fa-mouse-pointer icon" aria-hidden="true"></i>
					<span>Register Online</span>
				</a>
				<a class="link" href="<?php echo home_url('/'); ?>activity-guide">
					<i class="fa fa-file-text icon" aria-hidden="true"></i>
					<span>Activity Guide</span>
				</a>
				<a class="link" href="<?php echo home_url('/'); ?>rainout-line/">
					<i class="fa fa-bolt icon" aria-hidden="true"></i>
					<span>Rainout Line</span>
				</a>
				<a class="link" href="<?php echo home_url('/'); ?>contact/">
					<i class="fa fa-envelope icon" aria-hidden="true"></i>
					<span>Contact</span>
				</a>
			</div>
			<div class="accordion-menu">
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>About</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo home_url('/'); ?>about">About the Park District</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/annual-reports">Annual Reports</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/best-of-glen-ellyn">Best of Glen Ellyn</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/bids-rfps">Bids &amp; RFPs</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/board-of-commissioners">Board of Commissioners</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/board-meetings">Board Meetings</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/budget-financial-information">Budget &amp; Financial Information</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/capital-projects">Capital Projects &amp; Improvements</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/contracts-agreements">Contracts &amp; Agreements</a>
						<a class="link" href="<?php echo EMPLOYMENT_URL; ?>" target="_blank">Employment <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>about/foia-requests">FOIA Requests</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/green-initiatives">Green Initiatives</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/resident-advisory-committees">Resident Advisory Committees</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/plans-surveys">Plans, Surveys &amp; Reports</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/policies-ordinances">Policies &amp; Ordinances</a>
						<a class="link" href="<?php echo home_url('/'); ?>about/staff-directory">Staff Directory</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>Parks & Facilities</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/interactive-map/"><span style="display: inline-block; width: 20px; text-align: center;"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span> Interactive Map</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/interactive-map/#list_tab"><span style="display: inline-block; width: 20px; text-align: center;"><i class="fa fa-map-signs fa-lg" aria-hidden="true"></i></span> Park Directory</a>
						<a class="link" href="<?php echo home_url('/'); ?>parties-rentals/birthday-parties"><span style="display: inline-block; width: 20px; text-align: center;"><i class="fa fa-birthday-cake fa-lg" aria-hidden="true"></i></span> Parties &amp; Rentals</a>
						<a class="link" href="<?php echo home_url('/'); ?>boating-fishing"><span style="display: inline-block; width: 20px; text-align: center;"><i class="fa fa-life-ring fa-lg" aria-hidden="true"></i></span> Boating &amp; Fishing</a>
						<a class="link" href="http://ackermansfc.com" target="_blank">Ackerman SFC <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/clay-tennis-courts/">Clay Tennis Courts</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/holes-knolls/">Holes &amp; Knolls Mini Golf</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/lake-ellyn-boathouse/">Lake Ellyn Boathouse</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/main-street-recreation-center/">Main Street Rec Center</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/maintenance-facility/">Maintenance Facility</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/maryknoll-clubhouse/">Maryknoll Clubhouse</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/platform-tennis-courts/">Platform Tennis Courts &amp; Hut</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/safety-village/">Safety Village</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/skate-park/">Skate Park</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/splash-park/">Splash Park</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/spring-avenue-recreation-center/">Spring Avenue Rec Center</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/spring-avenue-dog-park/">Spring Avenue Dog Park</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/spring-avenue-fitness-center/">Spring Avenue Fitness Center</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/sunset-pool/">Sunset Pool</a>
						<a class="link" href="<?php echo home_url('/'); ?>parks-facilities/village-green-garden-plots/">Village Green Garden Plots</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>Recreation</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo home_url('/'); ?>recreation/age-groups/adults-seniors/">Adults &amp; Seniors</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/adventure-time/">Adventure Time</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/aquatics-swim/">Aquatics &amp; Swim</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/camps/">Camps</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/dance-academy/">Dance Academy</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/day-trips/">Day Trips</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/fitness/">Fitness</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/gymnastics/">Gymnastics</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/hockey-ice-skating/">Hockey &amp; Youth Ice Skating</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/outdoor-skating-sledding/">Outdoor Skating &amp; Sledding</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/martial-arts/">Martial Arts</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/nature/">Nature</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/platform-tennis/">Platform Tennis</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/preschool-education/">Preschool Education</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/tennis/">Tennis</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>Athletics</span>
					</div>
					<div class="content">
						<a class="link" href="http://geyouthrugby.org/" target="_blank">Boys Rugby <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="http://gebulldogs.com/" target="_blank">Bulldogs Lacrosse <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/field-hockey/">Field Hockey</a>
						<a class="link" href="http://www.gegators.com" target="_blank">Gators Swim Team <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/girls-lacrosse/">Girls Lacrosse</a>
						<a class="link" href="http://glenellynsoftball.com/" target="_blank">Girls Softball <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="http://www.glenellynfootball.com/" target="_blank">Golden Eagles Football <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="https://sites.google.com/site/glenellynfootball/cheer" target="_blank">Golden Eagles Cheer <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="http://glenellynbaseball.com/" target="_blank">Youth Baseball <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/youth-soccer/">Youth Soccer</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/youth-leagues/">Youth Leagues</a>
						<a class="link" href="<?php echo home_url('/'); ?>recreation/adult-leagues/">Adult Leagues</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>Registration</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo REGISTRATION_URL; ?>" target="_blank">Register Now <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>registration/how-to-register">How To Register</a>
						<a class="link" href="<?php echo home_url('/'); ?>activity-guide">Virtual Activity Guide</a>
						<a class="link" href="<?php echo home_url('/'); ?>registration/forms-applications">Forms &amp; Applications</a>
						<a class="link" href="<?php echo home_url('/'); ?>registration/financial-aid">Financial Aid</a>
						<a class="link" href="<?php echo home_url('/'); ?>activity-guide-redelivery-requests/">Request Redelivery</a>
						<a class="link" href="<?php echo home_url('/'); ?>registration/special-needs-inclusion">Special Needs &amp; Inclusion</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>Events</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo home_url('/'); ?>calendar/"><i class="fa fa-calendar" aria-hidden="true"></i> Calendar</a>
						<a class="link" href="http://www.ackermansfc.com/open-gym-turf-schedule/" target="_blank">Open Gym &amp; Turf Schedules <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>events/eggs-trordinary-egg-hunt">Eggs-traordinary Egg Hunt</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/touch-a-truck">Touch-a-Truck</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/movies-in-the-park">Movies in the Park</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/jazz-in-the-park">Jazz in the Park</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/2k-kids-fun-run">2K For Kids Fun Run</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/lake-ellyn-cardboard-regatta">Lake Ellyn Cardboard Regatta</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/afternoon-festivities">Afternoon Festivities</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/freedom-4-mile-run">Freedom Four 4-Mile Run</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/family-fun-night">Family Fun Night</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/boo-bash">Boo Bash</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/glow-in-the-park">Glow in the Park Lantern Walk</a>
						<a class="link" href="<?php echo home_url('/'); ?>events/pumpkin-flotilla-fest">Pumpkin Flotilla Fest</a>
						<a class="link" href="https://gepark.ejoinme.org/fallfete" target="_blank">Fall Fete <i class="fa fa-external-link" aria-hidden="true"></i></a>
						<a class="link" href="<?php echo home_url('/'); ?>events/turkey-trot">Turkey Trot</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>News</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo home_url('/'); ?>news/district">District News</a>
						<a class="link" href="<?php echo home_url('/'); ?>news/enews">eNewsletters</a>
						<a class="link" href="<?php echo home_url('/'); ?>media">Media Gallery</a>
					</div>
				</div>
				<div class="item">
					<div class="head">
						<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
						<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
						<span>Get Involved</span>
					</div>
					<div class="content">
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/adopt-a-park/">Adopt-A-Park</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/friends-glen-ellyn-parks/">Friends of Glen Ellyn Parks</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/garden-for-giving/">Gardens For Giving</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/make-a-donation/">Make A Donation</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/restoration-work-days/">Restoration Work Days</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/sponsorship-advertising/">Sponsorship &amp; Advertising</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/tree-bench-dedication/">Tree &amp; Bench Dedication</a>
						<a class="link" href="<?php echo home_url('/'); ?>get-involved/volunteer/">Volunteer</a>
					</div>
				</div>
			</div>
			<div class="social">
				<a href="<?php echo FACEBOOK_URL; ?>" class="facebook" target="_blank" title="Like GEPD on Facebook">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="<?php echo TWITTER_URL; ?>" class="twitter" target="_blank" title="Follow GEPD on Twitter">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
				<a href="<?php echo INSTAGRAM_URL; ?>" class="instagram" target="_blank" title="Find GEPD on Instagram">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function()
	{
		$('.accordion-menu > .item > .head').click(function()
		{
			$(this).closest('.item').toggleClass('open');
		});

		$('.mobile-menu .close').click(function()
		{
			$(this).closest('.mobile-menu').removeClass('open');
		});

		$('.mobile-menu-button').click(function()
		{
			$('.mobile-menu').addClass('open');
		});
	});
</script>