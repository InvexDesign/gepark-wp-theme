<?php
/*
Template Name: Skate Park
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Skate Park';
		$metaslider_id = 400;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.321560172492!2d-88.0599694845593!3d41.864427579223175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e5307713fbd3b%3A0xb3eee215773a9409!2s707+Fairview+Ave%2C+Glen+Ellyn%2C+IL+60137!5e0!3m2!1sen!2sus!4v1475700527255&output=embed';
		$sidebar_menu_id = 28;
		$sidebar_widget_area_id = 'skate_park_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
