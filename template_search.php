<?php
/*
Template Name: Search
*/
?>

<?php get_header(); ?>

<?php
if(isset($_POST['query']))
{
	$today = date("Y-m-d");
	$category_not_in = defined('SEARCH_EXCLUDE_CATEGORIES') ? explode(',', SEARCH_EXCLUDE_CATEGORIES) : [];
	$tag_not_in = defined('SEARCH_EXCLUDE_TAGS') ? explode(',', SEARCH_EXCLUDE_TAGS) : [];

	$events_query = new WP_Query([
		'post_type'        => [TribeEvents::POSTTYPE],
//	  'category__not_in' => $category_not_in,
		'tag__not_in' 		 => $tag_not_in,
		's'                => $_POST['query'],
		'post_status'      => 'publish',
		'meta_key'         => '_EventStartDate',
		'orderby'          => '_EventStartDate',
		'order'            => 'ASC',
		'eventDisplay'     => 'custom',
		'meta_query'       => [
			[
				'key'     => '_EventStartDate',
				'value'   => $today,
				'compare' => '>=',
			]
		],
		'posts_per_page'   => '-1',
	]);

	$posts_query = new WP_Query([
		'post_type'        => 'post',
		'category__not_in' => $category_not_in,
		'tag__not_in' 		 => $tag_not_in,
		's'                => $_POST['query'],
		'post_status'      => 'publish',
		'orderby'          => 'title',
		'order'            => 'ASC',
		'posts_per_page'   => '-1',
	]);

	$pages_query = new WP_Query([
		'post_type'        => 'page',
//		'category__not_in' => $category_not_in,
		'tag__not_in' 		 => $tag_not_in,
		's'                => $_POST['query'],
		'post_status'      => 'publish',
		'orderby'          => 'title',
		'order'            => 'ASC',
		'posts_per_page'   => '-1',
	]);
}
?>
<style>
	.thing {
		text-align: center;
	}
	.thing p {
		display: inline-block;
	}
	.thing .search-results-links {
		display: none;
		margin-bottom: 20px;
	}
	.search-results-columns {
		display: flex;
	}
	.search-results-columns .column {
		flex: 1;
		margin: 0 10px;
	}
	.content .main-col form {
		margin-bottom: 0;
	}
	@media (max-width: 900px) {
		.thing .search-results-links {
			display: inline-block;
		}
		.search-results-columns {
			flex-direction: column;
		}
	}
</style>
<section id="main">
	<?php
		$banner_title = "Search";
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-search.png';
		include('_templates/_partials/short-banner.php'); ?>
		<div class="content">
			<div class="main-col full-width">
				<h3><?php echo isset($_POST['query']) ? 'Search Results' : 'Search'; ?> <?php include('_templates/_partials/sharethis.php'); ?></h3>
				<div class="content-wrap" style="min-height: initial; margin-bottom: 20px;">
					<form method="post" id="searchform" action="">
						<input type="text" style="margin-left: -6px;width: 100%;" name="query" value="<?php echo isset($_POST['query']) ? $_POST['query'] : ''; ?>" placeholder="Enter your search terms here ..." />
						<input type="image" src="<?php echo get_template_directory_uri(); ?>/images/search_btn.png" id="search-btn">
					</form>
				<?php if(isset($_POST['query'])) : ?>
					<div class="thing">
						<p>Your search for <strong style="font-size: 15px;color:#333;">"<?php echo isset($_POST['query']) ? $_POST['query'] : ''; ?>"</strong> returned the following results:</p>
						&nbsp;
						&nbsp;
						<div class="search-results-links">
							<a href="#" class="smart-scroll" data-target="#search_results__pages" data-offset="-50">Pages (<?php echo $pages_query->post_count; ?>)</a>
							&nbsp;
							&nbsp;
							<a href="#" class="smart-scroll" data-target="#search_results__events" data-offset="-50">Events (<?php echo $events_query->post_count; ?>)</a>
							&nbsp;
							&nbsp;
							<a href="#" class="smart-scroll" data-target="#search_results__posts" data-offset="-50">Posts (<?php echo $posts_query->post_count; ?>)</a>
						</div>
					</div>
					<div class="search-results-columns">
						<div class="column pages">
							<h5 id="search_results__pages">Pages:</h5>
							<ul>
				  			<?php if($pages_query->have_posts()) : ?>
					  			<?php while($pages_query->have_posts()) : $pages_query->the_post(); ?>
										<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					  			<?php endwhile; ?>
							  <?php else : ?>
									<li>Sorry but we couldn't find any pages pertaining to your search. Maybe a different phrase will do the trick?</li>
							  <?php endif; ?>
							</ul>
						</div>
						<div class="column events">
							<h5 id="search_results__events">Events:</h5>
							<ul>
								<?php if($events_query->have_posts()) : ?>
									<?php while($events_query->have_posts()) : $events_query->the_post(); ?>
										<li>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											-
											<?php if(tribe_get_start_date(null, false) !== tribe_get_end_date(null, false)) : ?>
												<span><?php echo tribe_get_start_date(); ?> - <?php echo tribe_get_end_date(); ?></span>
											<?php else : ?>
												<span><?php echo tribe_get_start_date(); ?></span>
											<?php endif ?>
										</li>
									<?php endwhile; ?>
								<?php else : ?>
									<li>Sorry but we couldn't find any events pertaining to your search. Maybe a different phrase will do the trick?</li>
								<?php endif; ?>
							</ul>
						</div>
						<div class="column posts">
							<h5 id="search_results__posts">Posts:</h5>
							<ul>
								<?php if($posts_query->have_posts()) : ?>
									<?php while($posts_query->have_posts()) : $posts_query->the_post(); ?>
										<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> - <?php the_date(); ?></li>
									<?php endwhile; ?>
								<?php else : ?>
										<li>Sorry but we couldn't find any posts pertaining to your search. Maybe a different phrase will do the trick?</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			<?php endif; ?>
</section>

<?php get_footer(); ?>
