<?php

function snake_to_kebab($string)
{
	return strtolower(preg_replace('/_/', '/-/', $string));
}

function kebab_to_snake($string)
{
	return strtolower(preg_replace('/-/', '_', $string));
}
