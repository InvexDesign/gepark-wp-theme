<?php

if(!isset($color_scheme) || !$color_scheme) {
	echo "Missing \$color_scheme";
	return;
}

$type_class = (isset($type) && $type) ? ".type--$type" : "";
$popup_selector = ".featherlight > .featherlight-content .invex-popup${type_class}";

if($color_scheme['border'])
{
	if($color_scheme['border'] == 'none') {
	  $border = 'none';
	} else {
	  $border = '5px solid ' . $color_scheme['border'];
	}
} else {
	$border = ($type == 'emergency') ? '5px solid #DC3232' : 'none';
}

$background = $color_scheme['background'] ?
	$color_scheme['background'] :
	(($type == 'emergency') ? '#F0DBDA' : 'white');

$heading = $color_scheme['heading'] ?: '#333333';
$text = $color_scheme['text'] ?: '#333333';

$primary_button_background = $color_scheme['primary_button_background'] ?
	$color_scheme['primary_button_background'] :
	(($type == 'emergency') ? '#DC3232' : '#333');
$primary_button_text = $color_scheme['primary_button_text'] ?: 'white';

$secondary_button_background = $color_scheme['secondary_button_background'] ?
	$color_scheme['secondary_button_background'] :
	(($type == 'emergency') ? '#DC3232' : '#333');
$secondary_button_text = $color_scheme['secondary_button_text'] ?: 'white';

$close_button = $color_scheme['close_button'] ?: '#333333';

?>
<!-- ACF Configurable Styles - <?php echo $type ?: 'general'; ?> -->
<style>
	<?php echo $popup_selector ; ?> {
		border: <?php echo $border; ?>;
		background: <?php echo $background; ?>;
	}

	<?php echo $popup_selector ; ?>.image-layout--background > .container > .text-background {
		background: <?php echo $background; ?>;
	}

	<?php echo $popup_selector ; ?> > .container > .text > .featherlight-close {
		color: <?php echo $close_button; ?>;
	}

	<?php echo $popup_selector ; ?> > .container > .text .heading {
		color: <?php echo $heading; ?>;
	}

	<?php echo $popup_selector ; ?> > .container > .text .content {
		color: <?php echo $text; ?>;
	}

	<?php echo $popup_selector ; ?> > .container > .text .content a {
		color: blue;
	}

	<?php echo $popup_selector ; ?> > .container > .text .button-link.primary {
		color: <?php echo $primary_button_text; ?>;
		background: <?php echo $primary_button_background; ?>;
	}

	<?php echo $popup_selector ; ?> > .container > .text .button-link.secondary {
		color: <?php echo $secondary_button_text; ?>;
		background: <?php echo $secondary_button_background; ?>;
	}
</style>
