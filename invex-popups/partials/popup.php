<?php

if(!$popup_id) {
	echo "error - missing popup id!";
	return;
}

$title = get_post_field('post_title', $popup_id);
$slug = get_post_field('post_name', $popup_id);
$snake_case_slug = kebab_to_snake($slug);
$html_popup_id = INVEX_POPUP_ID_PREFIX . $popup_id;

$modified_at_utc_string = get_post_field('post_modified', $popup_id);
$_modified_at = new DateTime($modified_at_utc_string);
$_modified_at->setTimezone(new DateTimeZone('America/Chicago'));
$modified_at = $_modified_at->format('c');

$_image = get_field('image', $popup_id);
$image_url = $_image['image'];
$has_image_class = ($image_url ? 'has-image ' : 'no-image ');
$image_layout = $_image['layout'];
$image_link = $_image['link'];
$background_size = $_image['background_size'] ?: 'contain';
$background_size_class .= 'background-size--' . ($background_size ?: 'contain');
$image_opacity = $_image['opacity'];
$text_background_opacity_style = null;

switch($image_layout)
{
	case 'Right':
		$image_layout_class = 'image-layout--right';
		break;
	case 'Background':
		$image_layout_class = 'image-layout--background';
	  $image_link = null;
	  $background_size_class = '';
	  $text_background_opacity_style = "opacity: 0." . str_pad($image_opacity, 2, '0', STR_PAD_LEFT) . ";";
		break;
	case 'Left':
	default:
		$image_layout_class = 'image-layout--left';
		break;
}
//$image_layout_class = 'image-layout-background';
//$image_layout_class = 'image-layout-left';
//$image_layout_class = 'image-layout-right';

$_text = get_field('text', $popup_id);
$heading = $_text['heading'];
$content = $_text['content'];

$_buttons = get_field('buttons', $popup_id);
$primary_button = $_buttons['primary_button'];
$secondary_button = $_buttons['secondary_button'];

$_configuration = get_field('configuration', $popup_id);
$dismissal_timeout = $_configuration['dismissal_timeout'];

$container_classes = "$has_image_class $background_size_class";
?>

<!-- popup - <?php echo $slug; ?> -->
<div id="<?php echo $html_popup_id; ?>"
		 class="slug--<?php echo $slug; ?> invex-popup promotion-popup <?php echo $image_layout_class; ?>"
		 data-post-title="<?php echo $title; ?>"
		 data-modified-at="<?php echo $modified_at; ?>"
>
	<div class="emergency-toggle" onclick="toggleEmergencyClass()">
		<div class="button">Click To Toggle Between Basic/Emergency Style</div>
	</div>
	<div class="container <?php echo $container_classes; ?>">
		<?php if($image_url) : $image = "<img class=\"image\" src=\"$image_url\" />"; ?>
			<?php if($image_link) : ?>
				<a class="image image-link" href="<?php echo $image_link['url']; ?>" target="<?php echo $image_link['target']; ?>"
					 style="background-image: url('<?php echo $image_url; ?>')"
				>
				</a>
			<?php else : ?>
				<div class="image" style="background-image: url('<?php echo $image_url; ?>')"></div>
			<?php endif; ?>
		<?php endif; ?>
		<div class="text-background" style="<?php echo $text_background_opacity_style; ?>"></div>
		<div class="text">
			<i class="fa fa-times-circle featherlight-close"></i>
			<div class="container">
				<?php if($heading) : ?>
					<div class="heading"><?php echo $heading; ?></div>
				<?php endif; ?>
				<?php if($content) : ?>
					<div class="content"><?php echo $content; ?></div>
				<?php endif; ?>
				<?php if($primary_button) : ?>
					<a class="button-link primary" href="<?php echo $primary_button['url']; ?>"
						 target="<?php echo $primary_button['target']; ?>"
					>
						<?php echo $primary_button['title']; ?>
					</a>
				<?php endif; ?>
				<?php if($secondary_button) : ?>
					<a class="button-link secondary" href="<?php echo $secondary_button['url']; ?>"
						 target="<?php echo $secondary_button['target']; ?>"
					>
						<?php echo $secondary_button['title']; ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
