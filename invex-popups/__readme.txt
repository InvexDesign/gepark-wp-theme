Instructions:
- copy "wp-popups" directory to theme
- add the following line to functions.php:
    - require_once('wp-popups/invex-popups.php');
- examine popup styles and add to "popup-theme-adjustments.css" to address any css conflicts
- to debug, temporarily set "window.DEBUG_POPUP" to true in invex-popups.php