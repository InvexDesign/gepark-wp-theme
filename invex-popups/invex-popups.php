<?php

$expire_at_string = defined('INVEX_POPUP_TRIAL_EXPIRATION') ? INVEX_POPUP_TRIAL_EXPIRATION : '2060-06-18 00:00:00';
$now = new DateTime('now', new DateTimeZone('America/Chicago'));
$expire_at = new DateTime($expire_at_string, new DateTimeZone('America/Chicago'));
$now_time = strtotime($now->format('Y-m-d H:i:sP'));
$expire_time = strtotime($expire_at->format('Y-m-d H:i:sP'));
if($expire_time < $now_time) {
	error_log("invex popups trial expired! '$expire_at_string'");
	return;
}

require_once(get_template_directory() . '/invex-popups/helpers.php');
require_once(get_template_directory() . '/invex-popups/popup-custom-post-type.php');
require_once(get_template_directory() . '/invex-popups/popups-ajax.php');
require_once(get_template_directory() . '/invex-popups/popup-acf-field-groups.php');

function add_popups_assets_to_head()
{
	global $post;
	?>
	<!-- IWPCPPS -->
	<link href="<?php echo get_template_directory_uri(); ?>/invex-popups/popups-theme-adjustments.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo get_template_directory_uri(); ?>/invex-popups/popups.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo get_template_directory_uri(); ?>/invex-popups/invex-popups.min.js"></script>
	<script>
		window.BASE_URL = '<?php echo site_url(); ?>';
		window.THEME_URL = '<?php echo get_template_directory_uri(); ?>';
		window.POST_ID = <?php echo $post ? $post->ID : 'null'; ?>;
		window.INVEX_POPUP_ID_PREFIX = '<?php echo INVEX_POPUP_ID_PREFIX; ?>';
		window.DEBUG_POPUP = false;
	</script>
	<!-- IWPCPPS -->
	<?php
}
add_action('wp_head', 'add_popups_assets_to_head');

/* Create Popup Configuration ACF Option page */
if(function_exists('acf_add_options_page'))
{
	acf_add_options_page([
		'page_title'  => 'Popup Configuration',
		'menu_title'  => '-- Configuration --',
		'menu_slug'   => 'configuration',
		'capability'  => 'edit_posts',
		'parent_slug' => 'edit.php?post_type=popup',
		'redirect'    => false
	]);
}

