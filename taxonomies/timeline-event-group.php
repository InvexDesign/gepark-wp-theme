<?php

function add_timeline_event_group_taxonomy()
{
	// create a new taxonomy
	register_taxonomy(
		'timeline_event_group',
		'timeline_event',
		[
			'labels'            => [
				'name'              => _x('Timeline Event Groups', 'taxonomy general name'),
				'singular_name'     => _x('Timeline Event Group', 'taxonomy singular name'),
				'search_items'      => __('Search Timeline Event Groups'),
				'all_items'         => __('All Timeline Event Groups'),
				'parent_item'       => __('Parent Timeline Event Group'),
				'parent_item_colon' => __('Parent Timeline Event Group:'),
				'edit_item'         => __('Edit Timeline Event Group'),
				'update_item'       => __('Update Timeline Event Group'),
				'add_new_item'      => __('Add New Timeline Event Group'),
				'new_item_name'     => __('New Timeline Event Group Name'),
				'menu_name'         => __('Timeline Event Groups'),
			],
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => ['slug' => 'timeline-event-group'],
//			'capabilities' => [
//				'assign_terms' => 'edit_guides',
//				'edit_terms'   => 'publish_guides'
//			]
		]
	);
}

add_action('init', 'add_timeline_event_group_taxonomy');