<?php
/*
Template Name: Interactive Map
*/
?>

<?php get_header(); ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

	<?php if(defined('INVEX_MAPS_DIR')) : ?>
		<?php
			require INVEX_MAPS_DIR . '/vendor/autoload.php';
			$app = require_once INVEX_MAPS_DIR . '/bootstrap/app.php';
			$kernel = $app->make('Illuminate\Contracts\Http\Kernel');
			$kernel->handle($request = \Illuminate\Http\Request::capture());
			$__env = $app->make('Illuminate\View\Factory');

			$theme = \App\Models\Theme::getActiveTheme();
			foreach($theme->prepareStylesheets() as $index => $stylesheet) {
				wp_enqueue_style("invex-maps-style-$index", $stylesheet['url'], [], $stylesheet['version'], false);
			}

			foreach($theme->prepareScripts() as $index => $script) {
				wp_enqueue_script( "invex-maps-script-$index", $script['url'], [], $script['version']);
			}

			list($view, $data) = $theme->getViewAndData();
			echo $__env->make($view, $data, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render();
		?>
	<?php else : ?>
		<p style="display: none">The Interactive Map requires the "Invex Maps" installation and INVEX_MAPS_DIR to be set.</p>
	<?php endif; ?>

<?php endwhile; endif; ?>

<?php get_footer(); ?>