<?php
/*
Template Name: Platform Tennis
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Platform Tennis';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/platform-tennis.png';
		$sidebar_menu_id = 54;
		$sidebar_widget_area_id = 'platform_tennis_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
