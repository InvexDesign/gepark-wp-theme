<?php
/*
Template Name: Girls Lacrosse
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Girls Lacrosse';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/girls-lacrosse.png';
		$sidebar_menu_id = 58;
		$sidebar_widget_area_id = 'girls_lacrosse_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
