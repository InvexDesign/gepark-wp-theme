<?php
/*
Template Name: Youth Soccer
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Youth Soccer';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/youth-house-soccer.png';
		$sidebar_menu_id = 129;
		$sidebar_widget_area_id = 'youth_house_soccer_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
