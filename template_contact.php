<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Get in Touch';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-contact.png';

//		require_once('_templates/short-banner-full-width.php');
		?>
		<section id="main">
			<?php
			$banner_image_url = has_post_thumbnail() ? get_the_post_thumbnail_url() : (isset($banner_image_url) ? $banner_image_url : 'please-set-banner-image-url');
			//	$banner_image_url = isset($banner_image_url) ? $banner_image_url : (has_post_thumbnail() ? get_the_post_thumbnail_url() : 'please-set-banner-image-url');
			$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
			include('_partials/short-banner.php'); ?>
			<div class="content">
				<div class="main-col full-width">
					<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap" style="display: flex; flex-direction: column;">
						<div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-between;">
							<div style="flex-basis: 600px;">
								<?php the_content(); ?>
							</div>
							<div style="flex: 1; text-align: center;">
								<a href="<?php echo home_url('/'); ?>about/staff" class="callout" style="margin-top: 35px;">Browse the <span><i class="fa fa-users" aria-hidden="true"></i> Staff Directory</span></a>
							</div>
						</div>
						<hr>
						<div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-around;">
							<div style="flex-basis: 450px;">
								<?php echo do_shortcode('[wufoo username="glenellynpd" formhash="z1devz3k025ne7m" autoresize="true" height="600" header="show" ssl="true"]'); ?>
							</div>
							<div style="flex-basis: 450px;">
								<iframe style="max-width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23769.977847241713!2d-88.06228746191488!3d41.866026974148184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52fefae1c9cd%3A0x9bb09d0ae33f3f56!2sGlen+Ellyn+Park+District!5e0!3m2!1sen!2sus!4v1474034925459" style="border:0" allowfullscreen="" width="500" height="450" frameborder="0"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

<?php
/*
  *
  *                 	<table cellspacing="0" cellpadding="0">
                    	<tbody><tr>
                        	<td style="padding-right: 50px;" width="600" valign="top">
                            	<p><strong>We appreciate your feedback!</strong> If you have any comments about this web site, our programs, services or facilities, or if you need any additional information, we'd love to hear from you. Please use our general contact information or just fill out the form below.</p>
                   				<p style="padding-left: 25px; margin-bottom: 0px; font-size: 14px; line-height: 30px;"><span style="display: inline-block; width: 25px; text-align: center;"><i class="fa fa-lg fa-phone" aria-hidden="true"></i></span> <a href="tel:+16308582462">(630) 858-2462</a><br><span style="display: inline-block; width: 25px; text-align: center;"><i class="fa fa-lg fa-envelope-o" aria-hidden="true"></i></span> <a href="mailto:support@gepark.org">support@gepark.org</a></p>
                            </td>
                            <td valign="top">
                            	<p><a href="http://gepark.org/about/staff" class="callout" style="margin-top: 35px;">Browse the <span><i class="fa fa-users" aria-hidden="true"></i> Staff Directory</span></a></p>
                            </td>
                        </tr>
                    </tbody></table>
                    <hr>
                    <table cellspacing="0" cellpadding="0">
                    	<tbody><tr>
                        	<td style="padding-right: 50px;" width="500" valign="top">
                            	[wufoo username="glenellynpd" formhash="z1devz3k025ne7m" autoresize="true" height="600" header="show" ssl="true"]
                            </td>
                            <td valign="top">
                            	<div class="contact-map-wrap">
                                	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23769.977847241713!2d-88.06228746191488!3d41.866026974148184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52fefae1c9cd%3A0x9bb09d0ae33f3f56!2sGlen+Ellyn+Park+District!5e0!3m2!1sen!2sus!4v1474034925459" style="border:0" allowfullscreen="" width="500" height="450" frameborder="0"></iframe>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
  * */

?>