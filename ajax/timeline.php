<?php
header('Content-Type: application/json');

define('WP_USE_THEMES', false);
require('../../../../wp-load.php');

$group_id = $_GET['group_id'];
echo json_encode(getTimelineEvents($group_id));
