<?php
/*
Template Name: Activity Guide
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = "Browse Our Virtual Activity Guides";
		$banner_title = 'Activity Guide';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-registration.png';
		?>
        
        <?php
	$banner_image_url = has_post_thumbnail() ? get_the_post_thumbnail_url() : (isset($banner_image_url) ? $banner_image_url : 'please-set-banner-image-url');
//	$banner_image_url = isset($banner_image_url) ? $banner_image_url : (has_post_thumbnail() ? get_the_post_thumbnail_url() : 'please-set-banner-image-url');
	$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
	include('_templates/_partials/short-banner.php'); ?>

		<section id="main">
			<div class="content">
				<div class="main-col full-width">
					<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap">
						<?php if(isset($content)) : ?>
							<?php echo $content; ?>
						<?php else : ?>
							<?php the_content(); ?>
						<?php endif; ?>
						<div class="activity-guides">

                            <a href="https://joom.ag/vMBe" target="_blank" class="img-caption-slide-up-hover-effect">
                                <img title="Activity Guide Autumn 2019" src="https://gepark.org/wp-content/uploads/2020/02/gepark_spring-summer2020.jpg" alt="Activity Guide Spring/Summer 2020" />
                                <div class="caption orange">
                                    <h2>Spring/Summer 2020</h2>
                                    <p>Summer fun for the whole family</p>
                                </div>
                            </a>

                            <a href="https://joom.ag/0MAe" target="_blank" class="img-caption-slide-up-hover-effect">
                                <img title="Activity Guide Winter 2020" src="https://www.joomag.com/Frontend/embed/cover/issue_cover.php?UID=0542463001570130259" alt="Activity Guide Winter 2020" />
                                <div class="caption light-blue">
                                    <h2>Winter 2020</h2>
                                    <p>Programs designed for you and your family</p>
                                </div>
                            </a>

						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
