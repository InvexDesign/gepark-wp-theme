<?php
/*
Template Name: Tell Us What You Think
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Online Survey';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-contact.png';

		require_once('_templates/short-banner-full-width.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
