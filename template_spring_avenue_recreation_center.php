<?php
/*
Template Name: Spring Avenue Recreation Center
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Spring Avenue Recreation Center';
		$metaslider_id = 390;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.2592674285174!2d-88.0473526845593!3d41.865767979223094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52f8df4a81ef%3A0xc8add5e42f848b47!2sSpring+Avenue+Recreation+Center!5e0!3m2!1sen!2sus!4v1475700699358&output=embed';
		$sidebar_menu_id = 30;
		$sidebar_widget_area_id = 'spring_avenue_recreation_center_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
