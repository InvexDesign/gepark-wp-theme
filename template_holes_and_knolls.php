<?php
/*
Template Name: Holes & Knolls
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Holes & Knolls';
		$metaslider_id = 421;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.6627193360464!2d-88.05297068455955!3d41.8570859792238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e525335b35383%3A0x5e05062660e9cbd4!2sHoles+%26+Knolls+Miniature+Golf+Course!5e0!3m2!1sen!2sus!4v1475698954640&output=embed';
		$sidebar_menu_id = 40;
		$sidebar_widget_area_id = 'holes_and_knolls_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
