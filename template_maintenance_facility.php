<?php
/*
Template Name: Maintenance Facility
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Maintenance Facility';
		$metaslider_id = 431;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.7199331629595!2d-88.08090438455889!3d41.87737177922215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e536f142695ef%3A0x8324bfe7d8e12be!2s490+Kenilworth+Ave%2C+Glen+Ellyn%2C+IL+60137!5e0!3m2!1sen!2sus!4v1475700158710&output=embed';
		$sidebar_menu_id = 37;
		$sidebar_widget_area_id = 'maintenance_facility_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
