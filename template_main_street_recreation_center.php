<?php
/*
Template Name: Main Street Recreation Center
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Main Street Recreation Center';
		$metaslider_id = 412;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.030512144324!2d-88.06927368455912!3d41.870689979222796!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e530c439a68b3%3A0x94d30b04b2b723b9!2sMain+Street+Recreation+Center!5e0!3m2!1sen!2sus!4v1475700012473&output=embed';
		$sidebar_menu_id = 38;
		$sidebar_widget_area_id = 'main_street_recreation_center_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
