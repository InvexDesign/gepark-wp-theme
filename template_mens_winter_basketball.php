<?php
/*
Template Name: Mens Winter Basketball
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Men\'s Basketball';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/mens-winter-basketball.png';
		$sidebar_menu_id = 63;
		$sidebar_widget_area_id = 'mens_winter_basketball_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
