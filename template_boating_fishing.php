<?php
/*
Template Name: Boating & Fishing
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
    
    <?php
		$banner_title = 'Boating & Fishing';
		$banner_image_url = has_post_thumbnail() ? get_the_post_thumbnail_url() : (isset($banner_image_url) ? $banner_image_url : 'please-set-banner-image-url');
		$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
		include('_templates/_partials/short-banner.php'); ?>
    
		<section id="main">
			<div class="content">
				<div class="main-col full-width">
					<h3><?php the_title(); ?></h3>
					<div class="content-wrap">
						<?php the_content(); ?>
                        <iframe frameborder="0" marginwidth="0" marginheight="0" src="http://www.dupagehealth.org/ppi/ppi_embed.php?cond=1&company=Glen%20Ellyn%20Park%20District" width="150" height="356" scrolling="no"></iframe>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
