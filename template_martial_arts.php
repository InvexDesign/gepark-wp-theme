<?php
/*
Template Name: Martial Arts
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Martial Arts';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/martial-arts.png';
		$sidebar_menu_id = 52;
		$sidebar_widget_area_id = 'martial_arts_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
