<?php
/*
Template Name: TESTING
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main" class="home">
			<?php /*include('/home/gepark/public_html/alpha/includes/banner_home.php');*/ ?>
<!--			--><?php //include('/home/gepark/public_html/wp-content/themes/gepark/_templates/_partials/home-banner.php'); ?>
			<div class="content">
			<pre>
				<?php
				$metaslider_id = 2220;
//				$slides = getInvexSlides($metaslider_id);
				$args = [
					'force_no_custom_order' => true,
					'orderby'               => 'menu_order',
					'order'                 => 'ASC',
					'post_type'             => ['attachment', 'ml-slide'],
					'post_status'           => ['inherit', 'publish'],
					'lang'                  => '', // polylang, ingore language filter
					'suppress_filters'      => 1, // wpml, ignore language filter
					'posts_per_page'        => -1,
					'tax_query'             => [
						[
							'taxonomy' => 'ml-slider',
							'field'    => 'slug',
							'terms'    => $metaslider_id
						]
					]
				];
				
				$slides = [];
				$args = apply_filters( 'metaslider_populate_slides_args', $args, $metaslider_id, []);
				$query = new WP_Query( $args );
				while($query->have_posts())
				{
					$query->next_post();
					$post = $query->post;
					$featured_image = get_the_post_thumbnail_url($post->ID);
					$slides[] = [
						'image_url' => $featured_image ?: $post->guid,
						'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
						'content'   => $post->post_excerpt,
						'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
						'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
					];
				}
				wp_reset_postdata();

				var_dump($slides);
				?>
			</pre>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
