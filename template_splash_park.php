<?php
/*
Template Name: Splash Park
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Splash Park';
		$metaslider_id = 394;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.679395924343!2d-88.05215868455956!3d41.856727079223866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52533d6a2fa7%3A0x28ae9332704f0ff0!2sMaryknoll+Park+Splash+Pad!5e0!3m2!1sen!2sus!4v1475700603114&output=embed';
		$sidebar_menu_id = 12;
		$sidebar_widget_area_id = 'splash_park_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
