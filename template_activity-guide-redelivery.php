<?php
/*
Template Name: Activity Guide Redelivery
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = "Request Redelivery";
		$banner_title = 'Activity Guide';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/cumulus_clouds_panorama.jpg';

		require_once('_templates/short-banner-full-width.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
