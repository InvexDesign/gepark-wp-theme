<?php
/*
Template Name: Adult Volleyball
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Adult Volleyball';
		$banner_image_url = 'https://gepark.org/wp-content/uploads/2017/06/spi-adult-volleyball.png';
		$sidebar_menu_id = 108;
		$sidebar_widget_area_id = 'adult_volleyball_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
