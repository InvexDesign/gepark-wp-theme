<?php

function accordionShortcode($params, $content = null)
{
	extract(shortcode_atts(['slug' => 'this-slug-does-not-exist', 'style' => false], $params));

	if($post = get_page_by_path($slug, OBJECT, 'accordion'))
	{
		$post_id = $post->ID;
	}
	else
	{
		return "<h4>Invalid Accordion Slug ($slug)</h4>";
	}

	$accordion_items = getAdvancedCustomFieldValue('accordionItems', false, $post_id);
	if(!$accordion_items)
	{
		return "<div id='accordion_$post_id' style='display: none;'>Debug Message: Accordion #$post_id ($slug) has no items</div>";
	}

	$style_markup = $style ? "style='$style'" : '';
	$output = "<ul id='accordion_$post_id' class='accordion-group' $style_markup>";

	foreach($accordion_items as $index => $item)
	{
		if(!$item['enabled']) {
			continue;
		}

		$checked_markup = $item['open'] ? '' : "checked='checked'";
		$output .=
			"<li id='accordion_" . $post_id . "__item_$index' class='accordion-item'>" .
				"<input $checked_markup type='checkbox'><i></i><br />" .
				"<h4 class='accordion-title'><strong>" . $item['title'] . "</strong></h4>" .
				"<div class='accordion-content'>" .
					$item['content'] .
				"</div>" .
			"</li>"
		;
	}

	$output .= "</ul>";

	return $output;
}
add_shortcode('accordion', 'accordionShortcode');
