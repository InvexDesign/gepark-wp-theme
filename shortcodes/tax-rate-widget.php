<?php

function taxRateWidgetShortcode($params, $content = null)
{
	ob_start();
	include(get_template_directory() . '/_templates/_partials/tax-rate-widget.php');
	$widget = ob_get_clean();

	return $widget;
}
add_shortcode('tax-rate-widget', 'taxRateWidgetShortcode');
