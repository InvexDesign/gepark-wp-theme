<?php
/*
Template Name: Front End Map
*/
?>

<?php get_header(); ?>

<?php require(get_template_directory() . "/assets/libraries/interactive-map/map_functions.php"); ?>

<!-- Front End Map -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/interactive-map/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/interactive-map/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/interactive-map/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/interactive-map/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<!--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAApG3C02wvj58_ntggQeayvhQIXH0QpgSOV6MnyL1oQokAnOqf0BTAo9PxYt38jP3TyVvXJmmBFQkNww" type="text/javascript"></script>-->
<script src="https://maps.google.com/maps?file=api&v=2&key=AIzaSyAl9b47uT7VPFgg2qU6ML7yCaWEbHFuiMg" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/interactive-map/js/map_list_search.js" type="text/javascript"></script>

<!-- Front End Map -->

<style>
	/* Custom */
	.list-view-marker {
		min-height: 140px;
	}
</style>
<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Parks & Facilities';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/day-trips.png';
		?>
		<section id="main">
		<?php
		$banner_image_url = has_post_thumbnail() ? get_the_post_thumbnail_url() : (isset($banner_image_url) ? $banner_image_url : 'please-set-banner-image-url');
		$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
		require(get_template_directory() . '/_templates/_partials/short-banner.php'); ?>
			<div class="content">
				<div class="main-col full-width">
					<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap">
						<!-- Hidden DataTable for front end search/sort -->
						<table id="all_markers" style="display:none;">
							<thead>
							<tr>
								<th>id</th>
								<th>name</th>
								<th>url</th>
								<th>address</th>
								<th>city</th>
								<th>state</th>
								<th>zip</th>
								<th>lat</th>
								<th>lng</th>
								<th>features</th>
								<th>description</th>
								<th>imgPath</th>
								<th>phone</th>
								<th>facility_type</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$markers = get_all_markers();
							if(!$markers)
							{
								echo "There are no markers in the database!";
								return;
							}
							?>
							<?php foreach($markers as $row) : ?>
								<tr>
									<td><?php echo $row['id']; ?></td>
									<td><?php echo $row['name']; ?></td>
									<td><?php echo $row['url']; ?></td>
									<td><?php echo $row['address']; ?></td>
									<td><?php echo $row['city']; ?></td>
									<td><?php echo $row['state']; ?></td>
									<td><?php echo $row['zip']; ?></td>
									<td><?php echo $row['lat']; ?></td>
									<td><?php echo $row['lng']; ?></td>
									<td><?php echo $row['features']; ?></td>
									<td><?php echo $row['description']; ?></td>
									<td><?php echo $row['imgPath']; ?></td>
									<td><?php echo $row['phone']; ?></td>
									<td><?php echo $row['facility_type']; ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
							<td>ID</td>
							<td>Name</td>
							<td>URL</td>
							<td>Address</td>
							<td>City</td>
							<td>State</td>
							<td>Zip</td>
							<td>Latitude</td>
							<td>Longitude</td>
							<td>Features</td>
							<td>Description</td>
							<td>imgPath</td>
							<td>Phone</td>
							<td>Facility Type</td>
							</tfoot>
						</table>
						<script>
							var selected;
							var googleMarkers = new Array();
							var filteredMarkerRows = arrayifyTable();
							var infoWindows = new Array();
							var markerHtml = new Array();

							// ID of the div to be turned into a google map.
							var MAP_ID = "map";
							var GOOGLE_MAP;
							// Associative Array/Object mapping db field names to their column index.
							var HEADER_ROW = new Object;

							// Associative Array/Object mapping feature ids to names.
							var FEATURES = new Object;
							var FEATURES_SORTED_ARRAY = new Array();
							<?php
							$features = get_all_features();
							foreach($features as $feature)
							{
								echo "FEATURES['" . $feature['id'] . "'] = '" . $feature['name'] . "';";
								echo "FEATURES_SORTED_ARRAY.push(" . $feature['id'] . ");";
							}
							?>
							var FACILITY_TYPES = null;
							var FACILITY_TYPE_ICONS = [];
							<?php
							$facility_types = get_all_facility_types();
							foreach($facility_types as $facility_type)
							{
								$name = $facility_type['name'];
								$icon = $facility_type['icon'];
								echo "FACILITY_TYPE_ICONS['$name'] = generateMapIcon('$icon');";
							}
							?>
							//var PHOTO_PREFIX = '<?php echo PHOTO_PREFIX; ?>';
							var PHOTO_PREFIX = 'http://gepark.org/map-admin/MapPhotos/';

							$(document).ready(function()
							{
								/*
								 $('#facility_type_select').selectmenu().change(
								 function()
								 { filter(); }
								 );
								 $('input:text').button().css(
								 {
								 'font' : 'inherit',
								 'color' : 'inherit',
								 'text-align' : 'left',
								 'outline' : 'none',
								 'cursor' : 'text',
								 'width' : '176px'
								 }
								 );
								 */
								$("#view_tabs").tabs();
								initializeMap();
								var table = $('#all_markers').DataTable({bPaginate: false});
								HEADER_ROW = objectifyHeader();

								renderMarkersFromFilteredRows();
								renderSearchInterface();
								//renderSearchFacilities();
								$('#all_markers_wrapper').hide();
								$('#reset_button').button();
							});
						</script>
						<div class="front-end-map-content" style="display: block;width: 100%;">
<!--							<h2 class="page-title">Map/List Search Preview</h2>-->
							<table class="interactiveMap" style="max-width: 97%;">
								<tr>
									<td class="search-results">
										<div id="view_tabs" class="tab-container">
											<ul>
												<li><a href="#map_tab">Map View</a></li>
												<li><a href="#list_tab">List</a></li>
											</ul>
											<div id="map_tab" class="tab">
												<div id="map" class="search-results"></div>
											</div>
											<div id="list_tab" class="tab">
												<table id="list_view" style="position:relative; width: 620px; height: 530px">
												</table>
											</div>
										</div>
									</td>
									<td class="search-container">
										<h2 class="first">Search:</h2>
										<input type="text" name="text_search" id="text_search" maxlength="128" style="width:176px; margin-bottom:5px;" onkeyup="filter();"/>
										<input type="checkbox" name="text_search_by_name" id="text_search_by_name" onchange="filter();"
											   value="true" title="Check this box to search strictly by facility/park name." checked style="display:none;">
										<!--
										<h2 class="not-first">Facility Type:</h2>
										<select id="facility_type_select" name="facility" style="width:200px; margin-bottom:5px;" onchange="filter();">
										</select>
										<br/>
										-->
										<h2 class="not-first">Features</h2>
										<form id="features_form" style="width: 215px;" onchange="filter();">
										</form>

										<input id="reset_button" type="button" onclick="resetSearch();" value="Reset Search Parameters"/>
									</td>
								</tr>
							</table>
							<div class="clearer">&nbsp;</div>
							<div id="result_count" style="margin-bottom:25px; font-size:13px;">
							<div class="clearer">&nbsp;</div>
					</div>
					</div>
				</div>
			</div>
			</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>