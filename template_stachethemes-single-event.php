<style>
	.stachethemes-single-event .stachethemes-content-wrap .stec-layout-single {
		margin-top: 0 !important;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}
	.stachethemes-single-event .at-below-post.addthis_tool {
		display: none;
	}
</style>
<section id="main" class="stachethemes-single-event">
	<div class="content">
		<div class="main-col full-width">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<h3>Events Calendar</h3>
					<div class="stachethemes-content-wrap">
						<?php the_content(); ?>
						<div style="clear: both;"></div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
