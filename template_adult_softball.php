<?php
/*
Template Name: Adult Softball
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Adult Softball';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/adult-softball.png';
		$sidebar_menu_id = 61;
		$sidebar_widget_area_id = 'adult_softball_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
