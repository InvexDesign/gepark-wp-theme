<?php $SEARCH_DIRECTORY = 'search'; ?>

<?php get_header(); ?>

<section id="main">
	<div class="template template-search customized">
		<div class="container">
			<div class="main">
		  	<?php include("$SEARCH_DIRECTORY/head.php"); ?>

				<div class="content">
					<div class="main-col full-width">
						<h3>Search Results <?php include('_templates/_partials/sharethis.php'); ?></h3>
						<div class="content-wrap">
							<?php include("$SEARCH_DIRECTORY/form.php"); ?>

							<?php if(have_posts()) : ?>
								<div class="search-results">
									<?php while(have_posts()) : the_post(); ?>
					  				<?php include("$SEARCH_DIRECTORY/result.php"); ?>
									<?php endwhile; ?>
								</div>

								<div class="pagination"><?php
									echo paginate_links();
								?></div>
							<?php else : ?>
								<div class="no-results-display">
				  				<?php include("$SEARCH_DIRECTORY/no-results-display.php"); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="side"><?php
		  	include("$SEARCH_DIRECTORY/side.php");
			?></div>
		</div>

	  <?php include("$SEARCH_DIRECTORY/foot.php"); ?>
	</div>
</section>

<?php get_footer(); ?>
