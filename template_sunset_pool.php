<?php
/*
Template Name: Sunset Pool
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Sunset Pool';
		$metaslider_id = 310;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.3473337611813!2d-88.07069698455935!3d41.8638729792233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e530a8238c8ad%3A0x413f832d9e89687a!2sSunset+Pool!5e0!3m2!1sen!2sus!4v1475700933156&output=embed';
		$sidebar_menu_id = 33;
		$sidebar_widget_area_id = 'sunset_pool_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
