<?php
/*
Template Name: Home - Test
*/
?>

<?php get_header(); ?>
	<section id="main" class="home">
	  <?php
		define('LARAVEL_DIRECTORY', '/home/gepark/git/gepark-sliders');
	  require(LARAVEL_DIRECTORY . '/bootstrap/autoload.php');
	  $app = require_once(LARAVEL_DIRECTORY . '/bootstrap/app.php');
	  $kernel = $app->make('Illuminate\Contracts\Http\Kernel');
	  $kernel->handle($request = Illuminate\Http\Request::capture());
	  $__env = $app->make('Illuminate\View\Factory');

	  $slider = \App\Models\Sliders\Slider::find(1);
	  $slides = $slider->slides;
	  ?>

	  <?php echo $__env->make('frontend.sliders.gepark.home-slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	  <?php $metaslider_id = 2220;
	  $args = [
		  'post_type'       => ['attachment', 'ml-slide'],
		  'post_status'     => ['trash'],
//			'post__in' 				=> [10428,10719,10721,10722,11920,10730,10724,10726,11922,12001,12003,12005,12006,12007,12008,12009,12010,12012,12013,12014,12015,12016,12019,12020,12023,12067,12068,12071,12072,12073,12085,12086,12087,12088,12089],
			'post__in' 				=> [7616,6489,7081,7910,9204,10415,10424,7962,6134,7079,6490,7082,9208,9318,10422,7614,6133,5962,7080,8186,9212,9515,10414,6311,8188,9210,9565,10426,8190,9214,9319,8194,7909,9215,9517,10435,8196,9221,9519,10443,8198,6202,9223,9436,10445,8199,9224,10673,8201,10711,9317,10420,8204,8202,10425,10713,8206,10714,8207,10716,8209,8211,9432,8806,8213,10442,8215,8216,8217,8242,8222,9506,8243,9508,8221,9513,8223,9510,8240,9521,8245,9522,10717,8246,9523,12025,8251,12026,8292,12027,9194,12028,9196,12029,12030,9206,10728,12031,8248,12017,12018,9434,9435],
			'posts_per_page'	=> -1,
		];
//	  $args = [
//		  'force_no_custom_order' => true,
//		  'orderby'               => 'menu_order',
//		  'order'                 => 'ASC',
//		  'post_type'             => ['attachment', 'ml-slide'],
//		  'post_status'           => ['inherit', 'publish'],
//		  'lang'                  => '', // polylang, ingore language filter
//		  'suppress_filters'      => 1, // wpml, ignore language filter
//		  'posts_per_page'        => -1,
//		  'tax_query'             => [
//			  [
//				  'taxonomy' => 'ml-slider',
//				  'field'    => 'slug',
//				  'terms'    => $metaslider_id
//			  ]
//		  ]
//	  ];
	  $slides = [];
//	  $args = apply_filters( 'metaslider_populate_slides_args', $args, $metaslider_id, []);
	  $query = new WP_Query( $args );
	  while($query->have_posts())
	  {
		  $query->next_post();
		  $post = $query->post;
		  $featured_image = get_the_post_thumbnail_url($post->ID);
		  $slides[] = [
				'id'        => $post->ID,
				'image_url' => $featured_image ?: $post->guid,
				'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
				'content'   => $post->post_excerpt,
				'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
				'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true),
				'start_at'  => get_post_meta($post->ID, '_meta_slider_slide_scheduled_start', true),
				'end_at'    => get_post_meta($post->ID, '_meta_slider_slide_scheduled_end', true),
				'order'     => $post->menu_order,
//				'is_active' => true,
				'is_active' => false,
			];
	  }
	  wp_reset_postdata();
	  var_dump(count($slides));
	  ?>
		<p>
				<?php foreach($slides as $slide) : ?>
					$slide_<?php echo $slide['id']; ?> = Slide::create([<br />
						'slider_id'   => $slider->id,<br />
						'image'       => '<?php echo $slide['image_url']; ?>',<br />
						'title'       => <?php echo $slide['title'] ? '"' . $slide['title'] . '"' : 'null'; ?>,<br />
						'content'     => <?php echo $slide['content'] ? '"' . $slide['content'] . '"' : 'null'; ?>,<br />
						'button_text' => <?php echo $slide['button'] ? "'" . $slide['button'] ."'" : 'null'; ?>,<br />
						'button_link' => <?php echo $slide['link'] ? "'" . $slide['link'] ."'" : 'null'; ?>,<br />
						'order'       => <?php echo $slide['order'] ?: 'null'; ?>,<br />
						'start_at'    => <?php echo $slide['start_at'] ? "new Carbon('" . $slide['start_at'] . "')" : 'null'; ?>,<br />
						'end_at'      => <?php echo $slide['end_at'] ? "new Carbon('" . $slide['end_at'] . "')" : 'null'; ?>,<br />
						'is_active'   => <?php echo $slide['is_active'] ? 'true' : 'false'; ?>,<br />
					]);<br />
			<?php endforeach; ?>
		</p>
		<div class="content">
			<div class="home-col left">
				<div class="home-widget events"><h3>Upcoming Events</h3>
					<div class="content-wrap">
						<ul class="events">
								<?php
									/*Pull The Events Calendar Events*/
									global $post;
									$all_events = tribe_get_events(['eventDisplay' => 'upcoming']);
								?>
								<?php foreach($all_events as $post) : setup_postdata($post); ?>
								<li>
									<a href="<?php the_permalink(); ?>">
										<strong><?php echo tribe_get_start_date($post->ID, false, 'M j'); ?></strong>
										- <?php the_title(); ?>
									</a>
								</li>
								<?php endforeach; ?>
								<?php wp_reset_query(); ?>
						</ul>
					</div>
					<a href="<?php echo home_url('/'); ?>calendar/" class="widget-bottom-btn">Full Calendar &raquo;</a>
				</div>
				<div class="home-widget tweets">
					<h3>Tweets
						<a href="https://twitter.com/geparks" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/images/btn-follow.png" />
						</a>
					</h3>
					<div class="twitter-embed-wrap">
						<a class="twitter-timeline" data-width="311" data-height="450" data-link-color="#2293c5" data-chrome="noheader nofooter transparent" href="https://twitter.com/geparks">Tweets by geparks</a>
						<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>
					<a href="https://twitter.com/geparks" class="widget-bottom-btn" target="_blank"><span style="font-weight: 400;">Tweet</span> @geparks &raquo;</a>
				</div>
			</div>
			<div class="home-col mid">
				<div class="home-widget news"><h3>Latest News</h3>
					<div class="content-wrap">
						<!----- Display Posts ----->
						<?php query_posts('order=DESC&cat=68'); ?>
						<?php while(have_posts()) : the_post(); ?>
							<div class="post">
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								<?php $featured_image_link_url = get_post_meta(get_the_ID(), 'featured_image_link_url', true); ?>
								<?php if($featured_image_link_url && $featured_image_link_url != '') : ?>
									<a href="<?php echo $featured_image_link_url; ?>">
								<?php endif; ?>
								<?php the_post_thumbnail('thumbnail', ['class' => 'alignleft']); ?>
								<?php if($featured_image_link_url && $featured_image_link_url != '') : ?>
									</a>
								<?php endif; ?>
								<?php $content = the_content('Read More &raquo;');        /*$content = the_excerpt();*/				?>
								<p><?php echo /*strip_tags( $content, '<a><strong><em><span>' );*/ $content; ?></p>
								<div class="clearer"></div>
							</div>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</div>
					<a href="<?php echo home_url('/'); ?>news/district" class="widget-bottom-btn">All News &raquo;</a>
				</div>
			</div>
			<div class="home-col right">
				<div class="home-widget">
					<ul class="hot-buttons">
						<?php if(have_rows('hot-buttons')) : ?>
							<?php while(have_rows('hot-buttons')) : the_row(); ?>
								<?php
								$hot_button = [
									'title'       => get_sub_field('title'),
									'description' => get_sub_field('description'),
									'icon'        => get_sub_field('icon')['url'],
									'link'        => get_sub_field('link'),
									'active'   => get_sub_field('active'),
								];
								?>
								<?php if($hot_button['active']) : ?>
									<li>
										<a href="<?php echo $hot_button['link']['url']; ?>" <?php echo $hot_button['link']['target'] ? 'target="' . $hot_button['link']['target'] . '"' : ''; ?>>
											<div class="icon">
												<img src="<?php echo $hot_button['icon']; ?>">
											</div>
											<div class="text">
												<span class="title"><?php echo $hot_button['title']; ?></span>
												<span class="description">
													<?php echo $hot_button['description']; ?>
													<?php if($hot_button['link']['target']) : ?>
														<i class="fa fa-external-link" aria-hidden="true"></i>
													<?php endif; ?>
												</span>
											</div>
										</a>
									</li>
								<?php endif; ?>
							<?php endwhile; ?>
						<?php endif; ?>
					</ul>
				</div>
				<div class="home-widget info mailchimp"><h5>Email Signup <cite>Stay connected with news &amp; updates</cite>
					</h5>
					<!-- START MailChimp Form Embed -->
					<div id="mc_embed_signup">
						<form action="//gepark.us9.list-manage.com/subscribe/post?u=0519de60aadf81b3a43dd0a8d&amp;id=b69b9a5731"
									method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
									target="_blank" novalidate>
							<div id="mc_embed_signup_scroll">
								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>
								<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;" aria-hidden="true">
									<input type="text" name="b_0519de60aadf81b3a43dd0a8d_b69b9a5731" tabindex="-1" value="">
								</div>
								<div class="clear">
									<span class="btn-wrap">
										<input type="submit" value="Submit &raquo;" name="subscribe" id="mc-embedded-subscribe" class="button">
									</span>
								</div>
							</div>
						</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
					<script type='text/javascript'>(function($)
              {
                  window.fnames = new Array();
                  window.ftypes = new Array();
                  fnames[0] = 'EMAIL';
                  ftypes[0] = 'email';
                  fnames[1] = 'FNAME';
                  ftypes[1] = 'text';
                  fnames[2] = 'LNAME';
                  ftypes[2] = 'text';
              }(jQuery));
              var $mcj = jQuery.noConflict(true);</script>
					<!-- END MailChimp Form Embed -->
				</div>
				<a href="<?php echo home_url('/'); ?>online-survey" class="survey">Submit Feedback <span class="teaser">We want to hear from you!</span><span class="btn">Send Comments &raquo;</span></a>
				<!--<div class="home-widget info hotline">                <div class="content-wrap-small">                    <h5>Hotline Numbers</h5>                    <p><strong>Weather:</strong> <a href="tel:+16309845075">(630) 984-5075</a> <br /><strong>After Hours Emergency:</strong> <a href="tel:+16309473206">(630) 947-3206</a></p>                </div>            </div>-->
				<a href="<?php echo home_url('/'); ?>get-involved/make-a-donation/" class="donate">Support Your Parks <span class="teaser">Give online or volunteer time</span><span class="btn">Learn More &raquo;</span></a>
                <?php echo do_shortcode('[metaslider id="6398"]'); ?>
            </div>

		</div>
	</section>

	<?php if(isset($_GET['hover_test'])) : ?>
		<script>  $(document).ready(function()
				{
						$('ul.events a').addClass('recoil-hover-effect');
				});
		</script>
	<?php endif; ?>

	<?php if(isset($_GET['shadow_test'])) : ?>
		<style>
			.home-widget {
				box-shadow: 0px 2px 14px 1px black;
			}

			.home a.survey {
				box-shadow: 0px 2px 14px 1px black;
			}

			.home a.donate {
				box-shadow: 0px 2px 14px 1px black;
			}

			.home-banner {
				box-shadow: 0px 2px 14px 1px black;
			}
		</style>
	<?php endif; ?>

<?php get_footer(); ?>