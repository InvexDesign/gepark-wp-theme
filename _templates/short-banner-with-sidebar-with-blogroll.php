<section id="main">
	<?php
	$banner_image_url = has_post_thumbnail() ? get_the_post_thumbnail_url() : (isset($banner_image_url) ? $banner_image_url : 'please-set-banner-image-url');
//	$banner_image_url = isset($banner_image_url) ? $banner_image_url : (has_post_thumbnail() ? get_the_post_thumbnail_url() : 'please-set-banner-image-url');
	$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
	include('_partials/short-banner.php'); ?>
	<div class="content">
		<?php
		$sidebar_menu_id = isset($sidebar_menu_id) ? $sidebar_menu_id : null;
		$sidebar_widget_area_id = isset($sidebar_widget_area_id) ? $sidebar_widget_area_id : null;
		require_once('_partials/sidebar.php');
		?>
		<div class="main-col">
			<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
			<div class="content-wrap">
				<?php if(!isset($wp_query_parameters)) : ?>
					<p>Please set post query parameters!</p>
				<?php else : ?>
					<?php $wp_query = new WP_Query($wp_query_parameters);?>

					<?php if($wp_query->have_posts()) : ?>
						<table class="alt pic">
							<tbody>
							<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<tr>
									<td>
										<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										<?php if(isset($show_post_date) && $show_post_date) : ?><p class="post-date"><?php the_date(); ?></p><?php endif; ?>
										<p><?php
											//This removes any images from the content
											$content = get_the_content('Read More &raquo;');
											$content = preg_replace("/<img[^>]+\>/i", " ", $content);
											$content = apply_filters('the_content', $content);
											$content = str_replace(']]>', ']]>', $content);
											echo $content;
										?></p>
									</td>
									<td><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail', array('class' => 'alignleft')); ?></a></td>
								</tr>
							<?php endwhile; ?>
							</tbody>
						</table>
						<div class="nav-previous alignleft"><?php next_posts_link('&laquo; Older Posts'); ?></div>
						<div class="nav-next alignright"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
						<div class="clearer"></div>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p>Sorry, there are no posts at this time.</p>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>