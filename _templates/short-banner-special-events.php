<section id="main">
	<?php
	//TODO: Reverse this? Allow featured image to override banner_image_url?
	$banner_image_url = isset($banner_image_url) ? $banner_image_url : (has_post_thumbnail() ? get_the_post_thumbnail_url() : 'please-set-banner-image-url');
	$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
	include('_partials/short-banner.php'); ?>
	<div class="content">
		<?php
		$sidebar_menu_id = isset($sidebar_menu_id) ? $sidebar_menu_id : null;
		$sidebar_widget_area_id = isset($sidebar_widget_area_id) ? $sidebar_widget_area_id : null;
		require_once('_partials/sidebar.php');
		?>
		<div class="main-col">
			<div class="special-events-head">
                <?php
                    $thumb_id = get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                    $thumb_url = $thumb_url_array[0];
                ?>
                <div class="special-event-image-area" style="background-image:url(<?=$thumb_url; ?>);"></div>
				<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
			</div>
			<div class="content-wrap">
				<?php if(isset($content)) : ?>
					<?php echo $content; ?>
				<?php else : ?>
					<?php the_content(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>