<?php
//TODO: How to handle target blank links?

if(!isset($sidebar)) {
	error_log('smart-sidebar.php FAILED - missing parameter "$sidebar"');
	return;
}
if(!isset($sidebar['enabled']) || !$sidebar['enabled']) {
	return;
}

$position_desktop_class = 'position-desktop--' . $sidebar['position_desktop'];
$position_mobile_class = 'position-mobile--' . $sidebar['position_mobile'];
$sidebar_classes = "$position_desktop_class $position_mobile_class";

$sidebar_menu_id = isset($sidebar['menu_id']) ? $sidebar['menu_id'] : null;
$sidebar_widget_area_id = isset($sidebar['widget_area_id']) ? $sidebar['widget_area_id'] : null;
$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";//TODO: Extract this to header?
?>
<style>
	@media screen and (min-width: 1150px) {
		.sidebar.position-desktop--left {
			order: 0;
		}
		.sidebar.position-desktop--right {
			order: 2;
		}
	}
	@media screen and (max-width: 1150px) {
		.sidebar.position-mobile--top {
			order: 0;
		}
		.sidebar.position-mobile--bottom {
			order: 2;
		}

		.sidebar .menu-widget.widget {
			width: 230px;
			margin: 15px 0;
		}
		.sidebar .widget > .menu:after,
		.sidebar .widget > .menu.accordion .accordion-handle:after,
		.sidebar .widget .menu ul.menu-items:after {
			width: 215px !important;
		}
	}
	.sidebar {
		align-items: flex-start;
	}
	.sidebar .widget > .menu.accordion:not(.ready) {
		display: none;
	}
	.sidebar .widget > .menu.accordion:before {
		z-index: -1;
	}
	.sidebar .widget > .menu:not(.accordion) .accordion-handle {
		display: none;
	}
	.sidebar .widget > .menu.accordion .accordion-handle {
		display: flex;
		justify-content: flex-start;
		align-items: center;
		padding: 9px 12px;
		border-top: 1px solid #e39295;
		border-bottom: 1px solid #e39295;
		background-color: rgba(238, 41, 71, 1.0);
		cursor: pointer;
		transition: 300ms all linear;
	}
	.sidebar .widget > .menu.accordion .accordion-handle:hover {
		background-color: rgba(200, 27, 53, 1.0);
	}
	.sidebar .widget > .menu.accordion .accordion-handle .icon {
		margin-right: 10px;
		color: white;
		transition: 300ms all linear;
	}
	.sidebar .widget > .menu.accordion.open .accordion-handle .icon {
		margin-left: 3px;
		transform: rotateZ(90deg);
	}
	.sidebar .widget > .menu.accordion .accordion-handle span {
		font-weight: 700;
		text-transform: uppercase;
		letter-spacing: 0.5px;
		color: white;
	}
	.sidebar .widget > .menu.accordion .accordion-handle:before {
		content: '';
		display: block;
		position: absolute;
		right: 0;
		bottom: -15px;

		width: 0;
		height: 0;
		border-top: 15px solid rgba(238, 41, 71, 1.0);
		border-right: 15px solid transparent;
	}
	.sidebar .widget > .menu.accordion .accordion-handle:after {
		content: '';
		display: block;
		position: absolute;
		left: 0;
		bottom: -15px;

		height: 15px;
		width: 185px;
		background: rgba(238,41,71,1.0);
	}
	.sidebar .widget > .menu.accordion ul.menu-items li:first-child {
		border-top: none;
	}
	.sidebar .widget > .menu.accordion ul.menu-items li a {
		transition: 300ms all linear;
	}
</style>

<script>
$(document).ready(function()
{
	let $sidebarMenuAccordion = $('.sidebar .widget > .menu.accordion');
	$sidebarMenuAccordion.click(function()
	{
		let $menuAccordion = $(this);
		let $menuItems = $('.sidebar .widget > .menu.accordion .menu-items');

		$menuAccordion.toggleClass('open');

		if($menuAccordion.hasClass('open')) {
			$menuItems.slideDown();
		} else {
			$menuItems.slideUp();
		}
	});

	if($(window).width() < 1150) {
		$sidebarMenuAccordion.trigger('click').addClass('ready');
	} else {
		$sidebarMenuAccordion.addClass('ready');
	}

});
</script>
<div class="sidebar <?php echo $sidebar_classes; ?>">
	<?php if($sidebar_menu_id) : ?>
	  <?php
		$menu_items = wp_get_nav_menu_items($sidebar_menu_id);
		$menu_item_count = count($menu_items);
		?>
		<div class="widget menu-widget">
			<div class="menu <?php echo $menu_item_count > 1 ? 'accordion open' : ''; ?>">
				<div class="accordion-handle">
					<i class="icon fa fa-lg fa-caret-right" aria-hidden="true"></i>
					<span>Menu</span>
				</div>
				<ul class="menu-items">
					<?php foreach($menu_items as $menu_item) : ?>
						<li>
							<a class="<?php echo ($current_url == $menu_item->url) ? 'current' : ''; ?>"
								 href="<?php echo $menu_item->url; ?>"
							><?php
									echo $menu_item->title;
							?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>

	<?php if($sidebar_widget_area_id && is_active_sidebar($sidebar_widget_area_id)) : ?>
	  <?php
		if(!dynamic_sidebar($sidebar_widget_area_id)) {
			echo "<script>console.error('failed to load sidebar widget area')</script>";
		}
		?>
	<?php endif; ?>
</div>
