<?php
	$rates = [
	  'College of DuPage'     => 2.8,
	  'School District 87'    => 30.2,
	  'School District 41'    => 46.1,
	  'GE Public Library'     => 4.1,
	  'Village of Glen Ellyn' => 6.9,
	  'Milton Township'       => 2.0,
	  'DuPage Co. Services' 	 => 3.9,
	  'GE Park District'      => 4.2,
  ]
?>
<div class="tax-rate widget">
	<?php foreach($rates as $label => $rate) : ?>
		<div class="entry">
			<div class="label"><?php echo $label; ?></div>
			<div class="rate">
				<div class="fill"></div>
				<div class="text"><?php echo $rate; ?>%</div>
			</div>
		</div>
  <?php endforeach; ?>
</div>