<div class="banner short-banner" style="background-image: url(<?php echo isset($banner_image_url) ? $banner_image_url : 'please-set-image-url'; ?>)">
	<div class="banner-inner">
		<h2><span><?php echo isset($banner_title) ? $banner_title : 'Please set title!'; ?></span></h2>
	</div>
</div>