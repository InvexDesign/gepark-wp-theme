<?php $metaslider_id = 2220;
$slides = getInvexSlides($metaslider_id); ?>
<div class="home-banner">
	<div id="cycle" class="cycle-slideshow" data-cycle-fx="fade" data-cycle-pause-on-hover="false" data-cycle-speed="1000"
			 data-cycle-timeout="6000" data-cycle-slides="div.slide" data-cycle-pager=".flex-control-nav"
			 data-cycle-pager-template="<li><a class=''>{{slideNum}}</a></li>">
		<ol class="flex-control-nav flex-control-paging"></ol> <?php foreach($slides as $slide) : ?>
			<div class="slide" style="background-image: url(<?php echo $slide['image_url']; ?>);">
				<div class="overlay"><h2><?php echo $slide['title']; ?></h2>
					<p><?php echo $slide['content']; ?></p>        <a href="<?php echo $slide['link']; ?>"
																														class="link"><?php echo $slide['button']; ?> &raquo;</a>
				</div>
				<div class="basic-overlay"><h2><?php echo $slide['title']; ?></h2>
					<p><?php echo $slide['content']; ?></p>        <a href="<?php echo $slide['link']; ?>"
																														class="link"><?php echo $slide['button']; ?> &raquo;</a>
				</div>
			</div>  <?php endforeach; ?>  </div>
</div>