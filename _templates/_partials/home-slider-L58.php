<?php if(defined('INVEX_SLIDERS_DIR') && defined('HOME_SLIDER_ID')) : ?>
	<?php
	require INVEX_SLIDERS_DIR . '/vendor/autoload.php';
	$app = require_once INVEX_SLIDERS_DIR . '/bootstrap/app.php';
	$kernel = $app->make('Illuminate\Contracts\Http\Kernel');
	$kernel->handle($request = \Illuminate\Http\Request::capture());
	$__env = $app->make('Illuminate\View\Factory');

	$slider = \App\Models\Sliders\Slider::findOrFail(HOME_SLIDER_ID);
	$slider->generate();
	?>
<?php else : ?>
	<p style="display: none">The Home Slider requires the "Invex Sliders" installation and INVEX_SLIDERS_DIR and HOME_SLIDER_ID to be set.</p>
<?php endif; ?>