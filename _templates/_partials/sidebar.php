<?php
//TODO: How to handle target blank links?
//TODO: Extract this to header
$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<div class="sidebar">
	<?php if(isset($sidebar_menu_id) && $sidebar_menu_id) : ?>
	<div class="widget">
		<div class="menu">
			<ul>
				<?php $menu_items = wp_get_nav_menu_items($sidebar_menu_id); ?>
				<?php foreach($menu_items as $menu_item) : ?>
					<li><a href="<?php echo $menu_item->url; ?>" <?php echo ($current_url == $menu_item->url) ? 'class="current"' : ''; ?> ><?php echo $menu_item->title; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php endif; ?>
	<?php if(isset($sidebar_widget_area_id) && $sidebar_widget_area_id) : ?>
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar($sidebar_widget_area_id)) : endif; ?>
	<?php endif; ?>
</div>