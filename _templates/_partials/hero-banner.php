<?php
if(!isset($hero_banner)) {
	error_log('hero-banner.php FAILED - missing parameter "$hero_banner"');
	return;
}

$size = $hero_banner['size'] ?: 'short';

$_heading = $hero_banner['heading'];
$title = $_heading['custom_text'] ? $_heading['title'] : get_the_title();

$_button = $hero_banner['button'];

//$banner_image_url = has_post_thumbnail()
//	? get_the_post_thumbnail_url()
//	: (isset($banner_image_url)
//		? $banner_image_url : 'please-set-banner-image-url');

$heading_background_color_style = $_heading['background_color']
	? 'background-color: ' . $_heading['background_color'] . ';' : '';
$button_background_color_style = $_button['background_color']
	? 'background-color: ' . $_button['background_color'] . ';' : '';

?>
<style>
	.hero-banner.short {
		height: 148px;
	}
	.hero-banner .hero-buton {
		height: 148px;
	}
	.hero-banner .inner h2 {
		color: #FFF;
		-webkit-transform: initial;
		-moz-transform: initial;
		-o-transform: initial;
		background: none !important;
	}
	.hero-banner .inner h2 .background {
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		z-index: 1;
		background: #387b63;
		opacity: 0.8;
		-webkit-transform: skew(-45deg);
		-moz-transform: skew(-45deg);
		-o-transform: skew(-45deg);
	}
	.hero-banner .inner h2 span {
		position: relative;
		z-index: 2;
		-webkit-transform: initial;
		-moz-transform: initial;
		-o-transform: initial;
	}
	.hero-banner .inner a.hero-button .background {
		position: absolute;
		top: 0;
		left: 0;
		z-index: 1;
		width: 100%;
		height: 100%;
		-webkit-transform: skew(-45deg);
		-moz-transform: skew(-45deg);
		-o-transform: skew(-45deg);
		background-color: #218dc0;
		opacity: 0.7;
		transition: all 300ms linear;
	}
	.hero-banner .inner a.hero-button {
		position: absolute;
		bottom: 25px;
		right: -15px;
		display: block;
		color: #FFF;
		font-size: 14px;
		line-height: 22px;
		font-weight: 700;
		text-decoration: none;
		z-index: 10;
		padding: 8px 25px;
		transition: all 300ms linear;
	}
	.hero-banner .inner a.hero-button:hover .background {
		opacity: 1;
	}
	.hero-banner .inner a.hero-button span {
		position: relative;
		z-index: 2;
	}
	@media only screen and (max-width: 1099px) {
		.hero-banner .inner a.hero-button {
			right: 10px;
			font-size: 12px;
			line-height: 20px;
		}
	}
</style>
<div class="hero-banner new-facility-banner <?php echo $size; ?>"
		 style="background-image:url(<?php echo $hero_banner['image']; ?>"
>
	<div class="inner">
		<h2>
			<span class="background" style="<?php echo $heading_background_color_style; ?>"></span>
			<span><?php echo $title; ?></span>
		</h2>
	  <?php if($_button['enabled']) : ?>
			<a class="hero-button"
				 href="<?php echo $_button['link']['url']; ?>"
				 target="<?php echo $_button['link']['target']; ?>"
			>
				<span class="background" style="<?php echo $button_background_color_style; ?>"></span>
				<span>
					<?php if($_button['icon']) : ?>
						<i class="icon fa fa-lg <?php echo $_button['icon']; ?>" aria-hidden="true"></i>
						&nbsp;&nbsp;
					<?php endif; ?>

			  	<?php echo $_button['link']['title']; ?>
				</span>
			</a>
	  <?php endif; ?>
	</div>
</div>
