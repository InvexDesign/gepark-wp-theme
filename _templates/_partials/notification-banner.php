<?php
$notification_banner = get_field('notification_banner');
$message = $notification_banner['message'];
if(!isset($notification_banner) || ! $notification_banner['enabled'] || !$message) {
	return;
}

$icon = $notification_banner['icon'];
$background_color = $notification_banner['background_color'];
$background_color_style = $background_color ? "background-color: $background_color" : '';
?>
<style>
	.notification-banner-wrapper {
		padding-bottom: 0;
		min-height: 75px;
	}
	.notification-banner {
		flex: 1;
		width: 100%;
		display: flex;
		flex-direction: row;
		/*justify-content: center;*/
		/*align-items: center;*/
position: relative;
		padding-bottom: 8px;
		color: white;
		background-color: #59b190;
	}
	.notification-banner:after {
		content: '';
		position: absolute;
		bottom: 0;
		left: 0;
		right: 0;
		z-index: 2;
		height: 8px;
		background-color: rgba(0, 0, 0, 0.2);
	}
	.notification-banner > .container {
		flex: 1;
		display: flex;
		flex-direction: row;
		justify-content: flex-start;
		margin: 15px;
	}
	.notification-banner .icon-container {
		display: flex;
		justify-content: center;
		align-items: center;
		margin-right: 15px;
	}
	.notification-banner .message {
		flex: 1;
		display: flex;
		justify-content: center;
		align-items: center;
		font-size: 24px;
		font-weight: 700;
	}
</style>
<div class="content notification-banner-wrapper">
	<div class="notification-banner" style="<?php echo $background_color_style; ?>">
		<div class="container">
			<?php if($icon) : ?>
				<div class="icon-container">
					<i class="icon fa fa-lg <?php echo $icon; ?>" aria-hidden="true"></i>
				</div>
				<div class="separator"></div>
			<?php endif; ?>
			<div class="message"><?php echo $message; ?></div>
		</div>
	</div>
</div>
