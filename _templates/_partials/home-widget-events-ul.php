<ul class="events">
	<?php
	$max_events = defined('HOME_WIDGET_STC_EVENT_COUNT') ? HOME_WIDGET_STC_EVENT_COUNT : 10;

	// IMPORTANT - Must be database date YYYY-MM-DD
	$start_date = date('Y-m-d');

	// STC query doesnt appear to respect these settings
	$params = [
//		'posts_per_page' => '5',
//		'orderby'        => 'meta_value',
//		'meta_key'       => 'start-date',
//		'order'          => 'ASC'
	];
	$params['meta_query'] = [
		'key'     => 'start_date',
		'value'   => $start_date,
		'compare' => '>=',
		'type'    => 'DATE'
	];

	$stc_events = \Stachethemes\Stec\Events::get_events($params);
	if ($stc_events && is_array($stc_events))
	{
	  $count = 0;
		// IMPORTANT - this loops through backwards because query ordering is not respected
		for($i = count($stc_events) - 1; $i >= 0; $i--)
		{
			$stc_event = $stc_events[$i];
			if (false === ($stc_event instanceof \Stachethemes\Stec\Event_Post)) {
				continue;
			}
			if($count >= $max_events) {
				break;
			}

			$start_date_string = $stc_event->get_start_date();
			$start_datetime = strtotime($start_date_string);
			$formatted_start = date('M j', $start_datetime);
			?>
			<li data-id="<?php echo $stc_event->get_id(); ?>" data-start="<?php echo $start_date_string; ?>">
				<a href="<?php echo $stc_event->get_permalink(); ?>">
					<strong><?php echo $formatted_start; ?></strong>
					- <?php echo $stc_event->get_title(); ?>
				</a>
			</li>
			<?php

			$count++;
		}
		unset($stc_event);
	}
	?>
</ul>