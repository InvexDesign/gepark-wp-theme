<section id="main">
	<?php
	$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
	//TODO: Is this used?
	$banner_image_url = isset($banner_image_url) ? $banner_image_url : (has_post_thumbnail() ? get_the_post_thumbnail_url() : 'please-set-banner-image-url');
	$google_maps_url = isset($google_maps_url) ? $google_maps_url : 'please-set-google-maps-url';
	include('_partials/facility-banner.php'); ?>
	<div class="content">
		<?php
		$sidebar_menu_id = isset($sidebar_menu_id) ? $sidebar_menu_id : null;
		$sidebar_widget_area_id = isset($sidebar_widget_area_id) ? $sidebar_widget_area_id : null;
		require_once('_partials/sidebar.php');
		?>
		<div class="main-col">
			<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
			<div class="content-wrap">
				<?php if(isset($content)) : ?>
					<?php echo $content; ?>
				<?php else : ?>
					<?php the_content(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>