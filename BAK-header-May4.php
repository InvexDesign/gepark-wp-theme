<?php $CURRENT_URL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
require_once('includes/constants.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Glen Ellyn Park District | Established in 1919 | Glen Ellyn, Illinois</title>

	<?php include('includes/favicon.php'); ?>
	<?php include('includes/share-this.php'); ?>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
		<!-- Cycle for Home Page -->
	<?php if(is_home() || is_front_page()) : ?>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<?php endif; ?>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.cycle2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/tax-rate-widget.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lity.min.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/lightbox/js/lightbox-plus-jquery.min.js" type="text/javascript"></script>
	<script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/headhesive.min.js"></script>
    <script>
			var headhesive = false;
      function initializeHeadhesive()
      {
          if($(window).width() <= 1150)
          {
              if(headhesive != false)
              {
                  headhesive.destroy();
                  headhesive = false;
              }
          }
          else
          {
              if(headhesive == false)
              {
                  headhesive = new Headhesive('div.head-wrap', {offset: 160});
              }
          }
      }
      $(document).ready(function()
      {
          initializeHeadhesive();
          $(window).resize(function()
          {
              initializeHeadhesive();
          });
      });
	</script>

<style type="text/css">
/*** StacheThemes Style Updates (Feb 2023) ***/
section#main .stachethemes-content-wrap .stec-layout-single  p {
    margin-bottom: 24px !important;
}
</style>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> data-user-agent="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>" data-browser="<?php echo get_browser_name($_SERVER['HTTP_USER_AGENT']); ?>">

<?php include('includes/emergency-notifications.php'); ?>

<?php renderMobileMenu(); ?>

<!--	<div class="hex" style="position: fixed;--><!--    left: 128px;--><!--    top: -15px;-->
			<!--    z-index: 900;--><!--        border-right: 2px solid #0C5A7D;">--><!--		<a href="#"></a>-->
			<!--		<div class="corner-1" style="border-right: 2px solid #0C5A7D;"></div>-->
			<!--		<div class="corner-2" style="border-right: 2px solid #0C5A7D;"></div>--><!--	</div>-->
<div class="head-wrap">
	<header id="header"><!--			<div class="hex">--><!--				<a href="#"></a>-->
											<!--				<div class="corner-1"></div>--><!--				<div class="corner-2"></div>-->
											<!--			</div>-->      <!--<div class="hexagon"></div>-->
		<div class="meta">
            <?php
                $meta_nav = get_field('meta_nav', 'options');
                $social = get_field('social_networks', 'options');
            ?>
			<ul class="social">
                <?php if ($social['facebook'] != '') { ?>
				<li><a href="<?php echo $social['facebook']; ?>" title="Like GEPD on Facebook" target="_blank"><i class="fab fa-facebook"></i></a></li>
                <?php } ?>
                <?php if ($social['twitter'] != '') { ?>
				<li><a href="<?php echo $social['twitter']; ?>" title="Follow GEPD on Twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <?php } ?>
                <?php if ($social['instagram'] != '') { ?>
				<li><a href="<?php echo $social['instagram']; ?>" title="Find GEPD on Instagram" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <?php } ?>
                <?php if ($social['pinterest'] != '') { ?>
                    <li><a href="<?php echo $social['pinterest']; ?>" title="Pin GEPD on Pinterest" target="_blank"><i class="fab fa-pinterest"></i></a></li>
                <?php } ?>
                <?php if ($social['youtube'] != '') { ?>
                    <li><a href="<?php echo $social['youtube']; ?>" title="Watch GEPD on YouTube" target="_blank"><i class="fab fa-youtube"></i></a></li>
                <?php } ?>
                <?php if ($social['linkedin'] != '') { ?>
                    <li><a href="<?php echo $social['linkedin']; ?>" title="Find GEPD on LinkedIn" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                <?php } ?>
                <?php if ($social['flickr'] != '') { ?>
                    <li><a href="<?php echo $social['flickr']; ?>" title="View GEPD on Flickr" target="_blank"><i class="fab fa-flickr"></i></a></li>
                <?php } ?>
			</ul>
			<ul class="meta-nav">
                <?php
                    foreach ($meta_nav as $nav_link) {
                        $target = $nav_link['link']['target'];
                        $title = $nav_link['link']['title'];
                        $url = $nav_link['link']['url'];
                        echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                        if ($target == "_blank")
                            echo ' <i class="fas fa-external-link-alt"></i>';
                        echo '</a></li>';
                    }
                ?>
				<li class="magnifying-glass">
					<a href="<?php echo home_url(); ?>?s=" style="display: flex; justify-content: center; align-items: center;">
						<i style="font-size: 1.5em;" class="fa fa-search" aria-hidden="true"></i>
					</a>
				</li>
			</ul>
			<?php get_search_form(); ?>
		</div>
		<h1 class="logo"><a href="<?php echo home_url('/'); ?>" title="Glen Ellyn Park District">Glen Ellyn Park
																																														 District</a></h1>
		<h1 class="headhesive-logo logo"><a href="<?php echo home_url('/'); ?>" title="Glen Ellyn Park District">Glen Ellyn Park District</a></h1>
		<div class="callouts">
			<div class="activity-guide hover-sweep-to-right blue"><span class="text">Playbook</span> <a href="<?php echo home_url('/'); ?>activity-guide" class="activity-guide trigger"></a>
				<div class="sweeper"></div>
				<div class="background"></div>
			</div>
			<div class="register-now hover-sweep-to-right green"><span class="text">Register Now</span> <a href="<?php echo REGISTRATION_URL; ?>" class="register-now trigger"></a>
				<div class="sweeper"></div>
				<div class="background"></div>
			</div>
		</div>
        <?php
        $about_dd = get_field('about_dropdown', 'options');
        $pf_dd_left = get_field('pf_dropdown_left', 'options');
        $pf_dd_right = get_field('pf_dropdown_right', 'options');
        $rec_dd_left = get_field('rec_dropdown_left', 'options');
        $rec_dd_activities = get_field('registration_dropdown_activities', 'options');
        $rec_dd_athletics = get_field('registration_dropdown_athletics', 'options');
        $reg_dd = get_field('registration_dropdown', 'options');
        $events_dd = get_field('events_dropdown', 'options');
        $gi_dd = get_field('gi_dropdown', 'options');
        ?>
		<ul id="nav" class="nav"><span class="the-thin-blue-line left"></span>
			<li><a href="<?php echo home_url('/'); ?>about" class="about <?php if(isUnderTab("about", $CURRENT_URL))
		  {
			  echo "current";
		  } ?>" style="cursor: default;">About
					<div class="tab-padding"></div>
				</a>
				<div id="about_dropdown_menu" class="dropdown-menu" style="left: 0;">
					<div class="multi-section" style="height: 360px;">
						<div class="section">
							<ul class="multi-column has-separators eight-count" style="width: 500px;">
                                <?php
                                foreach ($about_dd as $nav_link) {
                                    $target = $nav_link['link']['target'];
                                    $title = $nav_link['link']['title'];
                                    $url = $nav_link['link']['url'];
                                    echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                                    if ($target == "_blank")
                                        echo ' <i class="fas fa-external-link-alt"></i>';
                                    echo '</a></li>';
                                }
                                ?>
							</ul>
						</div>
					</div>
					<span class="corner"></span></div>
			</li>
			<li><a href="<?php echo home_url('/'); ?>parks-facilities/interactive-map/"
						 class="parks-facilities <?php if(isUnderTab("parks-facilities", $CURRENT_URL))
			 {
				 echo "current";
			 } ?>" style="cursor: default;">Parks &amp; Facilities
					<div class="tab-padding"></div>
				</a>
				<div id="parks_facilities_dropdown_menu" class="dropdown-menu" style="left: 0;">
					<div class="multi-section" style="height: 340px;">
						<div class="section">
							<ul class="capitalize">
                                <?php
                                foreach ($pf_dd_left as $nav_link) {
                                    $target = $nav_link['link']['target'];
                                    $title = $nav_link['link']['title'];
                                    $url = $nav_link['link']['url'];
                                    echo '<li><a href="'.$url.'" target="'.$target.'">';
                                    if (isset($nav_link['icon']) && $nav_link['icon'] != '') {
                                        echo '<span style="display: inline-block; width: 20px; text-align: center;"><i class="fas '.$nav_link['icon'].' fa-lg" aria-hidden="true"></i></span> ';
                                    }
                                    echo $title;
                                    if ($target == "_blank")
                                        echo ' <i class="fas fa-external-link-alt"></i>';
                                    echo '</a></li>';
                                }
                                ?>
							</ul>
						</div>
						<div class="section">
							<ul class="multi-column has-separators eight-count" style="width: 500px;">
                                <?php
                                foreach ($pf_dd_right as $nav_link) {
                                    $target = $nav_link['link']['target'];
                                    $title = $nav_link['link']['title'];
                                    $url = $nav_link['link']['url'];
                                    echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                                    if ($target == "_blank")
                                        echo ' <i class="fas fa-external-link-alt"></i>';
                                    echo '</a></li>';
                                }
                                ?>
							</ul>
						</div>
					</div>
					<span class="corner"></span></div>
			</li>
			<li><a href="#" class="recreation <?php if(isUnderTab("recreation", $CURRENT_URL))
		  {
			  echo "current";
		  } ?>" style="cursor: default;">Programs & Sports
					<div class="tab-padding"></div>
				</a>
				<div id="recreation_dropdown_menu" class="dropdown-menu" style="left: -225px;">
					<div class="multi-section" style="height: 360px; padding-top: 6px;">
                        <div class="section">
                            <ul class="capitalize">
                                <?php
                                foreach ($rec_dd_left as $nav_link) {
                                    $target = $nav_link['link']['target'];
                                    $title = $nav_link['link']['title'];
                                    $url = $nav_link['link']['url'];
                                    echo '<li><a href="'.$url.'" target="'.$target.'">';
                                    if (isset($nav_link['icon']) && $nav_link['icon'] != '') {
                                        echo '<span style="display: inline-block; width: 20px; text-align: center;"><i class="fas '.$nav_link['icon'].' fa-lg" aria-hidden="true"></i></span> ';
                                    }
                                    echo $title;
                                    if ($target == "_blank")
                                        echo ' <i class="fas fa-external-link-alt"></i>';
                                    echo '</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="section">
                            <ul class="multi-column has-separators eight-count" style="width: 500px;">
                                <?php
                                foreach ($rec_dd_activities as $nav_link) {
                                    $target = $nav_link['link']['target'];
                                    $title = $nav_link['link']['title'];
                                    $url = $nav_link['link']['url'];
                                    echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                                    if ($target == "_blank")
                                        echo ' <i class="fas fa-external-link-alt"></i>';
                                    echo '</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
					</div>
					<span class="corner"></span></div>
			</li>
			<li><a href="<?php echo home_url('/'); ?>registration/how-to-register"
						 class="registration <?php if(isUnderTab("registration", $CURRENT_URL))
			 {
				 echo "current";
			 } ?>" style="cursor: default;">Registration
					<div class="tab-padding"></div>
				</a>
				<div id="registration_dropdown_menu" class="dropdown-menu" style="display: none;">
					<ul class="has-separators">
                        <?php
                        foreach ($reg_dd as $nav_link) {
                            $target = $nav_link['link']['target'];
                            $title = $nav_link['link']['title'];
                            $url = $nav_link['link']['url'];
                            echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                            if ($target == "_blank")
                                echo ' <i class="fas fa-external-link-alt"></i>';
                            echo '</a></li>';
                        }
                        ?>
                    </ul>
					<span class="corner"></span></div>
			</li>
			<li><a href="<?php echo home_url('/'); ?>calendar/" class="events <?php if(isUnderTab("events", $CURRENT_URL))
		  {
			  echo "current";
		  } ?>" style="cursor: default;">Events
					<div class="tab-padding"></div>
				</a>
				<div id="events_dropdown_menu" class="dropdown-menu" style="">
                    <ul class="has-separators">
                        <?php
                        foreach ($events_dd as $nav_link) {
                            $target = $nav_link['link']['target'];
                            $title = $nav_link['link']['title'];
                            $url = $nav_link['link']['url'];
                            echo '<li><a href="'.$url.'" target="'.$target.'">';
                            if (isset($nav_link['icon']) && $nav_link['icon'] != '') {
                                echo '<i class="fa '.$nav_link['icon'].'" aria-hidden="true"></i> ';
                            }
                            echo $title;
                            if ($target == "_blank")
                                echo ' <i class="fas fa-external-link-alt"></i>';
                            echo '</a></li>';
                        }
                        ?>
                    </ul>
					<span class="corner"></span></div>
			</li>
			<li><a href="<?php echo home_url('/'); ?>get-involved/adopt-a-park"
						 class="get-involved <?php if(isUnderTab("get-involved", $CURRENT_URL))
			 {
				 echo "current";
			 } ?>" style="cursor: default;">Get Involved
					<div class="tab-padding"></div>
				</a>
				<div id="get_involved_dropdown_menu" class="dropdown-menu" style="display: none;">
					<ul class="has-separators">
                        <?php
                        foreach ($gi_dd as $nav_link) {
                            $target = $nav_link['link']['target'];
                            $title = $nav_link['link']['title'];
                            $url = $nav_link['link']['url'];
                            echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                            if ($target == "_blank")
                                echo ' <i class="fas fa-external-link-alt"></i>';
                            echo '</a></li>';
                        }
                        ?>
					</ul>
					<span class="corner"></span></div>
			</li>
			<span class="the-thin-blue-line right"></span></ul>
	</header>
	<label id="hamburger_menu_toggle" class="mobile-menu-button hamburger-menu-utility">
		<i class="fa fa-bars" aria-hidden="true"></i>
		Main Menu
	</label>
</div>
