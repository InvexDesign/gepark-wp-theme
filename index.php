<?php get_header(); ?>
	<section id="main" class="home">
		<?php include('_templates/_partials/home-banner.php'); ?>
		<div class="content">
			<div class="home-col left">
				<div class="home-widget events"><h3>Upcoming Events</h3>
					<div class="content-wrap">
						<ul class="events">                      <?php /*Pull The Events Calendar Events*/												global $post;												$all_events = tribe_get_events(['eventDisplay' => 'upcoming']);											?>                      <?php foreach($all_events as $post) : setup_postdata($post); ?>
								<li><a
											href="<?php the_permalink(); ?>"><strong><?php echo tribe_get_start_date($post->ID, false, 'M j'); ?></strong>
										- <?php the_title(); ?></a>
								</li>                      <?php endforeach; ?>                      <?php wp_reset_query(); ?>
						</ul>
					</div>
					<a href="<?php echo home_url('/'); ?>calendar/" class="widget-bottom-btn">Full Calendar &raquo;</a></div>
				<div class="home-widget tweets"><h3>Tweets <a href="https://twitter.com/geparks" target="_blank"><img
									src="<?php echo get_template_directory_uri(); ?>/images/btn-follow.png" /></a></h3>
					<div class="twitter-embed-wrap"><a class="twitter-timeline" data-width="311" data-height="450"
																						 data-link-color="#2293c5" data-chrome="noheader nofooter transparent"
																						 href="https://twitter.com/geparks">Tweets by geparks</a>
						<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>
					<a href="https://twitter.com/geparks" class="widget-bottom-btn" target="_blank"><span
								style="font-weight: 400;">Tweet</span> @geparks &raquo;</a></div>
			</div>
			<div class="home-col mid">
				<div class="home-widget news"><h3>Latest News</h3>
					<div class="content-wrap">
						<!----- Display Posts -----> <?php query_posts('order=DESC&cat=68'); ?>    <?php while(have_posts()) : the_post(); ?>
							<div class="post"><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h4> <?php $featured_image_link_url = get_post_meta(get_the_ID(), 'featured_image_link_url', true); ?>        <?php if($featured_image_link_url && $featured_image_link_url != '') : ?>
								<a href="<?php echo $featured_image_link_url; ?>">        <?php endif; ?><?php the_post_thumbnail('thumbnail', ['class' => 'alignleft']); ?><?php if($featured_image_link_url && $featured_image_link_url != '') : ?>          </a> <?php endif; ?>        <?php $content = the_content('Read More &raquo;');        /*$content = the_excerpt();*/				?>
								<p><?php echo /*strip_tags( $content, '<a><strong><em><span>' );*/
					$content; ?></p>
								<div class="clearer"></div>
							</div>    <?php endwhile; ?>    <?php wp_reset_query(); ?>                </div>
					<a href="<?php echo home_url('/'); ?>news/district" class="widget-bottom-btn">All News &raquo;</a></div>
			</div>
			<div class="home-col right">
				<div class="home-widget">
					<ul class="hot-buttons">
						<li><a href="<?php echo home_url('/'); ?>parks-facilities/interactive-map/" class="map"> Interactive Map
								<span>View all of our parks &amp; facilities</span> </a></li>
						<li><a href="<?php echo home_url('/'); ?>activity-guide/" class="activity"> Virtual Activity Guide <span>Browse our interactive brochure</span>
							</a></li>
						<li><a href="<?php echo REGISTRATION_URL; ?>" class="registration"> Online Registration <span>Sign up for classes &amp; events online</span>
							</a></li>
						<!--<li>                            <a href="<?php echo home_url('/'); ?>rainout-line/" class="weather">                                Weather Center                                <span>Field condition &amp; cancellation updates</span>                            </a>                        </li>-->
						<!--<li>                            <a href="<?php echo home_url('/'); ?>rainout-line/" class="skating">                                Ice Skating Conditions                                <span>Check to see if it's safe to skate</span>                            </a>                        </li>-->
						<!--<li><a href="https://apm.activecommunities.com/gepark/Membership" class="aquatics" target="_blank"> Sunset
																																																								Pool
																																																								Memberships
								<span>Become a member or renew online <i class="fa fa-external-link" aria-hidden="true"></i></span> </a>
						</li>-->
<li><a href="https://apm.activecommunities.com/gepark/Membership" class="platform-tennis-racket" target="_blank">
								Platform Memberships <span>Become a member or renew online <i class="fa fa-external-link"
																																					aria-hidden="true"></i></span> </a></li>
						<li><a href="http://registration-software.net/appointment/gepark/" class="platform-tennis" target="_blank">
								Platform Tennis Courts <span>Online Court Reservations <i class="fa fa-external-link"
																																					aria-hidden="true"></i></span> </a></li>
						<li><a href="http://www.ackermansfc.com/open-gym-turf-schedule/" class="open-gym" target="_blank"> Open Gym
																																																							 / Turf
								<span>See open gym &amp; turf hours</span> </a></li>
					</ul>
				</div>
				<div class="home-widget info mailchimp"><h5>Email Signup <cite>Stay connected with news &amp; updates</cite>
					</h5>                <!-- START MailChimp Form Embed -->
					<div id="mc_embed_signup">
						<form action="//gepark.us9.list-manage.com/subscribe/post?u=0519de60aadf81b3a43dd0a8d&amp;id=b69b9a5731"
									method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
									target="_blank" novalidate>
							<div id="mc_embed_signup_scroll"><input type="email" value="" name="EMAIL" class="required email"
																											id="mce-EMAIL" placeholder="Email Address">
								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>
								<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
																																													name="b_0519de60aadf81b3a43dd0a8d_b69b9a5731"
																																													tabindex="-1" value=""></div>
								<div class="clear"><span class="btn-wrap"><input type="submit" value="Submit &raquo;" name="subscribe"
																																 id="mc-embedded-subscribe" class="button"></span></div>
							</div>
						</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
					<script type='text/javascript'>(function($)
              {
                  window.fnames = new Array();
                  window.ftypes = new Array();
                  fnames[0] = 'EMAIL';
                  ftypes[0] = 'email';
                  fnames[1] = 'FNAME';
                  ftypes[1] = 'text';
                  fnames[2] = 'LNAME';
                  ftypes[2] = 'text';
              }(jQuery));
              var $mcj = jQuery.noConflict(true);</script>                <!-- END MailChimp Form Embed -->
				</div>
				<a href="<?php echo home_url('/'); ?>online-survey" class="survey">Submit Feedback <span class="teaser">We want to hear from you!</span><span
							class="btn">Send Comments &raquo;</span></a>
				<!--<div class="home-widget info hotline">                <div class="content-wrap-small">                    <h5>Hotline Numbers</h5>                    <p><strong>Weather:</strong> <a href="tel:+16309845075">(630) 984-5075</a> <br /><strong>After Hours Emergency:</strong> <a href="tel:+16309473206">(630) 947-3206</a></p>                </div>            </div>-->
				<a href="<?php echo home_url('/'); ?>get-involved/make-a-donation/" class="donate">Support Your Parks <span
							class="teaser">Give online or volunteer time</span><span class="btn">Learn More &raquo;</span></a></div>
		</div>
	</section><?php if(isset($_GET['hover_test'])) : ?>
	<script>  $(document).ready(function()
      {
          $('ul.events a').addClass('recoil-hover-effect');
      });</script><?php endif; ?><?php if(isset($_GET['shadow_test'])) : ?>
	<style>  .home-widget {
			box-shadow: 0px 2px 14px 1px black;
		}

		.home a.survey {
			box-shadow: 0px 2px 14px 1px black;
		}

		.home a.donate {
			box-shadow: 0px 2px 14px 1px black;
		}

		.home-banner {
			box-shadow: 0px 2px 14px 1px black;
		}</style><?php endif; ?><?php get_footer(); ?>