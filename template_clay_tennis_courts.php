<?php
/*
Template Name: Clay Tennis Courts
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Clay Tennis Courts';
		$metaslider_id = 429;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.662203563413!2d-88.05295888455956!3d41.85709707922385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52534b8355bd%3A0x7fb35757a5b0a035!2s845+Pershing+Ave%2C+Glen+Ellyn%2C+IL+60137!5e0!3m2!1sen!2sus!4v1475700261215&output=embed';
		$sidebar_menu_id = 41;
		$sidebar_widget_area_id = 'clay_tennis_courts_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
