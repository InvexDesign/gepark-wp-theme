<?php
/*
Template Name: Age Groups
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Age Groups';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/age-groups.png';
		$sidebar_menu_id = 42;
		$sidebar_widget_area_id = 'age_groups_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
