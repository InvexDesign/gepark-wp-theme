<?php
/*
Template Name: Home PHP72
*/
?>

<?php get_header(); ?>
	<section id="main" class="home">
	  <?php
// 	  ic_cacheable('home-hero');
	  include('_templates/_partials/home-slider-L58.php');
	  ?>

	  <!-- HARCODE SLIDER -->
<!-- <div class="home-banner"><div id="cycle" class="cycle-slideshow">
<div class="slide cycle-slide cycle-slide-active" style="background-image: url(&quot;https://gepark.org/sliders/media/226/sli-registration-summer.png&quot;); position: absolute; top: 0px; left: 0px; z-index: 99; opacity: 1; display: block; visibility: visible;">
<div class="overlay">
<h2>Summer Playbook now available online</h2>
<div class="content" style="font-size: 13px;color: #fff;font-weight: 400;line-height: 21px;margin: 0;padding: 0 0 10px 15px;width: 240px;height: 100px;">New season, new activities for every age and interest. Resident registration opens at 9 am Saturday, May 4.</div>
<a href="https://gepark.org/playbook" class="link">Take a look »</a>
</div>
<div class="basic-overlay">
<h2>Summer Playbook now available online</h2>
<p>New season, new activities for every age and interest. Resident registration opens at 9 am Saturday, May 4.</p>
<a href="https://gepark.org/playbook" class="link">Take a look »</a>
</div>
</div>
</div></div> -->
	  <!-- END: Hardcode Slider -->

		<div class="content">
			<div class="home-col left">

				<div class="home-widget tweets">
					<!--<h3>Tweets
						<a href="https://twitter.com/glenellynparks" target="_blank">
							<img src="<?php echo get_template_directory_uri(); ?>/images/btn-follow.png" />
						</a>
					</h3>
					<div class="twitter-embed-wrap">
						<a class="twitter-timeline" data-width="371" data-height="700" data-link-color="#2293c5" data-chrome="noheader nofooter transparent" href="https://twitter.com/glenellynparks">Tweets by glenellynparks</a>
							<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
						<a class="twitter-timeline" data-width="371" data-height="700" data-link-color="#2293c5" data-chrome="noheader nofooter transparent" href="https://twitter.com/glenellynparks?ref_src=twsrc%5Etfw">Tweets by glenellynparks</>
						<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
					</div>
					<a href="https://twitter.com/glenellynparks" class="widget-bottom-btn" target="_blank"><span style="font-weight: 400;">Tweet</span> @glenellynparks &raquo;</a>-->
<iframe title="Facebook Timeline" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fglenellynparks%2F&tabs=timeline&width=371&height=830&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=118794238208428" width="371" height="830" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
				</div>
			</div>
			<div class="home-col mid">
				<div class="home-widget news"><h3>Latest News</h3>
					<div class="content-wrap">
						<!----- Display Posts ----->
			  <?php query_posts('order=DESC&cat=68'); ?>
			  <?php while(have_posts()) : the_post(); ?>
								<div class="post">
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<?php $featured_image_link_url = get_post_meta(get_the_ID(), 'featured_image_link_url', true); ?>
					<?php if($featured_image_link_url && $featured_image_link_url != '') : ?>
									<a href="<?php echo $featured_image_link_url; ?>">
					  <?php endif; ?>
					  <?php the_post_thumbnail('thumbnail', ['class' => 'alignleft']); ?>
					  <?php if($featured_image_link_url && $featured_image_link_url != '') : ?>
									</a>
				<?php endif; ?>
					<?php $content = the_content('Read More &raquo;');        /*$content = the_excerpt();*/				?>
									<p><?php echo /*strip_tags( $content, '<a><strong><em><span>' );*/ $content; ?></p>
									<div class="clearer"></div>
								</div>
			  <?php endwhile; ?>
			  <?php wp_reset_query(); ?>
					</div>
					<a href="<?php echo home_url('/'); ?>news/district" class="widget-bottom-btn">All News &raquo;</a>
				</div>
			</div>
			<div class="home-col right">
				<div class="home-widget">
					<ul class="hot-buttons">
			  <?php if(have_rows('hot-buttons')) : ?>
				  <?php while(have_rows('hot-buttons')) : the_row(); ?>
					  <?php
					  $hot_button = [
						  'title'       => get_sub_field('title'),
						  'description' => get_sub_field('description'),
						  'icon'        => get_sub_field('icon')['url'],
						  'link'        => get_sub_field('link'),
						  'active'   => get_sub_field('active'),
					  ];
					  ?>
					  <?php if($hot_button['active']) : ?>
										<li>
											<a href="<?php echo $hot_button['link']['url']; ?>" <?php echo $hot_button['link']['target'] ? 'target="' . $hot_button['link']['target'] . '"' : ''; ?>>
												<div class="icon">
													<img src="<?php echo $hot_button['icon']; ?>">
												</div>
												<div class="text">
													<span class="title"><?php echo $hot_button['title']; ?></span>
													<span class="description">
													<?php echo $hot_button['description']; ?>
							  <?php if($hot_button['link']['target']) : ?>
																<i class="fa fa-external-link" aria-hidden="true"></i>
							  <?php endif; ?>
												</span>
												</div>
											</a>
										</li>
					  <?php endif; ?>
				  <?php endwhile; ?>
			  <?php endif; ?>
					</ul>
				</div>
				<div class="home-widget info mailchimp"><h5>Email Signup <cite>Stay connected with news &amp; updates</cite>
					</h5>
					<!-- START MailChimp Form Embed -->
					<div id="mc_embed_signup">
						<form action="//gepark.us9.list-manage.com/subscribe/post?u=0519de60aadf81b3a43dd0a8d&amp;id=b69b9a5731"
									method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
									target="_blank" novalidate>
							<div id="mc_embed_signup_scroll">
								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>
								<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;" aria-hidden="true">
									<input type="text" name="b_0519de60aadf81b3a43dd0a8d_b69b9a5731" tabindex="-1" value="">
								</div>
								<div class="clear">
									<span class="btn-wrap">
										<input type="submit" value="Submit &raquo;" name="subscribe" id="mc-embedded-subscribe" class="button">
									</span>
								</div>
							</div>
						</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
					<script type='text/javascript'>(function($)
              {
                  window.fnames = new Array();
                  window.ftypes = new Array();
                  fnames[0] = 'EMAIL';
                  ftypes[0] = 'email';
                  fnames[1] = 'FNAME';
                  ftypes[1] = 'text';
                  fnames[2] = 'LNAME';
                  ftypes[2] = 'text';
              }(jQuery));
              var $mcj = jQuery.noConflict(true);</script>
					<!-- END MailChimp Form Embed -->
				</div>
				<!--<a href="<?php echo home_url('/'); ?>online-survey" class="survey">Submit Feedback <span class="teaser">We want to hear from you!</span><span class="btn">Send Comments &raquo;</span></a>-->
				<!--<div class="home-widget info hotline">                <div class="content-wrap-small">                    <h5>Hotline Numbers</h5>                    <p><strong>Weather:</strong> <a href="tel:+16309845075">(630) 984-5075</a> <br /><strong>After Hours Emergency:</strong> <a href="tel:+16309473206">(630) 947-3206</a></p>                </div>            </div>-->
				<!--<a href="<?php echo home_url('/'); ?>get-involved/make-a-donation/" class="donate">Support Your Parks <span class="teaser">Give online or volunteer time</span><span class="btn">Learn More &raquo;</span></a>-->
		  <?php //echo do_shortcode('[metaslider id="6398"]'); ?>
			</div>

		</div>
	</section>

<?php if(isset($_GET['hover_test'])) : ?>
	<script>  $(document).ready(function()
      {
          $('ul.events a').addClass('recoil-hover-effect');
      });
	</script>
<?php endif; ?>

<?php if(isset($_GET['shadow_test'])) : ?>
	<style>
		.home-widget {
			box-shadow: 0px 2px 14px 1px black;
		}

		.home a.survey {
			box-shadow: 0px 2px 14px 1px black;
		}

		.home a.donate {
			box-shadow: 0px 2px 14px 1px black;
		}

		.home-banner {
			box-shadow: 0px 2px 14px 1px black;
		}
	</style>
<?php endif; ?>

<?php get_footer(); ?>