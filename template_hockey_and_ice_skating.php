<?php
/*
Template Name: Hockey & Ice Skating
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Hockey & Ice Skating';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/hockey-and-ice-skating.png';
		$sidebar_menu_id = 50;
		$sidebar_widget_area_id = 'hockey_and_ice_skating_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
