<?php

function enqueue_assets()
{
	wp_enqueue_style('google-fonts-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800,400italic,300italic', false);
	wp_enqueue_style('google-fonts-cookie', 'https://fonts.googleapis.com/css?family=Cookie', false);
	wp_enqueue_style('knightlab-timeline', 'https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css', false);

    if(current_user_can('administrator')) {
        $css = [
            'metaslider'       => '/assets/css/metaslider.css',
            'base-styles'      => '/style_Aug2020.css',
            'header'           => '/assets/css/header_Aug2020.css',
            'font-awesome'     => '/assets/libraries/font-awesome-4.6.3/css/font-awesome.min.css',
            'main-styles'      => '/assets/css/style.css',
            'lity'             => '/assets/css/lity.min.css',
            'lightbox'         => '/assets/libraries/lightbox/css/lightbox.min.css',
            'responsive'       => '/assets/css/responsive_Aug2020.css',
            'accordions'       => '/assets/css/accordions.css',
            'headhesive'       => '/assets/css/headhesive_Aug2020.css',
            'decoration'       => '/assets/css/decoration_Aug2020.css',
            'tribe-events'     => '/assets/css/tribe-events.css',
            'hot-buttons'      => '/assets/css/hot-buttons.css',
            'mobile-menu'      => '/assets/css/mobile-menu.css',
            'tax-rate-widget'  => '/assets/css/tax-rate-widget.css',
            'bootstrap-custom' => '/assets/css/bootstrap.custom.css',
            'gutenberg-blocks' => '/assets/css/gutenberg-blocks.css',
            'smart-template'	 => '/assets/css/smart-template.css',
						'bundle'					 => '/assets/css/bundle.css',
            'search-main'      => '/search/search.min.css',
            'search-custom'    => '/search/search-custom.css',
        ];
    } else {
        /*$css = [
            'metaslider'       => '/assets/css/metaslider.css',
            'base-styles'      => '/style.css',
            'header'           => '/assets/css/header.css',
            'font-awesome'     => '/assets/libraries/font-awesome-4.6.3/css/font-awesome.min.css',
            'main-styles'      => '/assets/css/style.css',
            'lity'             => '/assets/css/lity.min.css',
            'lightbox'         => '/assets/libraries/lightbox/css/lightbox.min.css',
            'responsive'       => '/assets/css/responsive.css',
            'accordions'       => '/assets/css/accordions.css',
            'headhesive'       => '/assets/css/headhesive.css',
            'decoration'       => '/assets/css/decoration.css',
            'tribe-events'     => '/assets/css/tribe-events.css',
            'hot-buttons'      => '/assets/css/hot-buttons.css',
            'mobile-menu'      => '/assets/css/mobile-menu.css',
            'tax-rate-widget'  => '/assets/css/tax-rate-widget.css',
            'bootstrap-custom' => '/assets/css/bootstrap.custom.css',
            'search-main'      => '/search/search.min.css',
            'search-custom'    => '/search/search-custom.css',
        ];*/
        $css = [
            'metaslider'       => '/assets/css/metaslider.css',
            'base-styles'      => '/style_Aug2020.css',
            'header'           => '/assets/css/header_Aug2020.css',
            'font-awesome'     => '/assets/libraries/font-awesome-4.6.3/css/font-awesome.min.css',
            'main-styles'      => '/assets/css/style.css',
            'lity'             => '/assets/css/lity.min.css',
            'lightbox'         => '/assets/libraries/lightbox/css/lightbox.min.css',
            'responsive'       => '/assets/css/responsive_Aug2020.css',
            'accordions'       => '/assets/css/accordions.css',
            'headhesive'       => '/assets/css/headhesive_Aug2020.css',
            'decoration'       => '/assets/css/decoration_Aug2020.css',
            'tribe-events'     => '/assets/css/tribe-events.css',
            'hot-buttons'      => '/assets/css/hot-buttons.css',
            'mobile-menu'      => '/assets/css/mobile-menu.css',
            'tax-rate-widget'  => '/assets/css/tax-rate-widget.css',
            'bootstrap-custom' => '/assets/css/bootstrap.custom.css',
						'gutenberg-blocks' => '/assets/css/gutenberg-blocks.css',
						'bundle'					 => '/assets/css/bundle.css',
						'smart-template'	 => '/assets/css/smart-template.css',
            'search-main'      => '/search/search.min.css',
            'search-custom'    => '/search/search-custom.css',
        ];
    }

	if(get_browser_name($_SERVER['HTTP_USER_AGENT']) == 'Internet Explorer') {
		$css['internet-explorer'] = '/assets/css/internet-explorer.css';
	}

	if(is_page_template('template_front_end_map.php')) {
		$css['data-tables'] = '/assets/libraries/interactive-map/css/jquery.dataTables.css';
		$css['jquery-ui'] = '/assets/libraries/interactive-map/css/jquery-ui.css';
		$css['style-asit'] = '/assets/libraries/interactive-map/css/style_asit.css';
		$css['gpd-map'] = '/assets/libraries/interactive-map/css/gepd_map.css';
	}

	foreach($css as $handle => $filepath)
	{
		wp_enqueue_style($handle, get_template_directory_uri() . $filepath, [], filemtime(get_template_directory() . $filepath), false);
	}
}
add_action('wp_enqueue_scripts', 'enqueue_assets');

function enqueue_admin_styles()
{
	wp_register_style('font-awesome-admin',
			get_stylesheet_directory_uri() . '/assets/libraries/font-awesome-4.6.3/css/font-awesome.min.css',
			false,
			'1.0.0'
	);
	wp_enqueue_style('font-awesome-admin');
}
add_action('admin_enqueue_scripts', 'enqueue_admin_styles');

add_theme_support('post-thumbnails');

function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}
// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

require_once('includes/admin-scripts.php');
require_once('includes/advanced-custom-fields.php');
require_once('custom-post-types/accordion.php');
require_once('custom-post-types/timeline-event.php');
require_once('taxonomies/timeline-event-group.php');
require_once('shortcodes/accordion.php');
require_once('shortcodes/tax-rate-widget.php');
require_once(get_stylesheet_directory() . '/invex-cache/_invex-cache.php');

function register_widgetized_areas() {

	register_sidebar( array(
		'name'          => 'Registration Sidebar Area',
		'id'            => 'registration_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'About Us Sidebar Area',
		'id'            => 'about_us_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Splash Park Sidebar Area',
		'id'            => 'splash_park_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Skate Park Sidebar Area',
		'id'            => 'skate_park_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Safety Village Sidebar Area',
		'id'            => 'safety_village_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Spring Avenue Recreation Center Sidebar Area',
		'id'            => 'spring_avenue_recreation_center_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Spring Avenue Dog Park Sidebar Area',
		'id'            => 'spring_avenue_dog_park_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Spring Avenue Fitness Center Sidebar Area',
		'id'            => 'spring_avenue_fitness_center_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Sunset Pool Sidebar Area',
		'id'            => 'sunset_pool_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Village Green Garden Plots Sidebar Area',
		'id'            => 'village_green_garden_plots_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Platform Tennis Courts Sidebar Area',
		'id'            => 'platform_tennis_courts_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Maryknoll Clubhouse Sidebar Area',
		'id'            => 'maryknoll_clubhouse_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Maintenance Facility Sidebar Area',
		'id'            => 'maintenance_facility_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Main Street Recreation Center Sidebar Area',
		'id'            => 'main_street_recreation_center_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Lake Ellyn Boathouse Sidebar Area',
		'id'            => 'lake_ellyn_boathouse_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Holes & Knolls Sidebar Area',
		'id'            => 'holes_and_knolls_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Clay Tennis Courts Sidebar Area',
		'id'            => 'clay_tennis_courts_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Age Groups Sidebar Area',
		'id'            => 'age_groups_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Adventure Time Sidebar Area',
		'id'            => 'adventure_time_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Aquatics & Swim Sidebar Area',
		'id'            => 'aquatics_and_swim_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Camps Sidebar Area',
		'id'            => 'camps_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Dance Academy Sidebar Area',
		'id'            => 'dance_academy_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Day Trips Sidebar Area',
		'id'            => 'day_trips_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Fitness Sidebar Area',
		'id'            => 'fitness_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Gymnastics Sidebar Area',
		'id'            => 'gymnastics_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Hockey & Ice Skating Sidebar Area',
		'id'            => 'hockey_and_ice_skating_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Outdoor Skating & Sledding Sidebar Area',
		'id'            => 'outdoor_skating_and_sledding_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Martial Arts Sidebar Area',
		'id'            => 'martial_arts_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Nature Sidebar Area',
		'id'            => 'nature_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Platform Tennis Sidebar Area',
		'id'            => 'platform_tennis_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Preschool Education Sidebar Area',
		'id'            => 'preschool_education_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Tennis Sidebar Area',
		'id'            => 'tennis_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Field Hockey Sidebar Area',
		'id'            => 'field_hockey_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Girls Lacrosse Sidebar Area',
		'id'            => 'girls_lacrosse_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Winter Youth Basketball Sidebar Area',
		'id'            => 'winter_youth_basketball_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Youth House Soccer Sidebar Area',
		'id'            => 'youth_house_soccer_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Adult Softball Sidebar Area',
		'id'            => 'adult_softball_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Adult Indoor Soccer Sidebar Area',
		'id'            => 'adult_indoor_soccer_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Mens Winter Basketball Sidebar Area',
		'id'            => 'mens_winter_basketball_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'Youth Indoor Soccer Sidebar Area',
		'id'            => 'youth_indoor_soccer_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => 'News Sidebar Area',
		'id'            => 'news_sidebar_area',
		'before_widget' => '<div class="widget widget non-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
    register_sidebar( array(
        'name'          => 'Clay Tennis Courts Sidebar Area',
        'id'            => 'clay_tennis_courts_sidebar_area',
        'before_widget' => '<div class="widget widget non-menu">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
	register_sidebar( array(
        'name'          => 'Special Events Sidebar Area',
        'id'            => 'special_events_sidebar_area',
        'before_widget' => '<div class="widget widget non-menu">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
	register_sidebar( array(
        'name'          => 'Parties & Rentals Sidebar Area',
        'id'            => 'parties_rentals_sidebar_area',
        'before_widget' => '<div class="widget widget non-menu">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
	register_sidebar( array(
        'name'          => 'Adult Volleyball Sidebar Area',
        'id'            => 'adult_volleyball_sidebar_area',
        'before_widget' => '<div class="widget widget non-menu">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => 'Pickleball Sidebar Area',
        'id'            => 'pickleball_sidebar_area',
        'before_widget' => '<div class="widget widget non-menu">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
}
add_action('widgets_init', 'register_widgetized_areas');

function isUnderTab($tab, $url)
{
	$base_url = str_replace('/', '\\/', home_url());
	$regex = "/^$base_url\\/$tab.*$/";
//	echo "<span class=\"beansss\" style='display: none;'>";
//	var_dump([
//		$base_url,
//		$regex,
//		$tab,
//		$url
//	]);
//	echo "</span>";
	return preg_match($regex, $url);
}

//function new_excerpt_more($more) {
//	global $post;
////	remove_filter('excerpt_more', 'new_excerpt_more');
//	return " <a class=\"read-more\" href=\"". get_permalink($post->ID) . "\">Read More &raquo;</a>";
//}
//add_filter('excerpt_more','new_excerpt_more');

function getInvexSlides($metaslider_id)
{
	$args = [
		'force_no_custom_order' => true,
		'orderby'               => 'menu_order',
		'order'                 => 'ASC',
		'post_type'             => ['attachment', 'ml-slide'],
		'post_status'           => ['inherit', 'publish'],
		'lang'                  => '', // polylang, ingore language filter
		'suppress_filters'      => 1, // wpml, ignore language filter
		'posts_per_page'        => -1,
		'tax_query'             => [
			[
				'taxonomy' => 'ml-slider',
				'field'    => 'slug',
				'terms'    => $metaslider_id
			]
		]
	];

	$slides = [];
	$args = apply_filters( 'metaslider_populate_slides_args', $args, $metaslider_id, []);
	$query = new WP_Query( $args );
	while($query->have_posts())
	{
		$query->next_post();
		$post = $query->post;
		$featured_image = get_the_post_thumbnail_url($post->ID);
		$slides[] = [
			'image_url' => $featured_image ?: $post->guid,
			'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
			'content'   => $post->post_excerpt,
			'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
			'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
		];
	}
	wp_reset_postdata();

	return $slides;
}

function get_browser_name($user_agent)
{
	if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
	elseif (strpos($user_agent, 'Edge')) return 'Edge';
	elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
	elseif (strpos($user_agent, 'Safari')) return 'Safari';
	elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
	elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

	return 'Other';
}

// Custom TinyMCE Editor Styles
function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'PDF Link',
			'inline' => 'a',
			'selector' => 'a',
			'classes' => 'pdf',
			'wrapper' => false,

		),
		array(
			'title' => 'PDF List',
			'block' => 'ul',
			'selector' => 'ul',
			'classes' => 'pdf',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


function mprd_add_editor_styles() {
	add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'mprd_add_editor_styles' );

function wpb_disable_feed() {
	wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'wpb_disable_feed', 1);
add_action('do_feed_rdf', 'wpb_disable_feed', 1);
add_action('do_feed_rss', 'wpb_disable_feed', 1);
add_action('do_feed_rss2', 'wpb_disable_feed', 1);
add_action('do_feed_atom', 'wpb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wpb_disable_feed', 1);
add_action('do_feed_atom_comments', 'wpb_disable_feed', 1);

if(defined('INVEX_SLIDERS_URL') && INVEX_SLIDERS_URL)
{
	function addInvexSlidersButton($wp_admin_bar) {
		$args = [
			'id'    => 'invex_sliders_button',
			'title' => '<span class="dashicons dashicons-images-alt2 ab-icon" style="margin-top: 3px;"></span><span class="ab-label">Invex Sliders</span>',
			'href'  => INVEX_SLIDERS_URL,
		];
		$wp_admin_bar->add_node($args);
	}
	add_action('admin_bar_menu', 'addInvexSlidersButton', 50);
}

function renderMobileMenu()
{
	if(defined('DYNAMIC_MOBILE_MENU_ENABLED'))
	{
		if(is_bool(DYNAMIC_MOBILE_MENU_ENABLED) && DYNAMIC_MOBILE_MENU_ENABLED) {
			include('includes/mobile-menu.php');
		}
		elseif(is_int(DYNAMIC_MOBILE_MENU_ENABLED) && get_current_user_id() == DYNAMIC_MOBILE_MENU_ENABLED) {
			include('includes/mobile-menu.php');
		}
		else {
			include('includes/mobile-menu-hardcoded.php');
		}
	}
	else {
		include('includes/mobile-menu-hardcoded.php');
	}
}

function renderMobileMenuAccordion($title, $menu_id_constant)
{
  if(defined($menu_id_constant))
  {
	$wp_menu_id = constant($menu_id_constant);
	if(!isset($wp_menu_id)) {
	  return;
	}

	$menu_items = wp_get_nav_menu_items($wp_menu_id);
	if(!$menu_items || empty($menu_items)) {
	  return;
	}

	?>
	<div class="item">
	  <div class="head">
			<i class="fa fa-plus-circle plus icon" aria-hidden="true"></i>
			<i class="fa fa-minus-circle minus icon" aria-hidden="true"></i>
			<span><?php echo $title; ?></span>
		</div>
		<div class="content">
			<?php foreach($menu_items as $menu_item) : ?>
				<a class="link" href="<?php echo $menu_item->url; ?>" target="<?php echo $menu_item->target; ?>"><?php echo $menu_item->title; ?></a>
			<?php endforeach; ?>
	  </div>
	</div><?php
  }
}

function _strip_phone_num($phone) {
    $lower_phone = strtolower($phone);
    $keypad = array('a' => '2', 'b' => '2', 'c' => '2', 'd' => '3',
        'e' => '3', 'f' => '3', 'g' => '4', 'h' => '4',
        'i' => '4', 'j' => '5', 'k' => '5', 'l' => '5',
        'm' => '6', 'n' => '6', 'o' => '6', 'p' => '7',
        'q' => '7', 'r' => '7', 's' => '7', 't' => '8',
        'u' => '8', 'v' => '8', 'w' => '9', 'x' => '9',
        'y' => '9', 'z' => '9');
    foreach ($keypad as $letter => $number) {
        $lower_phone = str_replace($letter, $number, $lower_phone);
    }

    $stripped_phone = preg_replace('/\D+/', '', $lower_phone);
    return substr($stripped_phone, 0, 10);
}

//include('search/relevanssi-attachments.php');
//include('search/relevanssi-exclusions.php');
include('includes/components.php');
include('gutenberg/register-acf-gutenberg-blocks.php');
//include('gutenberg/gepark-button/gepark-button.php');
//
//if(defined('IAM_ENABLED') && IAM_ENABLED) {
//	require_once('includes/invex-access-manager/invex-access-manager.php');
//}
//
require_once('invex-popups/invex-popups.php');
