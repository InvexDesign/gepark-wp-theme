<?php
/*
Template Name: Timeline
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = "History of Glen Ellyn’s Parks";
		$banner_title = 'Timeline';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-registration.png';
		?>
        
        <?php
	$banner_image_url = has_post_thumbnail() ? get_the_post_thumbnail_url() : (isset($banner_image_url) ? $banner_image_url : 'please-set-banner-image-url');
	$banner_title = isset($banner_title) ? $banner_title : 'Please set banner title!';
	include('_templates/_partials/short-banner.php'); ?>

		<section id="main">
			<div class="content">
				<div class="main-col full-width">
					<h3><?php echo $page_title ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap">
						<div class="timeline-container">
							<div id='timeline' style="width: 100%; height: 600px"></div>
						</div>
						<br />
						<hr />
						<br />
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>

		<?php
		$timeline_event_group_id = getAdvancedCustomFieldValue('group_id');
		$timeline_event_ajax_url = get_template_directory_uri() . "/ajax/timeline.php?group_id=$timeline_event_group_id";

		$enabled = getAdvancedCustomFieldValue('enabled');
		$image = getAdvancedCustomFieldValue('image');
		$caption = getAdvancedCustomFieldValue('caption', false);
		$title = getAdvancedCustomFieldValue('title');
		$content = getAdvancedCustomFieldValue('content');
		$use_title_card = $enabled && $image && $title && $content;
		?>
		<script type="text/javascript">
        var request = new XMLHttpRequest();
        request.open('GET', '<?php echo $timeline_event_ajax_url; ?>', true);

        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                var events = JSON.parse(request.responseText);

                var configuration = {};
                var startingEventSlug = '<?php echo isset($_GET['event']) ? $_GET['event'] : ''; ?>';
                if(startingEventSlug !== '') {
                    var start_offset = <?php echo $use_title_card ? 1 : 0; ?>;
                    var start_at_slide = null;
                    for(var i = 0; i < events.length; i++) {
                        if(events[i].slug === startingEventSlug) {
                            start_at_slide = i + start_offset;
                            configuration['start_at_slide'] = start_at_slide;
                            console.log('starting on slide #' + start_at_slide + ', event #' + i);
                            break;
												}
										}

										if(start_at_slide === null) {
                        console.log('could not find requested starting slide');
										}
								}

                var timelineJson = {
                    <?php if($use_title_card): ?>
                    title: {
                        media: {
                            url: '<?php echo $image; ?>',
														<?php if($caption) : ?>
                            caption: '<?php echo $caption; ?>'
														<?php endif; ?>
                        },
                        text: {
                            headline: '<?php echo $title; ?>',
                            text: '<?php echo str_replace("\n","<br />", $content); ?>'
                        }
                    },
										<?php endif; ?>
                    events: events
                };

                window.timeline = new TL.Timeline('timeline', timelineJson, configuration);
            } else {
                console.error('Timeline ajax request returned an error');
            }
        };

        request.onerror = function() {
            console.error('Ajax connection error');
        };

        request.send();
		</script>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
