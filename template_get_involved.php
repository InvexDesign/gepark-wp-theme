<?php
/*
Template Name: Get Involved
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Get Involved';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-adopt-a-park.png';
		$sidebar_menu_id = 65;
		$sidebar_widget_area_id = 'get_involved_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
