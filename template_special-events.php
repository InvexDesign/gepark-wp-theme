<?php
/*
Template Name: Special Event
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>
        <?php
        $page_title = get_the_title();
        $banner_title = 'Special Events';
        $banner_image_url = get_template_directory_uri() . '/assets/images/banners/spi-calendar.png';
        $sidebar_menu_id = 66;
        $sidebar_widget_area_id = 'special_events_sidebar_area';

        require_once('_templates/short-banner-special-events.php'); ?>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
