<?php $CURRENT_URL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
require_once('includes/constants.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Glen Ellyn Park District | Established in 1919 | Glen Ellyn, Illinois</title>

	<?php include('includes/favicon.php'); ?>
	<?php include('includes/share-this.php'); ?>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
		<!-- Cycle for Home Page -->
	<?php if(is_home() || is_front_page()) : ?>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<?php endif; ?>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.cycle2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/tax-rate-widget.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lity.min.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/libraries/lightbox/js/lightbox-plus-jquery.min.js" type="text/javascript"></script>
	<script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/headhesive.min.js"></script>
    <script>
			var headhesive = false;
      function initializeHeadhesive()
      {
          if($(window).width() <= 1150)
          {
              if(headhesive != false)
              {
                  headhesive.destroy();
                  headhesive = false;
              }
          }
          else
          {
              if(headhesive == false)
              {
                  headhesive = new Headhesive('div.head-wrap', {offset: 160});
              }
          }
      }
      $(document).ready(function()
      {
          initializeHeadhesive();
          $(window).resize(function()
          {
              initializeHeadhesive();
          });
      });
	</script>

<style type="text/css">
/*** StacheThemes Style Updates (Feb 2023) ***/
section#main .stachethemes-content-wrap .stec-layout-single  p {
    margin-bottom: 24px !important;
}
</style>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> data-user-agent="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>" data-browser="<?php echo get_browser_name($_SERVER['HTTP_USER_AGENT']); ?>">

<?php include('includes/emergency-notifications.php'); ?>

<?php renderMobileMenu(); ?>

<!--	<div class="hex" style="position: fixed;--><!--    left: 128px;--><!--    top: -15px;-->
			<!--    z-index: 900;--><!--        border-right: 2px solid #0C5A7D;">--><!--		<a href="#"></a>-->
			<!--		<div class="corner-1" style="border-right: 2px solid #0C5A7D;"></div>-->
			<!--		<div class="corner-2" style="border-right: 2px solid #0C5A7D;"></div>--><!--	</div>-->
<div class="head-wrap">
	<header id="header"><!--			<div class="hex">--><!--				<a href="#"></a>-->
											<!--				<div class="corner-1"></div>--><!--				<div class="corner-2"></div>-->
											<!--			</div>-->      <!--<div class="hexagon"></div>-->
		<div class="meta">
            <?php
                $meta_nav = get_field('meta_nav', 'options');
                $social = get_field('social_networks', 'options');
            ?>
			<ul class="social">
                <?php if ($social['facebook'] != '') { ?>
				<li><a href="<?php echo $social['facebook']; ?>" title="Like GEPD on Facebook" target="_blank"><i class="fab fa-facebook"></i></a></li>
                <?php } ?>
                <?php if ($social['twitter'] != '') { ?>
				<li><a href="<?php echo $social['twitter']; ?>" title="Follow GEPD on Twitter" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <?php } ?>
                <?php if ($social['instagram'] != '') { ?>
				<li><a href="<?php echo $social['instagram']; ?>" title="Find GEPD on Instagram" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <?php } ?>
                <?php if ($social['pinterest'] != '') { ?>
                    <li><a href="<?php echo $social['pinterest']; ?>" title="Pin GEPD on Pinterest" target="_blank"><i class="fab fa-pinterest"></i></a></li>
                <?php } ?>
                <?php if ($social['youtube'] != '') { ?>
                    <li><a href="<?php echo $social['youtube']; ?>" title="Watch GEPD on YouTube" target="_blank"><i class="fab fa-youtube"></i></a></li>
                <?php } ?>
                <?php if ($social['linkedin'] != '') { ?>
                    <li><a href="<?php echo $social['linkedin']; ?>" title="Find GEPD on LinkedIn" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                <?php } ?>
                <?php if ($social['flickr'] != '') { ?>
                    <li><a href="<?php echo $social['flickr']; ?>" title="View GEPD on Flickr" target="_blank"><i class="fab fa-flickr"></i></a></li>
                <?php } ?>
			</ul>
			<ul class="meta-nav">
                <?php
                    foreach ($meta_nav as $nav_link) {
                        $target = $nav_link['link']['target'];
                        $title = $nav_link['link']['title'];
                        $url = $nav_link['link']['url'];
                        echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                        if ($target == "_blank")
                            echo ' <i class="fas fa-external-link-alt"></i>';
                        echo '</a></li>';
                    }
                ?>
				<li class="magnifying-glass">
					<a href="<?php echo home_url(); ?>?s=" style="display: flex; justify-content: center; align-items: center;">
						<i style="font-size: 1.5em;" class="fa fa-search" aria-hidden="true"></i>
					</a>
				</li>
			</ul>
			<?php get_search_form(); ?>
		</div>
		<h1 class="logo"><a href="<?php echo home_url('/'); ?>" title="Glen Ellyn Park District">Glen Ellyn Park
																																														 District</a></h1>
		<h1 class="headhesive-logo logo"><a href="<?php echo home_url('/'); ?>" title="Glen Ellyn Park District">Glen Ellyn Park District</a></h1>
		<div class="callouts">
			<div class="activity-guide hover-sweep-to-right blue"><span class="text">Playbook</span> <a href="<?php echo home_url('/'); ?>activity-guide" class="activity-guide trigger"></a>
				<div class="sweeper"></div>
				<div class="background"></div>
			</div>
			<div class="register-now hover-sweep-to-right green"><span class="text">Register Now</span> <a href="<?php echo REGISTRATION_URL; ?>" class="register-now trigger"></a>
				<div class="sweeper"></div>
				<div class="background"></div>
			</div>
		</div>
        <?php
        $about_dd = get_field('about_dropdown', 'options');
        $pf_dd_left = get_field('pf_dropdown_left', 'options');
        $pf_dd_right = get_field('pf_dropdown_right', 'options');
        $rec_dd_left = get_field('rec_dropdown_left', 'options');
        $rec_dd_activities = get_field('registration_dropdown_activities', 'options');
        $rec_dd_athletics = get_field('registration_dropdown_athletics', 'options');
        $reg_dd = get_field('registration_dropdown', 'options');
        $events_dd = get_field('events_dropdown', 'options');
        $gi_dd = get_field('gi_dropdown', 'options');
        ?>

        <!-- Hardcode Main Nav -->
<ul id="nav" class="nav"><span class="the-thin-blue-line left"></span>
<li><a href="https://gepark.org/about" class="about " style="cursor: default;">About
<div class="tab-padding"></div>
</a>
<div id="about_dropdown_menu" class="dropdown-menu" style="left: 0;">
<div class="multi-section" style="height: 360px;">
<div class="section">
<ul class="multi-column has-separators eight-count" style="width: 500px;">
<li><a href="https://gepark.org/overview/" target="">About, Mission, &amp; History</a></li><li><a href="https://gepark.org/annual-reports/" target="">Annual Reports</a></li><li><a href="https://gepark.org/bids-rfps/" target="">Bids &amp; RFPs</a></li><li><a href="https://gepark.org/commissioners" target="">Board of Commissioners</a></li><li><a href="https://gepark.org/board-meetings/" target="">Board Meetings</a></li><li><a href="https://gepark.org/financial/" target="">Budget &amp; Financial</a></li><li><a href="https://gepark.org/projects/" target="">Capital Projects &amp; Improvements</a></li><li><a href="https://gepark.org/contracts/" target="">Contracts &amp; Agreements</a></li><li><a href="https://gepark.org/foia/" target="">FOIA Requests</a></li><li><a href="https://gepark.org/green-initiatives/" target="">Green Initiatives</a></li><li><a href="https://gepark.bamboohr.com/careers" target="_blank">Job Opportunities <i class="fas fa-external-link-alt"></i></a></li><li><a href="https://gepark.org/advisory-committees/" target="">Resident Advisory Committees</a></li><li><a href="https://gepark.org/plans-surveys/" target="">Plans, Surveys &amp; Reports</a></li><li><a href="https://gepark.org/policies-ordinances/" target="">Policies &amp; Ordinances</a></li><li><a href="https://gepark.org/enews/" target="">Subscribe to Our eNews</a></li><li><a href="https://gepark.org/staff-directory/" target="">Staff Directory</a></li> </ul>
</div>
</div>
<span class="corner"></span></div>
</li>
<li><a href="https://gepark.org/parks-facilities/interactive-map/" class="parks-facilities " style="cursor: default;">Parks &amp; Facilities
<div class="tab-padding"></div>
</a>
<div id="parks_facilities_dropdown_menu" class="dropdown-menu" style="left: 0;">
<div class="multi-section" style="height: 340px;">
<div class="section">
<ul class="capitalize">
<li><a href="https://gepark.org/park-directory/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-map-signs fa-lg" aria-hidden="true"></i></span> Find A Park</a></li><li><a href="https://gepark.org/boating-fishing" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-life-ring fa-lg" aria-hidden="true"></i></span> Fishing &amp; Boating</a></li><li><a href="https://gepark.org/tennis-pickleball-courts/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-baseball-ball fa-lg" aria-hidden="true"></i></span> Tennis &amp; Pickleball Courts</a></li><li><a href="https://gepark.org/outdoor-skating-sledding/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-snowman fa-lg" aria-hidden="true"></i></span> Ice Rinks &amp; Sled Hill</a></li><li><a href="https://gepark.org/rental-opportunities/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-birthday-cake fa-lg" aria-hidden="true"></i></span> Rentals &amp; Parties</a></li><li><a href="https://gepark.org/rainout-line/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-exclamation-circle fa-lg" aria-hidden="true"></i></span> Facility Status Hotline</a></li><li><a href="https://gepark.org/trails/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-shoe-prints fa-lg" aria-hidden="true"></i></span> Trail Recommendations</a></li> </ul>
</div>
<div class="section">
<ul class="multi-column has-separators eight-count" style="width: 500px;">
<li><a href="http://ackermansfc.com/" target="_blank">Ackerman SFC <i class="fas fa-external-link-alt"></i></a></li><li><a href="https://gepark.org/claytenniscourts/" target="">Clay Tennis Courts</a></li><li><a href="https://gepark.org/gardenplots/" target="">Community Garden Plots</a></li><li><a href="https://gepark.org/holesandknolls/" target="">Holes &amp; Knolls Mini Golf</a></li><li><a href="https://gepark.org/boathouse/" target="">Lake Ellyn Boathouse</a></li><li><a href="https://gepark.org/mainstreet/" target="">Main Street Rec Center</a></li><li><a href="https://gepark.org/clubhouse/" target="">Maryknoll Clubhouse</a></li><li><a href="https://gepark.org/splashpark/" target="">Maryknoll Splash Park</a></li><li><a href="https://gepark.org/skatepark/" target="">Newton Skate Park</a></li><li><a href="https://gepark.org/platformtennis/" target="">Platform Tennis Center</a></li><li><a href="https://gepark.org/dogpark/" target="">Spring Avenue Dog Park</a></li><li><a href="https://gepark.org/springavenuefitness/" target="">Spring Avenue Fitness Center</a></li><li><a href="https://gepark.org/springavenue/" target="">Spring Avenue Rec Center</a></li><li><a href="https://gepark.org/sunsetpool/" target="">Sunset Pool</a></li> </ul>
</div>
</div>
<span class="corner"></span></div>
</li>
<li><a href="#" class="recreation " style="cursor: default;">Programs &amp; Sports
<div class="tab-padding"></div>
</a>
<div id="recreation_dropdown_menu" class="dropdown-menu" style="left: -225px;">
<div class="multi-section" style="height: 360px; padding-top: 6px;">
<div class="section">
<ul class="capitalize">
<li><a href="https://gepark.org/activity-guide/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-book-open fa-lg" aria-hidden="true"></i></span> Playbook Program Guide</a></li><li><a href="https://apm.activecommunities.com/gepark/Home" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-mouse-pointer fa-lg" aria-hidden="true"></i></span> Register Online</a></li><li><a href="https://gepark.org/registration/how-to-register/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-bullhorn fa-lg" aria-hidden="true"></i></span> Registration Information</a></li><li><a href="https://gepark.org/registration/financial-aid/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-heart fa-lg" aria-hidden="true"></i></span> Scholarship Assistance</a></li><li><a href="https://gepark.org/registration/special-needs-inclusion/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-users fa-lg" aria-hidden="true"></i></span> Inclusion Services</a></li><li><a href="https://gepark.org/refundform/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-undo-alt fa-lg" aria-hidden="true"></i></span> Refund Requests</a></li><li><a href="https://gepark.org/rainout-line/" target=""><span style="display: inline-block; width: 20px; text-align: center;"><i class="fas fa-exclamation-circle fa-lg" aria-hidden="true"></i></span> Program Status Hotline</a></li> </ul>
</div>
<div class="section">
<ul class="multi-column has-separators eight-count" style="width: 500px;">
<li><a href="https://gepark.org/adults-seniors/" target="">Adults &amp; Seniors</a></li><li><a href="https://gepark.org/adventure-time/" target="">Before &amp; After School Care</a></li><li><a href="https://gepark.org/camps/" target="">Camps</a></li><li><a href="https://gepark.org/dance/" target="">Dance Academy</a></li><li><a href="https://gepark.org/fitness/" target="">Fitness</a></li><li><a href="https://gepark.org/gymnastics/" target="">Gymnastics</a></li><li><a href="https://gepark.org/nature/" target="">Nature</a></li><li><a href="https://gepark.org/platformtennis/" target="">Platform Tennis</a></li><li><a href="https://gepark.org/preschool/" target="">Preschool &amp; Enrichment</a></li><li><a href="https://gepark.org/athletics/" target="">Sports &amp; Leagues</a></li><li><a href="https://gepark.org/swimlessons/" target="">Swim Lessons</a></li><li><a href="https://gepark.org/tennis/" target="">Tennis Lessons</a></li><li><a href="https://gepark.org/theatre/" target="">Theatre</a></li> </ul>
</div>
</div>
<span class="corner"></span></div>
</li>
<li><a href="https://gepark.org/registration/how-to-register" class="registration " style="cursor: default;">Registration
<div class="tab-padding"></div>
</a>
<div id="registration_dropdown_menu" class="dropdown-menu" style="display: none;">
<ul class="has-separators">
<li><a href="https://apm.activecommunities.com/gepark/Home" target="_blank">Register Online <i class="fas fa-external-link-alt"></i></a></li><li><a href="https://gepark.org/activity-guide" target="">Playbook Program Guide</a></li><li><a href="https://gepark.org/registration/how-to-register" target="">Registration Information</a></li><li><a href="https://gepark.org/registration/financial-aid" target="">Scholarship Assistance</a></li><li><a href="https://gepark.org/registration/special-needs-inclusion" target="">Inclusion Services</a></li><li><a href="https://gepark.org/refundform/" target="">Refund Requests</a></li> </ul>
<span class="corner"></span></div>
</li>
<li><a href="https://gepark.org/calendar/" class="events " style="cursor: default;">Events
<div class="tab-padding"></div>
</a>
<div id="events_dropdown_menu" class="dropdown-menu" style="">
<ul class="has-separators">
<li><a href="https://gepark.org/calendar/" target=""><i class="fa fa-calendar-alt" aria-hidden="true"></i> Community &amp; Registered Events</a></li><li><a href="https://gepark.org/drop-in-programs/" target="">Drop-in Programs</a></li><li><a href="https://gepark.org/drives" target="">Blood &amp; Food Drives</a></li><li><a href="https://raceroster.com/events/2024/83872/freedom-four" target="">Freedom Four 4-Mile Run/Walk</a></li> </ul>
<span class="corner"></span></div>
</li>
<li><a href="https://gepark.org/get-involved/adopt-a-park" class="get-involved " style="cursor: default;">Get Involved
<div class="tab-padding"></div>
</a>
<div id="get_involved_dropdown_menu" class="dropdown-menu" style="display: none;">
<ul class="has-separators">
<li><a href="https://gepark.org/advertising/" target="">Advertising</a></li><li><a href="https://gepark.org/donations/" target="">Donations</a></li><li><a href="https://gepark.bamboohr.com/careers" target="_blank">Join Our Team <i class="fas fa-external-link-alt"></i></a></li><li><a href="https://gepark.org/friends/" target="">Foundation</a></li><li><a href="https://gepark.org/sponsorship/" target="">Sponsorship</a></li><li><a href="https://gepark.org/commemorative/" target="">Tree &amp; Bench Program</a></li><li><a href="https://gepark.org/volunteer/" target="">Volunteer</a></li> </ul>
<span class="corner"></span></div>
</li>
<span class="the-thin-blue-line right"></span></ul>
        <!-- END: Hardcode Main Nav -->
		
	</header>
	<label id="hamburger_menu_toggle" class="mobile-menu-button hamburger-menu-utility">
		<i class="fa fa-bars" aria-hidden="true"></i>
		Main Menu
	</label>
</div>
