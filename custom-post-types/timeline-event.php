<?php

function getTimelineEvents($group_id)
{
	$timeline_events = [];
	$query = [
		'post_type'      => 'timeline_event',
		'post_parent'    => 0,
		'posts_per_page' => -1,
		'tax_query'      => [
			[
				'taxonomy' => 'timeline_event_group',
				'field'    => 'term_id',
				'terms'    => $group_id,
			]
		],
		'meta_key'       => 'date',
		'orderby'        => 'meta_value',
		'order'          => 'ASC'
	];
	$timeline_event_posts = get_posts($query);
	foreach($timeline_event_posts as $timeline_event_post)
	{
		$date_string = getAdvancedCustomFieldValue('date', null, $timeline_event_post->ID);
		preg_match('/(\d+)\/(\d+)\/(\d+)/', $date_string, $date_array);
		if(count($date_array) < 4)
		{
			//TODO: Notify?
			continue;
		}

		$timeline_event = [
			'id'         => $timeline_event_post->ID,
			'slug'       => $timeline_event_post->post_name,
			'unique_id'  => $timeline_event_post->post_name,
			'name'       => $timeline_event_post->post_title,
			'media'      => [
				'url'     => getAdvancedCustomFieldValue('image', null, $timeline_event_post->ID),
				'caption' => getAdvancedCustomFieldValue('caption', null, $timeline_event_post->ID)
			],
			'date'       => $date_array[0],
			'start_date' => [
				'month' => null,
				'day'   => null,
				'year'  => $date_array[3]
			],
			'text'       => [
				'headline' => $timeline_event_post->post_title,
				'text'     => $timeline_event_post->post_content,
			]
		];

		$timeline_events[] = $timeline_event;
	}

	return $timeline_events;
}

function register_timeline_event_custom_post_type()
{
	// Set UI labels for Custom Post Type
	$labels = [
		'name'               => _x('Timeline Events', 'Post Type General Name', 'twentythirteen'),
		'singular_name'      => _x('Timeline Event', 'Post Type Singular Name', 'twentythirteen'),
		'menu_name'          => __('Timeline Events', 'twentythirteen'),
		'parent_item_colon'  => __('Parent Timeline Event', 'twentythirteen'),
		'all_items'          => __('All Timeline Events', 'twentythirteen'),
		'view_item'          => __('View Timeline Event', 'twentythirteen'),
		'add_new_item'       => __('Add New Timeline Event', 'twentythirteen'),
		'add_new'            => __('Add New', 'twentythirteen'),
		'edit_item'          => __('Edit Timeline Event', 'twentythirteen'),
		'update_item'        => __('Update Timeline Event', 'twentythirteen'),
		'search_items'       => __('Search Timeline Event', 'twentythirteen'),
		'not_found'          => __('Not Found', 'twentythirteen'),
		'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
	];

	// Set other options for Custom Post Type
	$args = [
		'label'               => __('timeline_events', 'twentythirteen'),
		'description'         => __('Timeline Event', 'twentythirteen'),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => [
			'title',
			'page-attributes',
			'editor',
//			'excerpt',
//			'author',
//			'thumbnail',
//			'comments',
//			'revisions',
			'custom-fields',
		],
		// You can associate this CPT with a taxonomy or custom taxonomy.
		'taxonomies'          => ['genres'],
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-media-default',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,//allows permalink
		'capability_type'     => 'page',
	];

	// Registering your Custom Post Type
	register_post_type('timeline_event', $args);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action('init', 'register_timeline_event_custom_post_type', 0);