<?php
/*
Template Name: Smart Template
*/

$content_container = get_field('content_container');
$content_container_title = $content_container['title'] ?: get_the_title();

function enqueue_font_awesome5()
{
	if(!defined('FA5_CDN_INCLUDE')) {
		return;
	}

	echo FA5_CDN_INCLUDE;
}

if(defined('FA5_CDN_INCLUDE')) {
	add_action('wp_head', 'enqueue_font_awesome5');
}

get_header();
?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main" class="smart-template">
			<?php _heroBanner(); ?>

			<?php _notificationBanner(); ?>

			<div class="content">
			  <?php _sidebar(); ?>

				<div class="main-col">
					<h3><?php echo $content_container_title; ?> <?php _shareThis(); ?></h3>
					<div class="content-wrap">
						<?php if(isset($content)) : ?>
							<?php echo $content; ?>
						<?php else : ?>
							<?php the_content(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
