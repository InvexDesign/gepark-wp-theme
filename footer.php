    <div class="foot-wrap">
	    <footer id="footer">
            <?php
                $org_info = get_field('org_info', 'options');
                $links_1 = get_field('links_column_1', 'options');
                $links_2 = get_field('links_column_2', 'options');
                $mission = get_field('mission', 'options');
            ?>
		    <div class="address col">
			    <p><strong><?= $org_info['name']; ?></strong><br /><?= $org_info['address']; ?><?php if ($org_info['gmaps'] != '') { ?><br /><a href="<?= $org_info['gmaps']; ?>" target="_blank">[+] Google Maps</a><?php } ?></p>
                <?php if ($org_info['general_contact_phone'] != '') { ?>
				<a href="tel:+1<?= _strip_phone_num($org_info['general_contact_phone']); ?>" class="icon-link"><!--<i class="fa fa-mobile" aria-hidden="true"></i> --><?= $org_info['general_contact_phone']; ?></a>
                <?php } ?>
                <?php if ($org_info['general_contact_email'] != '') { ?>
				<a href="mailto:<?= $org_info['general_contact_email']; ?>" class="icon-link"><!--<i class="fa fa-envelope-o" aria-hidden="true"></i> --><?= $org_info['general_contact_email']; ?></a>
                <?php } ?>
				<style type="text/css">
						#google_translate_element * {
							display: unset;
						}
				</style>
				<!--<div id="google_translate_element" style="margin-top: 16px;"></div>
				<script type="text/javascript">
					function googleTranslateElementInit()
					{
						new google.translate.TranslateElement({
							pageLanguage: 'en',
							includedLanguages: 'es,pl,ru,gu,zh-CN,tl',
							layout: google.translate.TranslateElement.InlineLayout.SIMPLE
						}, 'google_translate_element');

						setTimeout(() => {
							$("a.VIpgJd-ZVi9od-xl07Ob-lTBxed").click(function (event) {
				                event.preventDefault();
				            });
						}, 2000)
					}
				</script>
				<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->
		    </div>
		    <div class="col mission">
			    <h4><?= $mission['heading']; ?></h4>
			    <p><?= $mission['content']; ?></p>
		    </div>
		    <div class="links-1 col footer-nav">
			    <ul>
                    <?php
                    foreach ($links_1 as $nav_link) {
                        $target = $nav_link['link']['target'];
                        $title = $nav_link['link']['title'];
                        $url = $nav_link['link']['url'];
                        echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                        if ($target == "_blank")
                            echo ' <i class="fas fa-external-link-alt"></i>';
                        echo '</a></li>';
                    }
                    ?>
			    </ul>
		    </div>
		    <div class="links-2 col footer-nav">
			    <ul>
                    <?php
                    foreach ($links_2 as $nav_link) {
                        $target = $nav_link['link']['target'];
                        $title = $nav_link['link']['title'];
                        $url = $nav_link['link']['url'];
                        echo '<li><a href="'.$url.'" target="'.$target.'">'.$title;
                        if ($target == "_blank")
                            echo ' <i class="fas fa-external-link-alt"></i>';
                        echo '</a></li>';
                    }
                    ?>
			    </ul>
		    </div>
		    <div class="copyright col">
			    <img src="<?php echo get_template_directory_uri(); ?>/images/logo_footer.png" />
			    <span>Glen Ellyn Park District &copy; <?php echo date('Y'); ?><br />Powered by <a href="https://www.invexdesign.com" target="_blank" title="Chicago Web Design, Web Hosting, SEO | Invex Design"><strong>Invex Design</strong></a></span>
		    </div>
		    <span class="overline"> </span>
	    </footer>
    </div>
		<script>
        $(document).ready(function()
        {
						$('.smart-scroll').click(function()
						{
						    $anchor = $(this);
						    var target = $anchor.data('target');

						    if(target)
						    {
                    var offset = $anchor.data('offset') || 0;

                    $('html, body').animate({
                        scrollTop: $(target).offset().top + offset
                    }, 500);
								}

								return false;
						});
        });
		</script>
    <?php wp_footer(); ?>
</body>
</html>
