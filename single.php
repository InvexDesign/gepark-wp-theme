<?php get_header(); ?>

<?php global $post; ?>

<?php if($post && ($post->post_type == 'stec_event')) : ?>
	<?php include(get_template_directory() . '/template_stachethemes-single-event.php'); ?>
<?php else : ?>
	<section id="main">
		<div class="banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/spi-calendar.jpg)">
			<div class="banner-inner">
				<h2><span>News &amp; Updates</span></h2>
			</div>
		</div>
		<div class="content">
			<div class="main-col full-width">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<h3><?php the_title(); ?>	</h3>
						<div class="content-wrap">
							<p><span style="display: inline-block; background: rgba(34,147,197,1.0); color: #FFF; padding: 7px 12px;">Posted on: <strong><?php the_date('F j, Y'); ?></strong></span></p>
							<?php the_content(); ?>
							<div style="clear: both;"></div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php get_footer(); ?>