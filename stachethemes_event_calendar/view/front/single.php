<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main" class="i know this is here">
			<div class="content">
				<div class="main-col full-width">
					<h3><?php the_title(); ?></h3>
					<div class="content-wrap">
						<?php the_content(); ?>
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
