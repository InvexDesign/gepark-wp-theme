<?php

namespace Stachethemes\Stec;
?>


<div class="stec-layout-grid-event">

    <a href="#stec_replace_permalink">
        <span class="stec-layout-grid-event-image" stec_replace_image></span>
    </a>

    <div class="stec-layout-grid-event-tags">
        stec_replace_tags
    </div>

    <div class="stec-layout-grid-invited"><?php esc_html_e('Invited', 'stec'); ?></div>

    <div class="stec-layout-grid-event-wrap">

        <div class="stec-layout-grid-icon" stec_replace_event_background>stec_replace_icon</div>

        <span class="stec-layout-grid-event-title"><a target="_blank" href="#stec_replace_permalink">stec_replace_summary</a></span>
        <span class="stec-layout-grid-event-short-desc">stec_replace_short_desc</span>

        <div class="stec-layout-grid-event-status">
            <div class="stec-layout-grid-event-status-expired"><?php esc_html_e('Expired', 'stec'); ?></div>
            <div class="stec-layout-grid-event-status-progress"><?php esc_html_e('In Progress', 'stec'); ?></div>
        </div>
    </div>

    <div class="stec-layout-grid-event-ul">
        <span class="stec-layout-grid-has-guests">
            <span class="stec-layout-grid-guest" stec_replace_guest_image></span>
            <span class="stec-layout-grid-guest-name">stec_replace_guest_name</span>
        </span>
        <span class="stec-layout-grid-has-products">
            <i class="fa fa-shopping-cart"></i>
            <span>stec_replace_product_name</span>
        </span>
        <span class="stec-layout-grid-has-location">
            <i class="fa fa-map-marker"></i>
            <span>stec_replace_location</span>
        </span>
        <span class="stec-layout-grid-date">
            <i class="fas fa-clock"></i>
            <span>stec_replace_date</span>
        </span>
        <span class="stec-layout-grid-can-rsvp">
            <a class="stec-layout-grid-rsvp stec-style-button stec-layout-event-btn-fontandcolor" href="javascript:void(0);"><?php esc_html_e('RSVP to Event', 'stec'); ?></a>
        </span>
    </div>

</div>