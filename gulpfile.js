"use strict";

let gulp = require('gulp');
let sass = require('gulp-sass');
let concat = require('gulp-concat');
let del = require('del');
let tap = require('gulp-tap');

sass.compiler = require('node-sass');

const clean = () => {
	return del('./assets/css/bundle.css');
};

const compileGutenbergSass = () => {
	return gulp.src('./assets/sass/gutenberg/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('./assets/css/gutenberg'));
};

const compileSassBundle = () => {
	return gulp.src(['./assets/sass/bundle.scss'])
	.pipe(sass().on('error', sass.logError))
	.pipe(concat('bundle.css'))
	.pipe(gulp.dest('./assets/css'));
};

const build = gulp.series(clean, compileGutenbergSass, compileSassBundle);

const watch = () => {
	gulp.watch('./assets/sass/**/*.scss', gulp.series(build));
};

const defaultTasks = gulp.parallel(build);

exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = defaultTasks;