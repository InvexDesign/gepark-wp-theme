<?php
$id = !empty($block['anchor']) ? $block['anchor'] : 'gepark-heading-' . $block['id'];
$type = get_field('type');


switch(get_field('type'))
{
		case 'subheading':
			$heading_tag = 'h4';
			$type_class = 'subheading';
			break;
		case 'heading':
		default:
			$heading_tag = 'h2';
			$type_class = 'heading';
}

$show_border = get_field('show_border');
$color_scheme = get_field('color_scheme');
$use_fa_icon = get_field('use_fa_icon');
$text = get_field('text');
$classes = "agb-gepark-heading $type_class"
		. ($color_scheme ? " color-scheme--$color_scheme" : '')
		. ($show_border ? ' bordered' : '')
;
?>
<<?php echo $heading_tag; ?>
		id="<?php echo esc_attr($id); ?>"
		class="<?php echo $classes; ?>"
>
	<?php if($use_fa_icon) : ?>
		<?php
		$fa_icon = get_field('fa_icon');
		$position = $fa_icon['position_right'] ? 'right' : 'left';
		$icon = $fa_icon['icon_class'];
		?>
			<i class="icon <?php echo $icon; ?> position--<?php echo $position; ?>" aria-hidden="true"></i>
	<?php endif; ?>

	<span><?php echo $text; ?></span>
</<?php echo $heading_tag; ?>>
