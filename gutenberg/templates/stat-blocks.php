<?php
$id = !empty($block['anchor']) ? $block['anchor'] : 'stat-blocks-' . $block['id'];

$has_icons = false;//NOTE: true if any icon is set
$statistics = get_field('statistics');
foreach($statistics as $statistic)
{
	if($statistic['fa_icon'])
	{
	  $has_icons = true;
	  break;
  }
}
?>
<div id="<?php echo esc_attr($id); ?>" class="agb-stat-blocks <?php echo $has_icons ? 'has-icons' : 'no-icons'; ?>">
	<?php foreach($statistics as $statistic) : ?>
			<div class="stat-block">
				<?php if($statistic['fa_icon']) : ?>
					<i class="icon <?php echo $statistic['fa_icon']; ?>" aria-hidden="true"></i>
				<?php else : ?>
					<i class="icon placeholder fab fa-rebel" aria-hidden="true"></i>
				<?php endif; ?>

				<div class="stat">
					<span class="number"><?php echo $statistic['value']; ?></span>
					<?php if(isset($statistic['unit']) && $statistic['unit'] != '') : ?>
						<span class="unit"><?php echo $statistic['unit']; ?></span>
					<?php endif; ?>
				</div>
				<div class="description"><?php echo $statistic['description']; ?></div>
			</div>
	<?php endforeach; ?>
</div>
