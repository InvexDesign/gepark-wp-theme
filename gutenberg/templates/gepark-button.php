<?php
$id = !empty($block['anchor']) ? $block['anchor'] : 'gepark-button-' . $block['id'];
$link = get_field('link');
$use_fa_icon = get_field('use_fa_icon');
?>
<a id="<?php echo esc_attr($id); ?>"
	 class="agb-gepark-button gepark-button color-scheme--<?php echo get_field('color_scheme'); ?>"
	 href="<?php echo $link['url']; ?>"
	 target="<?php echo $link['target']; ?>"
>
	<?php if($use_fa_icon) : ?>
		<?php
		$fa_icon = get_field('fa_icon');
	  $position = $fa_icon['position_right'] ? 'right' : 'left';
	  $icon_class = $fa_icon['icon_class'];
		?>
		<i class="icon <?php echo $icon_class; ?> position--<?php echo $position; ?>" aria-hidden="true"></i>
	<?php endif; ?>
	<span><?php echo $link['title']; ?></span>
</a>
