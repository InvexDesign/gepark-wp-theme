<?php

if(!function_exists('acf_register_block_type')) {
	error_log('register-acf-gutenberg-blocks.php: Cannot register ACF Gutenberg Blocks, ACF is required!');
	return;
}

function register_acf_block_types()
{
	acf_register_block_type([
		'name'            => 'gepark-button',
		'title'           => __('GEPark Button'),
		'description'     => __('A button using the GEPark style guide'),
		'render_template' => 'gutenberg/templates/gepark-button.php',
		'category'        => 'formatting',
		'icon'            => '<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" role="img" aria-hidden="true" focusable="false"><path d="M19 6.5H5c-1.1 0-2 .9-2 2v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7c0-1.1-.9-2-2-2zm.5 9c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5v-7c0-.3.2-.5.5-.5h14c.3 0 .5.2.5.5v7zM8 13h8v-1.5H8V13z"></path></svg>',
		'enqueue_style'   => get_stylesheet_directory_uri() . '/assets/css/gutenberg/gepark-button.css',
		'keywords'        => ['button'],
	]);
	acf_register_block_type([
		'name'            => 'gepark-heading',
		'title'           => __('GEPark Heading'),
		'description'     => __('A heading using the GEPark style guide'),
		'render_template' => 'gutenberg/templates/gepark-heading.php',
		'category'        => 'formatting',
		'icon'            => '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" aria-hidden="true" focusable="false"><path d="M6.2 5.2v13.4l5.8-4.8 5.8 4.8V5.2z"></path></svg>',
		'enqueue_style'   => get_stylesheet_directory_uri() . '/assets/css/gutenberg/gepark-heading.css',
		'keywords'        => ['heading'],
	]);
	acf_register_block_type([
		'name'            => 'stat-blocks',
		'title'           => __('Stat Blocks'),
		'description'     => __('A row of up to 3 stats'),
		'render_template' => 'gutenberg/templates/stat-blocks.php',
		'category'        => 'formatting',
		'icon'            => 'chart-bar',
		'enqueue_style'   => get_stylesheet_directory_uri() . '/assets/css/gutenberg/stat-blocks.css',
		'keywords'        => ['heading'],
	]);
}
add_action('acf/init', 'register_acf_block_types');
