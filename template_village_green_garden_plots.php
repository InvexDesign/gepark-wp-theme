<?php
/*
Template Name: Village Green Garden Plots
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Village Green Garden Plots';
		$metaslider_id = 383;
		$google_maps_url = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2971.659503886826!2d-88.07853118455954!3d41.85715517922395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e539bceb6c05f%3A0xf1d96c7b58e88d7d!2s103+S+Lambert+Rd%2C+Glen+Ellyn%2C+IL+60137!5e0!3m2!1sen!2sus!4v1475701036949&output=embed';
		$sidebar_menu_id = 34;
		$sidebar_widget_area_id = 'village_green_garden_plots_sidebar_area';

		require_once('_templates/facility-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
