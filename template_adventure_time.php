<?php
/*
Template Name: Adventure Time
*/
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php
		$page_title = get_the_title();
		$banner_title = 'Adventure Time';
		$banner_image_url = get_template_directory_uri() . '/assets/images/banners/adventure-time.png';
		$sidebar_menu_id = 43;
		$sidebar_widget_area_id = 'adventure_time_sidebar_area';

		require_once('_templates/short-banner-with-sidebar.php'); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
